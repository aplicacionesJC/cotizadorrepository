@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('modprod.store', $modelo) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">DATOS</h5>
            {!! Form::open(['route' => ['productos.store', $modelo->id], 'class' => 'form']) !!}
            <div class="form-row">
                <div class="form-group col-md-6">
                    {!! Form::label('modelo', 'Modelo', ['class' => 'control-label']) !!}
                    {!! Form::text('modelo', $modelo->nombre, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('producto_id', 'Producto', ['class' => 'control-label']) !!}
                    {!! Form::select('producto_id', $productos, null, ['class' => $errors->has('producto_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('producto_id') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection
