@extends('layout.master') 

@section('styles')
<link rel="stylesheet" href="{{ URL::asset('css/jquery.steps.css') }}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('modprod.index', $modelo) !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        {{-- <h5 class="card-title">BUSQUEDA</h5> --}}
        {!! Form::open(['method' => 'GET', 'route' => 'producto.index']) !!}
            <div class="form-row">
                {{-- <div class="form-group col-md-4">
                    {!! Form::label('compania', 'Aseguradoras', ['class' => 'control-label']) !!}
                    {!! Form::select('compania', $companias, Request::get('compania'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('producto', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('producto', Request::get('producto'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('abreviatura', 'Abreviatura', ['class' => 'control-label']) !!}
                    {!! Form::text('abreviatura', Request::get('abreviatura'), ['class' => 'form-control']) !!}
                </div> --}}
            </div>
            <div class="col-md-12">
                <a href="{{ URL::asset('modelo/'.$modelo->id.'/productos/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />

@if (!$productos->isEmpty())
    @foreach ($productos as $index => $item)
    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="row">
                            <div class="col-xs-12">
                                {{ strtoupper($item->abreviatura_producto) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <button class="btn btn-danger float-right btn-sm m-1" onclick="change_state({{ $modelo->id }},{{ $item->codigo }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">COMPAÑIA</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre_compania) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">PRODUCTO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre_producto) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">TOPE GPS</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">$ {{ strtoupper($item->tope_gps) }}</div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <br />
    @endforeach
    <div class="col-xs-12">
        {{ $productos->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div>  
@else
    <div class="card">
        <div class="card-body">
            <p>No se encontraron resultados.</p>
        </div>
    </div>
@endif
@endsection

@section('scripts')
<script type="text/javascript">
    function change_state(modeloId, modeloProductoId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/modelo/'.$modelo->id.'/productos') }}/" + modeloProductoId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };
</script>
@endsection