@extends('layout.master') 

@section('styles')
<style type="text/css">
    @media screen and (min-width: 768px) {
        .modal-dialog {
          width: 700px; /* New width for default modal */
        }
        .modal-sm {
          width: 350px; /* New width for small modal */
        }
    }
    @media screen and (min-width: 992px) {
        .modal-lg {
          width: 950px; /* New width for large modal */
        }
    }
</style>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('prospecto.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        {!! Form::open(['method' => 'GET', 'route' => 'prospecto.index']) !!}
            <div class="form-row">
                <fieldset  class="form-group col-md-4">
                    {!! Form::label('cotizacion', 'Cotizacion', ['class' => 'control-label']) !!}
                    {!! Form::text('cotizacion', Request::get('cotizacion'), ['class' => 'form-control']) !!}
                </fieldset>
                <div class="form-group col-md-4">
                    {!! Form::label('documento', 'Documento', ['class' => 'control-label']) !!}
                    {!! Form::text('documento', Request::get('documento'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('estado', 'Estado', ['class' => 'control-label']) !!}
                    {!! Form::select('estado', config('global.ESTADO_COTIZACION'), Request::get('estado'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                {{-- <a href="{{ URL::asset('producto.create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a> --}}
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />

@if (!$prospectos->isEmpty())
    @foreach ($prospectos as $index => $item)
    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8">
                                COTIZACION Nº {{ str_pad($item->codigo_cotizacion, 8, '0', STR_PAD_LEFT) }}
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <h5>
                                    @if($item->estado_cotizacion == 1)
                                        <span class="badge badge-WARNING">PENDIENTE</span>
                                    @elseif($item->estado_cotizacion == 2)
                                        <span class="badge badge-INFO">EN SOLICITUD</span>
                                    @elseif($item->estado_cotizacion == 3)
                                        <span class="badge badge-SUCCESS">COMPRADA</span>
                                    @endif
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="row justify-content-end">
                            <a href="{{ URL::asset('prospecto/pdf/'.$item->codigo_cotizacion.'') }}" class="btn btn-secondary btn-sm m-1 col-xs-12" target="_blank"><i class="mdi mdi-download"></i>PDF</a>
                            <button id="btnCorreo" class="btn btn-secondary btn-sm m-1 col-xs-12"  data-toggle="modal" data-target="#" data-title="{{ str_pad($item->codigo_cotizacion, 8, '0', STR_PAD_LEFT) }}" onclick="fnSetDataModal('{{ $item->codigo_cotizacion }}')"><i class="mdi mdi-email-alert"></i>Enviar mail</button>
                            <a href="{{ URL::asset('prospecto/'.$item->codigo_cotizacion.'') }}" class="btn btn-dark btn-sm m-1 col-xs-12"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">DOCUMENTO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->documento) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">CELULAR</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->celular) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">CORREO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->correo) }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">AUTO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre_marca) }} {{ strtoupper($item->nombre_modelo) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">AÑO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->anio) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">VALOR</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">$ {{ strtoupper($item->valor_aproximado) }}</div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <br />
    @endforeach
    <div class="col-xs-12">
        {{ $prospectos->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div>
@else
    <div class="card">
        <div class="card-body">
            <p>No se encontraron resultados.</p>
        </div>
    </div>
@endif

{{-- MODAL DE CORREO --}}
<div class="modal fade" id="modalCorreo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body load_modal">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" onclick="fnSendMail()">Enviar</button>
        </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var prospecto = 0;

    function fnSetDataModal(prospectoId){
        prospecto = prospectoId;

        var title = $("#btnCorreo").attr('data-title');

        $.get( "{{ url('/prospecto/correo') }}/" + prospectoId , function(data) {
            $('#modalCorreo').modal();
            $('#modalCorreo').on('shown.bs.modal', function(){
                $('#modalCorreo .load_modal').html(data);
                $('.modal-header .modal-title').html(title);
            });
            $('#modalCorreo').on('hidden.bs.modal', function(){
                $('#modalCorreo .modal-body').data('');
            });
        });
    };

    function fnSendMail(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        
        $.ajax({ 
            type: "POST",
            url: "{{ url('/prospecto/correo') }}/" + prospecto,
            data: '',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result){
                $('#modalCorreo').modal('toggle');
            },
            error: function(result){
                // Nada por ahora
            }
        });
    }
</script>
@endsection