@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('prospecto.show', $prospecto) !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">DETALLE</h5>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <div class="row">
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Documento</div>
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ $prospecto['documento_persona'] }}</div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="row">
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Celular</div>
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ $prospecto['celular_persona'] }}</div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="row">
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Correo</div>
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($prospecto['correo_persona']) }}</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <div class="row">
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Auto</div>
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ $prospecto['nombre_marca'] }} {{ $prospecto['nombre_modelo'] }}</div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="row">
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Año</div>
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ $prospecto['anio_fabricacion'] }}</div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="row">
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Valor</div>
                        <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">$ {{ $prospecto['valor_aproximado_prospecto'] }}</div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
<br />
<div class="card">
    <div class="card-body">
        <h5 class="card-title">COTIZACIONES</h5>
        @foreach($prospecto['cotizados'] as $index => $item)
        <div class="card">
                <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm">
                                {{ $item['nombre_compania'] }}
                            </div>
                           
                        </div>
                    </div>  
                </div>
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm">
                                <div class="row">
                                    <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Producto</div>
                                    <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ $item['nombre_producto'] }}</div>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="row">
                                    <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Prima Minima</div>
                                    <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">$ {{ $item['prima_minima'] }}</div>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="row">
                                    <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Tasa</div>
                                    <div class="col-sm" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ $item['valor_tasa'] }}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm">
                                <div class="row">
                                    <div class="col-sm mt-2" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">Cuotas</div>
                                </div>
                            </div>
                            <div class="col-sm-12 mt-2">
                                <table class="table table-bordered">
                                    <thead class="thead thead-light">
                                        <tr>
                                            <th>Mes</th>
                                            <th>Prima</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbdody>
                                        @foreach($item['cuotas'] as $index => $cuota)
                                        <tr>
                                            <td style="width: 33.33%"> {{ $cuota['meses'] }} </td>
                                            <td style="width: 33.33%"> {{ $cuota['prima'] }} </td>
                                            <td style="width: 33.34%">
                                                @if (empty($prospecto['codigo_cotizado']) && !Session::has('flash_message'))
                                                    <button id="btnAdquirir" class="btn btn-success btn-sm mx-1" onclick="fnOpenModal({{ $prospecto['codigo_prospecto'] }},{{ $cuota['codigo_cotizado'] }})"><i class="mdi mdi-shopping"></i>Adquirir</button>    
                                                @elseif ($cuota['codigo_cotizado'] == $prospecto['codigo_cotizado'])
                                                    <span id="state" class="badge badge-success">SELECCIONADO</span>
                                                @endif  
                                            </td>
                                        <tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
            <br />
        @endforeach
    </div>
</div>

{{-- MODAL DE SOLICITUD DE COMPRA --}}
<div class="modal fade" id="modalCompra" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">La cotización seleccionada formara parte de la solicitud de compra del cliente ({{ $prospecto['documento_persona'] }}), si esta seguro de ello por favor seleccionar </label>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-dark" onclick="fnSolicitarCompra()">Solicitar</button>
        </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var prospectoId = 0;
    var cotizacionId = 0;

    function fnOpenModal(prospecto, cotizacion) {
        prospectoId = prospecto;
        cotizacionId = cotizacion;
        $('#modalCompra').modal();
   };

   function fnSolicitarCompra(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        
        $.ajax({ 
            type: "POST",
            url: "{{ url('/prospecto/compra') }}",
            data: JSON.stringify({ 'prospectoId' : prospectoId, 'cotizacionId': cotizacionId }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result){
                $('#modalCompra').modal('hide');
                document.location = document.location;
            },
            error: function(result){
                // Nada por ahora
            }
        });
   }
</script>
@endsection