@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('compania.update', $compania) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Datos</h5>
        {!! Form::model($compania, ['method' => 'PATCH', 'route' => ['compania.update', $compania->id], 'class' => 'form', 'novalidate' => 'novalidate', 'files' => true]) !!}
            <div class="form-row">
                <div class="form-group col-md-6">
                    {!! Form::label('abreviatura', 'Abreviatura', ['class' => 'control-label']) !!}
                    {!! Form::text('abreviatura', $compania->abreviatura, ['class' => $errors->has('abreviatura') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('abreviatura') }}</div>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', $compania->nombre, ['class' => $errors->has('nombre') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('nombre') }}</div>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('logo', 'Logo', ['class' => 'control-label']) !!}
                    <div class="input-group input-file" name="logo">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-choose" type="button">Escoger</button>
                        </span>
                        <!-- <input id="logo" type="text" class="form-control" placeholder={{ $compania->nombre }} /> -->
                        {!! Form::text('logo', $compania->logo, ['class' => $errors->has('logo') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Escoge un archivo ...']) !!}
                        <div class="invalid-feedback">{{ $errors->first('logo') }}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right mt-4"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
    function bs_input_file() {
        $(".input-file").before(
            function() {
                if ( ! $(this).prev().hasClass('input-ghost') ) {
                    var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                    element.attr("name",$(this).attr("name"));
                    element.change(function(){
                        element.next(element).find('input').val((element.val()).split('\\').pop());
                    });
                    $(this).find("button.btn-choose").click(function(){
                        element.click();
                    });
                    $(this).find("button.btn-reset").click(function(){
                        element.val(null);
                        $(this).parents(".input-file").find('input').val('');
                    });
                    $(this).find('input').css("cursor","pointer");
                    $(this).find('input').mousedown(function() {
                        $(this).parents('.input-file').prev().click();
                        return false;
                    });
                    return element;
                }
            }
        );
    }

    $(function() {
        bs_input_file();
    });
</script>
@endsection