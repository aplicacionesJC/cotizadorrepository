
@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('compania.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        {!! Form::open(['method' => 'GET', 'route' => 'compania.index']) !!}
            <div class="form-row">
                <div class="form-group col-md-6">
                    {!! Form::label('abreviatura', 'Abreviatura', ['class' => 'control-label']) !!}
                    {!! Form::text('abreviatura', Request::get('abreviatura'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', Request::get('nombre'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                <a href="{{ URL::asset('compania/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />

@if (!$companias->isEmpty())
    @foreach ($companias as $index => $item)
        <div class="card">
            <div class="card-header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                             {{ strtoupper($item->abreviatura) }}  
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <button class="btn btn-danger float-right btn-sm m-1" onclick="change_state({{ $item->id }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                            <a href="{{ URL::asset('compania/'.$item->id.'') }}" class="btn btn-dark float-right btn-sm m-1"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                            <a href="{{ URL::asset('producto?compania='.$item->id) }}" class="btn btn-secondary float-right btn-sm m-1"><i class="mdi mdi-wallet-giftcard"></i>Productos</a>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-5 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">COMPAÑIA</div>
                                <div class="col-sm-7 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre) }}</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-5 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">LOGO</div>
                                <div class="col-sm-7 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">
                                    <span onclick="fnSetDataModal('{{ $item->abreviatura }}', '{{ $item->logo }}')">
                                        <a href="#">Vista previa </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <br />
    @endforeach
    <div class="col-xs-12">
        {{ $companias->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div> 
@else
<div class="card">
    <div class="card-body">
        <p>No se encontraron resultados.</p>
    </div>
</div>
@endif

{{-- MODAL DE IMAGENES --}}
<div class="modal fade" id="modalImagen" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body load_modal">
                
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">

    function fnSetDataModal(compania, logo)
    {
        var compania = compania.toLowerCase();
        var imagen = "<div style='text-align:center'><image src='" + logo + "' width='300px' heigth='600px'/></div>";
        
        $('#modalImagen').on('shown.bs.modal', function(){
            $('#modalImagen .load_modal').html(imagen);
            $('.modal-header .modal-title').html(compania.toUpperCase());
        });
        $('#modalImagen').modal();
        $('#modalImagen').on('hidden.bs.modal', function(){
            $('#modalImagen .modal-body').data('');
        });
    };

    function change_state(companiaId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/compania') }}/" + companiaId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                location.reload();
                //document.location = document.location;
            }
        });
    };

</script>

@endsection