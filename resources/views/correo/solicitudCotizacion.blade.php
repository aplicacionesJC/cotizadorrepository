<div style="font-family: Arial, verdana; font-size:13px; color: #444444; min-height: 200px;" bgcolor="#E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" height="100%" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
		<tbody>
		<tr>
			<td width="100%" align="center" valign="top" bgcolor="#E4E6E9" style="background-color:#E4E6E9; min-height: 200px;">
			<table>
				<tbody>
				<tr>
					<td class="table-td-wrap" align="center" width="558">
					<table class="table-space" height="18" style="height: 18px; font-size: 0px; line-height: 0; width: 800px; background-color: #e4e6e9;" width="800" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-space-td" valign="middle" height="18" style="height: 18px; width: 800px; background-color: #e4e6e9;" width="800" bgcolor="#E4E6E9" align="left">&nbsp;</td>
						</tr>
						</tbody>
					</table>
					<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" align="left">&nbsp;</td>
						</tr>
						</tbody>
					</table>

					<table class="table-row" width="800" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-row-td" style="font-family: Arial, verdana; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
							<table class="table-col" align="left" width="728" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
								<tbody>
								<tr>
									<td class="table-col-td" width="728" style="font-family: Arial, verdana; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 728px;" valign="top" align="left">
										<table class="header-row" width="728" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
											<tbody>
											<tr>
												<td class="header-row-td" width="728" style="font-family: Arial, verdana; font-weight: bold; line-height: 19px; color: #092B46; margin: 0px; font-size: 20px; padding-bottom: 10px; padding-top: 15px;" valign="top" align="center">
														<img width="200.61px" height="41px" src="http://arias.com.pe/cotizacion/images/cotizador/logo.png" />
														<br/>
														<p>HOLA! TU SEGURO DE AUTO TE ESPERA! RÁPIDO Y FÁCIL!</p>
												</td>
											</tr>
											</tbody>
										</table>
										<div style="font-family: Arial, verdana; line-height: 20px; color: #444444; font-size: 13px;">
											<p>
												Te saludamos de <strong> Corredores de seguros Arias & Asociados </strong>, Nos es grato adjuntar tu solicitud de cotizacion para tu auto <strong> {{ $info['info_auto'] }} </strong> por la suma de <strong> USD. {{ $info['valor_aproximado'] }} </strong>, estamos aqui para apoyarte durante el proceso de decisión de compra de tu seguro.
												Puedes contactarte con nosotros comunicandote con nuestras asesoras via email
											</p>
										</div>
										<div style="font-family: Arial, verdana; line-height: 20px; color: #444444; font-size: 13px;">
											<p>
												Teresa Morales Espinar <br/>
												Email: <a href="teresa.morales@arias.com.pe">teresa.morales@arias.com.pe</a>
											</p>
										</div>
										<div style="font-family: Arial, verdana; line-height: 20px; color: #444444; font-size: 13px;">
											<p>
											Carmen Dionicio Aira <br/>
											Email: <a href="carmen.dionicio@arias.com.pe">carmen.dionicio@arias.com.pe</a>
											</p>
										</div>
										<div style="font-family: Arial, verdana; line-height: 20px; color: #444444; font-size: 13px;">
											<p>o puedes contactarte con nostros vía telefónica al siguiente número <strong> (01) 711-6393 </strong>
											</p>
										</div>
									</td>
								</tr>
								</tbody>
							</table>
							</td>
						</tr>
						</tbody>
					</table>
					<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" align="left">&nbsp;</td>
						</tr>
						</tbody>
					</table>
					<table class="table-row" width="800" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-row-td" style="font-family: Arial, verdana; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
							<table class="table-col" align="left" width="728" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
								<tbody>
								<tr>
									<td class="table-col-td" width="728" style="font-family: Arial, verdana; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 728px;" valign="top" align="left">
									<div style="font-family: Arial, verdana; line-height: 19px; color: #444444; text-align: center;">

										<table style="border:1px solid #e3e3e3;border-collapse:collapse;" width="100%">
										<tr>
											<td align="center" bgcolor="#f5f5f5" style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:bold;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;background-color:#f5f5f5;width:180px" valign="top">Productos</td>
											<td align="center" bgcolor="#f5f5f5" style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:bold;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;background-color:#f5f5f5" valign="top">Prima</td>
										</tr>
										@foreach($info['companias'] as $compania)
										<tr>
											@if($compania['abreviatura'] == 'RIMAC')
												<td style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:normal;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;width:180px" valign="top">
													<img width="180px" height="87px" src="http://arias.com.pe/cotizacion/images/compania/rimac.png" />
												</td>
											@elseif($compania['abreviatura'] =='MAPFRE')
												<td style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:normal;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;width:180px" valign="top">
													<img width="180px" height="87px" src="http://arias.com.pe/cotizacion/images/compania/mapfre.png" />
												</td>
											@elseif($compania['abreviatura'] =='POSITIVA')
												<td style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:normal;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;width:180px" valign="top">
													<img width="180px" height="87px" src="http://arias.com.pe/cotizacion/images/compania/positiva.png" />												
												</td>
											@elseif($compania['abreviatura'] =='HDI')
												<td style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:normal;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;width:180px" valign="top">
													<img width="180px" height="87px" src="http://arias.com.pe/cotizacion/images/compania/hdi.png" />
												</td>
											@elseif($compania['abreviatura'] =='PACIFICO')
												<td style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:normal;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;width:180px" valign="top">
													<img width="180px" height="87px" src="http://arias.com.pe/cotizacion/images/compania/pacifico.png" />
												</td>
											@elseif($compania['abreviatura'] =='QUALITAS')
												<td style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:normal;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;width:180px" valign="top">
													<img width="180px" height="87px" src="http://arias.com.pe/cotizacion/images/compania/qualitas.png" />
												</td>
											@endif
											<td style="font-family:Arial,verdana;line-height:24px;color:#444444;font-size:13px;font-weight:normal;text-align:center;padding:9px;border-width:1px;border-style:solid;border-color:#e3e3e3;" valign="top">
												@foreach($compania['prima'] as $prima)
													@if($prima['mes'] == 1)
														<div>
																US$ {{ $prima['prima'] }} / mes
														</div>
													@else
														<div>
																{{ $prima['mes'] }} cuotas de US$ {{ $prima['prima'] }} 
														</div>
													@endif
												@endforeach
											</td>
										</tr>
										@endforeach
										</table>

									</div>
									<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 728px; background-color: #ffffff;" width="728" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
										<tbody>
										<tr>
											<td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 728px; background-color: #ffffff;" width="728" bgcolor="#FFFFFF" align="left">&nbsp;</td>
										</tr>
										</tbody>
									</table>
									</td>
								</tr>
								</tbody>
							</table>
							</td>
						</tr>
						</tbody>
					</table>
					<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" align="left">&nbsp;</td>
						</tr>
						</tbody>
					</table>
					<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 800px; padding-left: 16px; padding-right: 16px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" align="center">
							&nbsp;<table bgcolor="#E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
									<td bgcolor="#E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td>
								</tr>
								</tbody>
							</table>
							</td>
						</tr>
						</tbody>
					</table>
					<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" align="left">&nbsp;</td>
						</tr>
						</tbody>
					</table>

					<table class="table-row-fixed" width="800" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-row-fixed-td" style="font-family: Arial, verdana; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 1px; padding-right: 1px;" valign="top" align="left">
							<table class="table-col" align="left" width="798" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
								<tbody>
								<tr>
									<td class="table-col-td" width="798" style="font-family: Arial, verdana; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
									<table width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
										<tbody>
										<tr>
											<td width="100%" align="center" bgcolor="#f5f5f5" style="font-family: Arial, verdana; line-height: 24px; color: #bbbbbb; font-size: 13px; font-weight: normal; text-align: center; padding: 9px; border-width: 1px 0px 0px; border-style: solid; border-color: #e3e3e3; background-color: #f5f5f5;" valign="top">
											<a href="http://arias.com.pe" style="color: #000df2; text-decoration: none; background-color: transparent;">Arias & Asociados corredores de Seguros © 2020</a>
											</td>
										</tr>
										</tbody>
									</table>
									</td>
								</tr>
								</tbody>
							</table>
							</td>
						</tr>
						</tbody>
					</table>
					<table class="table-space" height="1" style="height: 1px; font-size: 0px; line-height: 0; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-space-td" valign="middle" height="1" style="height: 1px; width: 800px; background-color: #ffffff;" width="800" bgcolor="#FFFFFF" align="left">&nbsp;</td>
						</tr>
						</tbody>
					</table>
					<table class="table-space" height="36" style="height: 36px; font-size: 0px; line-height: 0; width: 800px; background-color: #e4e6e9;" width="800" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td class="table-space-td" valign="middle" height="36" style="height: 36px; width: 800px; background-color: #e4e6e9;" width="800" bgcolor="#E4E6E9" align="left">&nbsp;</td>
						</tr>
						</tbody>
					</table>
					</td>
				</tr>
				</tbody>
			</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>