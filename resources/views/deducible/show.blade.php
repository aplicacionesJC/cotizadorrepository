@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('deducible.update', $deducible) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">DATOS</h5>
        {!! Form::model($deducible, ['method' => 'PATCH', 'route' => ['deducible.update', $deducible->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-4">
                    {!! Form::label('flag_resumen', 'Mostrar en resumen', ['class' => 'control-label']) !!}
                    {!! Form::select('flag_resumen', $resumen, $deducible->flag_resumen, ['class' => $errors->has('flag_resumen') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('flag_resumen') }}</div>
                </div>
                <div class="form-group col-md-8">
                    {!! Form::label('producto_id', 'Producto', ['class' => 'control-label']) !!}
                    {!! Form::select('producto_id', $productos, $deducible->producto_id, ['class' => $errors->has('adicional_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('producto_id') }}</div>
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('tipo_adicional_id', 'Tipo', ['class' => 'control-label']) !!}
                    {!! Form::select('tipo_adicional_id', $tiposConcepto, $deducible->tipo_adicional_id, ['class' => $errors->has('tipo_adicional_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('tipo_adicional_id') }}</div>
                </div>
                <div class="form-group col-md-8">
                    {!! Form::label('adicional_id', 'Concepto', ['class' => 'control-label']) !!}
                    {!! Form::select('adicional_id', $conceptos, $deducible->adicional_id, ['class' => $errors->has('adicional_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('adicional_id') }}</div>
                </div>
                <div class="form-group col-md-12">
                    {!! Form::label('descripcion', 'Deducible', ['class' => 'control-label']) !!}
                    {!! Form::text('descripcion', $deducible->descripcion, ['class' => $errors->has('descripcion') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('descripcion') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#tipo_adicional_id').change(function(){
            $.get("{{ url('/conceptosportipo') }}",
                { tipo : $(this).val() },
                function(data){
                    var adicionales = $("#adicional_id");
    
                    adicionales.empty();
        
                    adicionales.append("<option value=''>SELECCIONAR</option>");
                    $.each(data, function(index, element){
                        adicionales.append("<option value='" + element.id + "'>" + element.descripcion + "</option>");
                    });
                });
        });
    });
</script>
@endsection