@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('deducible.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        {!! Form::open(['method' => 'GET', 'route' => 'deducible.index']) !!}
            <div class="form-row">
            <div class="form-group col-md-4">
                    {!! Form::label('tipo_concepto_id', 'Tipos de Concepto', ['class' => 'control-label']) !!}
                    {!! Form::select('tipo_concepto_id', $tiposConcepto, Request::get('tipo_concepto_id'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-8">
                    {!! Form::label('concepto_id', 'Conceptos', ['class' => 'control-label']) !!}
                    {!! Form::select('concepto_id', $conceptos, Request::get('concepto_id'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('producto_id', 'Producto', ['class' => 'control-label']) !!}
                    {!! Form::select('producto_id', $productos, Request::get('producto_id'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('descripcion', 'Descripción', ['class' => 'control-label']) !!}
                    {!! Form::text('descripcion', Request::get('descripcion'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-2">
                    {!! Form::label('flag_resumen', 'En Resumen', ['class' => 'control-label']) !!}
                    {!! Form::select('flag_resumen', $resumen, Request::get('flag_resumen'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                <a href="{{ URL::asset('deducible/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />

@if (!$deducibles->isEmpty())
    @foreach ($deducibles as $index => $item)
    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="row">
                            <div class="col-xs-12">
                                {{ strtoupper($item->nombre_producto) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <button class="btn btn-danger float-right btn-sm m-1" onclick="change_state({{ $item->codigo_deducible }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                        <a href="{{ URL::asset('deducible/'.$item->codigo_deducible.'') }}" class="btn btn-dark float-right btn-sm m-1"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">TIPO DE CONCEPTO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->descripcion_tipo) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">CONCEPTO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->descripcion_concepto) }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">DEDUCIBLE</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->descripcion_deducible) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">EN RESUMEN</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ $item->flag_resumen == 1 ? 'SI' : 'NO' }}</div>
                        </div>
                    </div> 
                </div>
            </div>  
        </div>
    </div>
    <br />
    @endforeach
    <div class="col-xs-12">
        {{ $deducibles->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div>  
@else
    <div class="card">
        <div class="card-body">
            <p>No se encontraron resultados.</p>
        </div>
    </div>
@endif
@endsection


@section('scripts')

<script type="text/javascript">

    function change_state(deducibleId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/deducible') }}/" + deducibleId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };

</script>

@endsection
