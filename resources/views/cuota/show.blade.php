@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('cuota.update', $cuota) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">DATOS</h5>
        {!! Form::model($cuota, ['method' => 'PATCH', 'route' => ['cuota.update', $cuota->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-4">
                    {!! Form::label('compania_id', 'Aseguradora', ['class' => 'control-label']) !!}
                    {!! Form::select('compania_id', $companias, $cuota->compania_id, ['class' => $errors->has('compania_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('compania_id') }}</div>
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('meses', 'Meses', ['class' => 'control-label']) !!}
                    {!! Form::text('meses', $cuota->meses, ['class' => $errors->has('meses') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('meses') }}</div>
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('factor', 'Factor', ['class' => 'control-label']) !!}
                    {!! Form::text('factor', $cuota->factor, ['class' => $errors->has('factor') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('factor') }}</div>
                </div>  
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection