@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('concepto.update', $concepto) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Concepto</h5>
        {!! Form::model($concepto, ['method' => 'PATCH', 'route' => ['concepto.update', $concepto->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-12">
                    {!! Form::label('tipo_adicional_id', 'Tipo de Concepto', ['class' => 'control-label']) !!}
                    {!! Form::select('tipo_adicional_id', $tiposConcepto, $concepto->tipo_adicional_id, ['class' => $errors->has('tipo_adicional_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('tipo_adicional_id') }}</div>
                </div>
                <div class="form-group col-md-12">
                    {!! Form::label('descripcion', 'Concepto', ['class' => 'control-label']) !!}
                    {!! Form::text('descripcion', $concepto->descripcion, ['class' => $errors->has('descripcion') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('descripcion') }}</div>
                </div>
                <div class="form-group col-md-12">
                    {!! Form::label('orden', 'Orden', ['class' => 'control-label']) !!}
                    {!! Form::text('orden', $concepto->orden, ['class' => $errors->has('orden') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('orden') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection