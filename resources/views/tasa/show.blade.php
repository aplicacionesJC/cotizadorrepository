@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('tasa.update', $tasa) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Tasa</h5>
        {!! Form::model($tasa, ['method' => 'PATCH', 'route' => ['tasa.update', $tasa->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-4">
                    {!! Form::label('producto_id', 'Producto', ['class' => 'control-label']) !!}
                    {!! Form::select('producto_id', $productos, $tasa->producto_id, ['class' => $errors->has('producto') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('producto') }}</div>
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('antiguedad', 'Año', ['class' => 'control-label']) !!}
                    {!! Form::text('antiguedad', $tasa->antiguedad, ['class' => $errors->has('antiguedad') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('antiguedad') }}</div>
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('porcentaje', 'Porcentaje', ['class' => 'control-label']) !!}
                    {!! Form::text('porcentaje', $tasa->porcentaje, ['class' => $errors->has('porcentaje') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('porcentaje') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection