@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('tasa.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        {!! Form::open(['method' => 'GET', 'route' => 'tasa.index']) !!}
            <div class="form-row">
                <div class="form-group col-md-6">
                    {!! Form::label('producto_id', 'Producto', ['class' => 'control-label']) !!}
                    {!! Form::select('producto_id', $productos, Request::get('producto_id'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('anio', 'Año', ['class' => 'control-label']) !!}
                    {!! Form::select('anio', $anios, Request::get('anio'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                <a href="{{ URL::asset('tasa/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />


@if (!$tasas->isEmpty())
    @foreach ($tasas as $index => $item)
    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="row">
                            <div class="col-xs-6 col-md-8">
                                {{ strtoupper($item->nombre_producto) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="row justify-content-end">
                            <a href="{{ URL::asset('tasa/'.$item->codigo_tasa.'') }}" class="btn btn-dark btn-sm m-1"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                            <button class="btn btn-danger btn-sm m-1" onclick="change_state({{ $item->codigo_tasa }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">PORCENTAJE</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ $item->porcentaje }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">AÑO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ date("Y") - $item->antiguedad }}</div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <br />
    @endforeach
    <div class="col-xs-12">
        {{ $tasas->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div>  
@else
    <div class="card">
        <div class="card-body">
            <p>No se encontraron resultados.</p>
        </div>
    </div>
@endif
@endsection

@section('scripts')

<script type="text/javascript">
    function change_state(tasaId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/tasa') }}/" + tasaId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };

</script>
@endsection
