@if (Session::has('flash_message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ Session::get('flash_message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="card">
    <div class="card-body">
            <h5 class="card-title">DATOS</h5>
            {!! Form::open(['route' => 'tasa.store', 'class' => 'form']) !!}
                {!! Form::hidden('producto_id', $producto_id, ['required']) !!}
                <div class="form-row">
                    <div class="form-group col-md-5">
                        {!! Form::label('antiguedad', 'Antiguedad', ['class' => 'control-label']) !!}
                        {!! Form::text('antiguedad', null, ['class' => $errors->has('antiguedad') ? 'form-control is-invalid' : 'form-control']) !!}
                        <div class="invalid-feedback">{{ $errors->first('antiguedad') }}</div>
                    </div>
                    <div class="form-group col-md-4">
                        {!! Form::label('porcentaje', 'Porcentaje', ['class' => 'control-label']) !!}
                        {!! Form::text('porcentaje', null, ['class' => $errors->has('porcentaje') ? 'form-control is-invalid' : 'form-control']) !!}
                        <div class="invalid-feedback">{{ $errors->first('porcentaje') }}</div>
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::label('', 'Guardar', ['class' => 'control-label']) !!}
                        <button type="submit" class="btn btn-dark form-control"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
                    </div>
                </div>  
            {!! Form::close() !!}
        <div class="table table-striped table-hover">
            <div>
                <div class="form-row">
                    <div class="form-group col-md-4"></div>
                    <div class="form-group col-md-4"></div>
                    <div class="form-group col-md-4"></div>
                </div>
            </div>
            <div>
            @if (!$tasas->isEmpty())
                @foreach ($tasas as $item)
                {!! Form::model($item, ['method' => 'PATCH', 'route' => ['tasa.update', $item->codigo_tasa]]) !!}
                <div class="form-row">
                    <div class="form-group col-md-3">
                        {!! Form::text('antiguedad', $item->antiguedad_tasa, ['class' => $errors->has('antiguedad') ? 'form-control is-invalid' : 'form-control']) !!}
                        <div class="invalid-feedback">{{ $errors->first('antiguedad') }}</div>
                        {{--  <input type="text" class="form-control" id="antiguedad" value="{{ $item->antiguedad_tasa }}" required>  --}}
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::text('porcentaje', $item->porcentaje_tasa, ['class' => $errors->has('porcentaje') ? 'form-control is-invalid' : 'form-control']) !!}
                        <div class="invalid-feedback">{{ $errors->first('porcentaje') }}</div>
                        {{--  <input type="text" class="form-control" id="porcentaje" value="{{ $item->porcentaje_tasa }}" required>  --}}
                    </div>
                    <div class="form-group col-md-3">
                        <button type="submit" class="btn btn-dark btn-sm"><i class="mdi mdi-tooltip-edit"></i>Editar</button>
                    </div>
                    <div class="form-group col-md-3">
                            <button class="btn btn-danger float-right btn-sm" onclick="change_state({{ $item->codigo_tasa }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                    </div>
                </div>
                {!! Form::close() !!}
                @endforeach	
            @else
                <div class="form-group col-md-12">
                    <div>No se encontraron resultados.</div>
                </div>
            @endif						
            </tbody>
        </div>
    </div>
</div>

<script>
    function editarTasa(codigoTasa){
        $.post( "{{ url('/tasa')}}/" + producto , function(data) {
            console.log(data);
            $('#modalTasas').modal();
            $('#modalTasas').on('shown.bs.modal', function(){
                $('#modalTasas .load_modal').html(data);
                $('.modal-header .modal-title').html(title);
            });
            $('#modalTasas').on('hidden.bs.modal', function(){
                $('#modalTasas .modal-body').data('');
            });
        });
    }

    function change_state(tasaId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/tasa') }}/" + tasaId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };
</script>