<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Administrador - cotizador vehicular</title>
    <link rel="administracion" href="{{ URL::asset('images/cotizador/logo.min.png') }}" />

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ URL::asset('layout/iconfonts/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('layout/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('layout/css/vendor.bundle.addons.css') }}">
    <!-- End plugin css for this page -->

    <!-- inject:css -->
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">
    <!-- endinject -->

    @yield('styles')

</head>
<body>
    <div class="container-scroller">

        <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center" style="background-color:#0d61de">
            <a class="navbar-brand brand-logo" href="{{ URL::asset('/home') }}">
                <img src="{{ URL::asset('images/cotizador/logo.png') }}" alt="logo"  />
            </a>
            <a class="navbar-brand brand-logo-mini" href="{{ URL::asset('/home') }}">
                <img src="{{ URL::asset('images/cotizador/logo.min.png') }}" alt="logo" style="background-color:white"/>
            </a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center">
                <!-- <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
                    <li class="nav-item">
                    <a href="#" class="nav-link">Schedule
                        <span class="badge badge-primary ml-1">New</span>
                    </a>
                    </li>
                    <li class="nav-item active">
                    <a href="#" class="nav-link">
                        <i class="mdi mdi-elevation-rise"></i>Reports</a>
                    </li>
                    <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
                    </li>
                </ul> -->
            <ul class="navbar-nav navbar-nav-right">
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                        <i class="mdi mdi-bell"></i>
                        <span class="count">4</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                        <a class="dropdown-item">
                        <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
                        </p>
                        <span class="badge badge-pill badge-warning float-right">View all</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-success">
                            <i class="mdi mdi-alert-circle-outline mx-0"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-medium text-dark">Application Error</h6>
                            <p class="font-weight-light small-text">
                            Just now
                            </p>
                        </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-warning">
                            <i class="mdi mdi-comment-text-outline mx-0"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-medium text-dark">Settings</h6>
                            <p class="font-weight-light small-text">
                            Private message
                            </p>
                        </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-info">
                            <i class="mdi mdi-email-outline mx-0"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-medium text-dark">New user registration</h6>
                            <p class="font-weight-light small-text">
                            2 days ago
                            </p>
                        </div>
                        </a>
                    </div>
                </li> -->
                <li class="nav-item dropdown d-none d-xl-inline-block">
                    <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                        @if (Auth::check())
                            <span class="profile-text">Bienvenido, {{ Auth::user()->nombre }}</span>
                        @else
                            <span class="profile-text">Login</span>
                        @endif
                        <img class="rounded-circle img-sm" src="{{ URL::asset('images/faces/face3.png') }}" alt="Profile image"></img>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                        <!-- <a class="dropdown-item p-0">
                            <div class="d-flex border-bottom">
                                <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                                </div>
                                <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                                <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                                </div>
                                <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                                </div>
                            </div>
                        </a> -->
                        <a class="dropdown-item mt-3" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Cerrar Sesion
                        </a>
                        <a class="dropdown-item mt-3" href="{{ URL::asset('cambiarcontrasenia') }}">
                            Cambiar contraseña
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                <span class="mdi mdi-menu"></span>
            </button>
            </div>
        </nav>
        <div class="container-fluid page-body-wrapper">
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item nav-profile">
                    <div class="nav-link">
                        <div class="user-wrapper">
                            <div class="profile-image">
                                <img src="{{ URL::asset('images/faces/face3.png') }}" alt="profile image">
                            </div>
                            @if (Auth::check())
                            <div class="text-wrapper">
                                <p class="profile-name">{{ Auth::user()->nombre }}</p>
                                <div>
                                <small class="designation text-muted">{{ Auth::user()->roles()->first()->nombre }}</small>
                                <span class="status-indicator online"></span>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::asset('/prospecto') }}">
                            <i class="menu-icon mdi mdi-cash-100"></i>
                            <span class="menu-title">Cotizaciones</span>
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="{{ URL::asset('/compania') }}">
                            <i class="menu-icon mdi mdi-bank"></i>
                            <span class="menu-title">Aseguradoras</span>
                        </a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#compania" aria-expanded="false" aria-controls="compania">
                            <i class="menu-icon mdi mdi-bank"></i>
                            <span class="menu-title"> Compañias </span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="compania">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/compania') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Aseguradoras</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/cuota') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Cuotas</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#auto" aria-expanded="false" aria-controls="auto">
                            <i class="menu-icon mdi mdi-car"></i>
                            <span class="menu-title"> Automoviles </span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="auto">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/marca') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Marcas</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/modelo') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Modelos</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#producto" aria-expanded="false" aria-controls="producto">
                            <i class="menu-icon mdi mdi-wallet-giftcard"></i>
                            <span class="menu-title"> Productos </span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="producto">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/producto') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Productos</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/tasa') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Tasas</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="{{ URL::asset('/producto') }}">
                            <i class="menu-icon mdi mdi-wallet-giftcard"></i>
                            <span class="menu-title">Productos</span>
                        </a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#deducible" aria-expanded="false" aria-controls="deducible">
                            <i class="menu-icon mdi mdi-clipboard-check"></i>
                            <span class="menu-title"> Beneficios </span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="deducible">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/tipo') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Tipos de Conceptos</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/concepto') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Conceptos</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::asset('/deducible') }}">
                                        <!-- <i class="menu-icon mdi mdi-clipboard-check"></i> -->
                                        <span class="menu-title">Detalle de Conceptos</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::asset('/usuario') }}">
                            <i class="menu-icon mdi mdi-account"></i>
                            <span class="menu-title">Usuarios</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::asset('/configuracion') }}">
                            <i class="menu-icon mdi mdi-settings"></i>
                            <span class="menu-title">Configuraciones</span>
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                            <i class="menu-icon mdi mdi-restart"></i>
                            <span class="menu-title">User Pages</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="auth">
                            <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="pages/samples/blank-page.html"> Blank Page </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="pages/samples/login.html"> Login </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="pages/samples/register.html"> Register </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="pages/samples/error-404.html"> 404 </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="pages/samples/error-500.html"> 500 </a>
                            </li>
                            </ul>
                        </div>
                    </li> -->
                </ul>
            </nav>
            <div class="main-panel">
                <div class="col-xs-12"> 
                    @yield('breadcrumbs') 
                </div>
                <div class="content-wrapper">
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get('flash_message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(Session::has('info_message'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            {{ Session::get('info_message')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(Session::has('error_message'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ Session::get('error_message')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <!-- plugins:js -->
    <script src="{{ URL::asset('layout/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ URL::asset('layout/js/vendor.bundle.addons.js') }}"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="{{ URL::asset('js/off-canvas.js') }}"></script>
    <script src="{{ URL::asset('js/misc.js') }}"></script>
    <!-- endinject -->
    @yield('scripts')
</body>
</html>