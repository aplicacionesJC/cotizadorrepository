@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('usuario.update', $usuario) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">DATOS</h5>
        {!! Form::model($usuario, ['method' => 'PATCH', 'route' => ['usuario.update', $usuario->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-6">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', null, ['class' => $errors->has('nombre') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('nombre') }}</div>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('email', 'Correo', ['class' => 'control-label']) !!}
                    {!! Form::text('email', null, ['class' => $errors->has('email') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                </div>
                {{-- <div class="form-group col-md-6">
                    {!! Form::label('password', 'Contraseña', ['class' => 'control-label']) !!}
                    {!! Form::password('password', ['class' => $errors->has('nombre') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('password_confirmation', 'Repetir Contraseña', ['class' => 'control-label']) !!}
                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                </div> --}}
                <div class="form-group col-md-6">
                    {!! Form::label('rol', 'Rol', ['class' => 'control-label']) !!}
                    {!! Form::select('rol[]', $roles, $usuario->roles, ['multiple'=>'multiple', 'class' => $errors->has('rol') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('rol') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection