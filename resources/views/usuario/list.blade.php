@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('usuario.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        {!! Form::open(['method' => 'GET', 'route' => 'usuario.index']) !!}
            <div class="form-row">
                <div class="form-group col-md-4">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', Request::get('nombre'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('correo', 'Correo', ['class' => 'control-label']) !!}
                    {!! Form::text('correo', Request::get('correo'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('rol', 'Rol', ['class' => 'control-label']) !!}
                    {!! Form::select('rol', $roles, Request::get('rol'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                <a href="{{ URL::asset('usuario/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />


@if (!$usuarios->isEmpty())
    @foreach ($usuarios as $index => $item)
    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="row">
                            <div class="col-xs-6 col-md-4">
                                {{ $item->nombre }}
                            </div>
                            {{-- <div class="col-xs-6 col-md-4">
                                <h5>
                                    @if($item->estado == 1)
                                        <span class="badge badge-SUCCESS">ACTIVO</span>
                                    @elseif($item->estado == 0)
                                        <span class="badge badge-DANGER">INACTIVO</span>
                                    @endif
                                </h5>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label class="switch">
                                    <input type="checkbox" class="success" {{ $item->estado == 1 ? "checked": ""}} onclick="change_state({{ $item->id }})">
                                    <span class="slider round"></span>
                                </label>
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <button class="btn btn-danger float-right btn-sm m-1" onclick="change_state({{ $item->id }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                        <a href="{{ URL::asset('usuario/'.$item->id.'') }}" class="btn btn-dark float-right btn-sm m-1"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">NOMBRE</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">CORREO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->email) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">ROLES</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">
                                @foreach($item->roles as $index => $rol)
                                    {{ strtoupper($rol->nombre) }}
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <br />
    @endforeach
    <div class="col-xs-12">
        {{ $usuarios->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div>
@else
    <div class="card">
        <div class="card-body">
            <p>No se encontraron resultados.</p>
        </div>
    </div>
@endif
@endsection

@section('scripts')
<script type="text/javascript">
    function change_state(usuarioId){
        if(!confirm('Desea eliminar el registro')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/usuario') }}/" + usuarioId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };
</script>
@endsection
