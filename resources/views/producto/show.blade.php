@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('producto.update', $producto) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">DATOS</h5>
        {!! Form::model($producto, ['method' => 'PATCH', 'route' => ['producto.update', $producto->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-3">
                    {!! Form::label('compania_id', 'Aseguradora', ['class' => 'control-label']) !!}
                    {!! Form::select('compania_id', $companias, $producto->companias, ['class' => $errors->has('compania_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('compania_id') }}</div>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', null, ['class' => $errors->has('nombre') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('nombre') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('abreviatura', 'Abreviatura', ['class' => 'control-label']) !!}
                    {!! Form::text('abreviatura', null, ['class' => $errors->has('abreviatura') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('abreviatura') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('prima_minima', 'Prima mìnima', ['class' => 'control-label']) !!}
                    {!! Form::text('prima_minima', null, ['class' => $errors->has('prima_minima') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('prima_minima') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('tope_gps', 'Tope GPS', ['class' => 'control-label']) !!}
                    {!! Form::text('tope_gps', null, ['class' => $errors->has('tope_gps') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('tope_gps') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('comision', 'Comision (%)', ['class' => 'control-label']) !!}
                    {!! Form::text('comision', null, ['class' => $errors->has('comision') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('comision') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('descuento', 'Descuento (%)', ['class' => 'control-label']) !!}
                    {!! Form::text('descuento', null, ['class' => $errors->has('descuento') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('descuento') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection