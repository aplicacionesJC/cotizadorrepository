@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('producto.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        {!! Form::open(['method' => 'GET', 'route' => 'producto.index']) !!}
            <div class="form-row">
                <div class="form-group col-md-4">
                    {!! Form::label('compania', 'Aseguradora', ['class' => 'control-label']) !!}
                    {!! Form::select('compania', $companias, Request::get('compania'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('producto', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('producto', Request::get('producto'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('abreviatura', 'Abreviatura', ['class' => 'control-label']) !!}
                    {!! Form::text('abreviatura', Request::get('abreviatura'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                <a href="{{ URL::asset('producto/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />


@if (!$productos->isEmpty())
    @foreach ($productos as $index => $item)
    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="row">
                            <div class="col-xs-6 col-md-8">
                                {{ strtoupper($item->abreviatura_producto) }}
                            </div>
                            {{-- <div class="col-xs-6 col-md-4">
                                <h5>
                                    @if($item->estado == 1)
                                        <span class="badge badge-SUCCESS">ACTIVO</span>
                                    @elseif($item->estado == 0)
                                        <span class="badge badge-DANGER">INACTIVO</span>
                                    @endif
                                </h5>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label class="switch">
                                    <input type="checkbox" class="success" {{ $item->estado == 1 ? "checked": ""}} onclick="change_state({{ $item->codigo_producto }})">
                                    <span class="slider round"></span>
                                </label>
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="row justify-content-end">
                            <!-- <button id="btnTasas" class="btn btn-secondary btn-sm m-1" data-toggle="modal" data-target="#" onclick="fnSetDataModal('{{ $item->codigo_producto }}', '{{ $item->abreviatura_producto }}')"><i class="mdi mdi-percent"></i>Tasas</button> -->
                            <a href="{{ URL::asset('tasa?producto_id='.$item->codigo_producto) }}" class="btn btn-secondary btn-sm m-1"><i class="mdi mdi-percent"></i>Tasas</a>
                            <a href="{{ URL::asset('deducible?producto_id='.$item->codigo_producto) }}" class="btn btn-secondary btn-sm m-1"><i class="mdi mdi-clipboard-check"></i>Beneficios</a>
                            <a href="{{ URL::asset('producto/'.$item->codigo_producto.'') }}" class="btn btn-dark btn-sm m-1"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                            <button class="btn btn-danger btn-sm m-1" onclick="change_state({{ $item->codigo_producto }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">COMPAÑIA</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre_compania) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">PRODUCTO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre_producto) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">TOPE GPS</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">$ {{ strtoupper($item->tope_gps) }}</div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <br />
    @endforeach
    <div class="col-xs-12">
        {{ $productos->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div>  
@else
    <div class="card">
        <div class="card-body">
            <p>No se encontraron resultados.</p>
        </div>
    </div>
@endif

{{-- MODAL DE TASAS --}}
<div class="modal fade" id="modalTasas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body load_modal">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    function fnSetDataModal(producto, title){
        $.get( "{{ url('/producto/tasa') }}/" + producto , function(data) {
            $('#modalTasas').modal();
            $('#modalTasas').on('shown.bs.modal', function(){
                $('#modalTasas .load_modal').html(data);
                $('.modal-header .modal-title').html(title);
            });
            $('#modalTasas').on('hidden.bs.modal', function(){
                $('#modalTasas .modal-body').data('');
            });
        });
    };

    function change_state(productoId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/producto') }}/" + productoId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };

</script>
@endsection
