@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('modelo.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        {!! Form::open(['method' => 'GET', 'route' => 'modelo.index']) !!}
            <div class="form-row">
            <div class="form-group col-md-5">
                    {!! Form::label('marca_id', 'Marca', ['class' => 'control-label']) !!}
                    {!! Form::select('marca_id', $marcas, Request::get('marca_id'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-5">
                    {!! Form::label('modelo', 'Modelo', ['class' => 'control-label']) !!}
                    {!! Form::text('modelo', Request::get('modelo'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-2">
                    {!! Form::label('eschino', 'Procedencia', ['class' => 'control-label']) !!}
                    {!! Form::select('eschino', config('global.PROCEDENCIA'), Request::get('eschino'), ['class' => 'form-control']) !!}
                </div>
            </div>
        
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                <a href="{{ URL::asset('modelo/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
            {!! Form::close() !!}
    </div>
</div>
<br />

@if (!$modelos->isEmpty())
    @foreach ($modelos as $index => $item)
    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                               MARCA: {{ strtoupper($item->nombre_marca) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="row justify-content-end">
                            <a href="{{ URL::asset('modelo/'.$item->codigo_modelo.'/productos') }}" class="btn btn-secondary btn-sm m-1"><i class="mdi mdi-wallet-giftcard"></i>Productos</a>
                            <a href="{{ URL::asset('modelo/'.$item->codigo_modelo.'') }}" class="btn btn-dark btn-sm m-1"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                            <button class="btn btn-danger btn-sm m-1" onclick="change_state({{ $item->codigo_modelo }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">MODELO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre_modelo) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">CHINO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">
                                @if($item->es_chino == 0)
                                    NO
                                @else
                                    SI
                                @endif  
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">VALOR</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->valor_referencial) }}</div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <br />
    @endforeach
    <div class="col-xs-12">
        {{ $modelos->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div>
@else
    <div class="card">
        <div class="card-body">
            <p>No se encontraron resultados.</p>
        </div>
    </div>
@endif
    
@endsection

@section('scripts')
<script type="text/javascript">

    function change_state(modeloId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/modelo') }}/" + modeloId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };
</script>
@endsection