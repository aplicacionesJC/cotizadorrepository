@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('modelo.update', $modelo) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Modelo</h5>
        {!! Form::model($modelo, ['method' => 'PATCH', 'route' => ['modelo.update', $modelo->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-9">
                    {!! Form::label('marca_id', 'Marca', ['class' => 'control-label']) !!}
                    {!! Form::select('marca_id', $marcas, null, ['class' => $errors->has('marca_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('marca_id') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('valor_referencial', 'Valor de mercado', ['class' => 'control-label']) !!}
                    {!! Form::text('valor_referencial', $modelo->valor_referencial, ['class' => $errors->has('valor_referencial') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('valor_referencial') }}</div>
                </div>
                <div class="form-group col-md-9">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', $modelo->nombre, ['class' => $errors->has('nombre') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('nombre') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('anio', 'Año', ['class' => 'control-label']) !!}
                    {!! Form::select('anio[]', $anios, $modelo->anios, ['multiple'=>'multiple', 'class' => $errors->has('anio') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('anio') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection