@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('configuracion.store') !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">DATOS</h5>
        {!! Form::open(['route' => 'configuracion.store', 'class' => 'form']) !!}
            <div class="form-row">
                 <div class="form-group col-md-6" id="selectAgrupador">
                    {!! Form::label('codigo_grupo', 'Agrupador', ['class' => 'control-label']) !!}
                    <select class="form-control {{$errors->has('codigo_grupo') ? 'form-control is-invalid' : 'form-control'}}" name="codigo_grupo" id="codigo_grupo" onchange="fnSetValue()">
                        <option value="">SELECCIONAR</option>
                        @foreach($agrupadores as $agrupador)
                            <option value="{{ $agrupador->nombre }}">{{ $agrupador->nombre }}</option>
                        @endforeach
                    </select>
                    <div class="invalid-feedback">{{ $errors->first('codigo_grupo') }}</div>
                </div> 
                <div class="form-group col-md-6" id="inputAgrupador" style="display: none">
                    {!! Form::label('codigo_grupo', 'Agrupador', ['class' => 'control-label']) !!}
                    {!! Form::text('codigo_grupo', null, ['class' => $errors->has('codigo_grupo') ? 'form-control is-invalid codigo_grupo' : 'form-control codigo_grupo']) !!}
                    <div class="invalid-feedback">{{ $errors->first('codigo_grupo') }}</div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check form-check-flat mt-4">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="nuevoGrupo" onchange="fnChangeShow()"> Nuevo Grupo
                        </label>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('codigo', 'Codigo', ['class' => 'control-label']) !!}
                    {!! Form::text('codigo', null, ['class' => $errors->has('codigo') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('codigo') }}</div>
                </div>
                
                <div class="form-group col-md-4">
                    {!! Form::label('descripcion', 'Descripcion', ['class' => 'control-label']) !!}
                    {!! Form::text('descripcion', null, ['class' => $errors->has('descripcion') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('descripcion') }}</div>
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('valor_texto', 'Valor', ['class' => 'control-label']) !!}
                    {!! Form::text('valor_texto', null, ['class' => $errors->has('valor_texto') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('valor_texto') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    var ischecked = false;
    function fnChangeShow(){
        ischecked = !ischecked;
        if(ischecked){ 
            $( "#inputAgrupador" ).show(); 
            $( "#selectAgrupador" ).hide();
        }
        else{ 
            $( "#selectAgrupador" ).show(); 
            $( "#inputAgrupador" ).hide();
            $( ".codigo_grupo" ).val($("#selectAgrupador option:selected").text());
        }
    }

    function fnSetValue(){
        $(".codigo_grupo").val($("#selectAgrupador option:selected").text());
    }
</script>
@endsection