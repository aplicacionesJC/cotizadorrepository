<html>
	<head>
		<style>
				@page 
				{ 
					margin: 0px; 
				}

				footer {
					position: fixed; 
					bottom: 0px; 
					left: 0px; 
					right: 0px;
					height: 40px;
				}

				.basic0
				{
					color: #fc4850;
					font-family: "Verdana";
					font-size: 11px;
					font-weight: bold;
					text-align: left;
				}

				.basic1
				{
					color: #092B46;
					font-family: "Verdana";
					font-size: 10px;
					font-weight: bold;
					text-align: left;
				}

				.basicfoot
				{
					color: #092B46;
					font-family: "Verdana";
					font-size: 10px;
					font-weight: bold;
					text-align: left;
					margin: 0px 20px 0px 20px;
				}

				.page
				{
					 width: 790px; 
				}

				.oval 
				{
					z-index: 3;
					float: left;
					width: 260px;
					height: 100px;
					background: #fc4850;
					border-bottom-right-radius: 120px;
				}

				.titlediv
				{
					z-index: 3;
					float: left;
					width: 660px;
					height: 100px;
				}

				.title 
				{
					z-index: 3;
					width: 440px;
					height: 26px;
					color: #092B46;
					font-family: "Verdana";
					font-size: 18px;
					font-weight: 700;
					line-height: 24px;
					text-align: left;
					margin: 40px;
				}
			
				.info {
					z-index: 3;
					display: inline-block;
					margin: 120px 20px 0px 20px;
					width: 700px;
					height: 20px;
					font-family: "Verdana";
					font-size: 14px;
					font-weight: 700;
					line-height: 17px;
					color: #1F2928;
				}

				.line 
				{
					z-index: 3;
					display: inline-block;
					margin: 0px 20px 0px 20px;
					width: 760px;
					height: 0px;
					border: solid #fc4850
				}
				  
				.container-fluid 
				{
					width: 700px;
					margin: 20px 20px 20px 20px;
					font-family: "Verdana";
				}

				table
				{
					margin: 10px auto;
				}
				  
				table > tbody > tr.tableizer-firstrow > th 
				{
					padding: 5px;
					color: #fc4850;
					background: #ffffff;
				}
				  
				body > div.container-fluid > table > tbody > tr > td
				{	
					padding: 5px;
					background: #f8f8f8;
				}
				
		</style>

		<div class="page">
			<div class="oval">
				<img src="images/cotizador/logo.png" width="180px" height="37px" style="margin:25px"></img>
			</div>
			<div class="titlediv">
				<div class="title">{{ $cotizacion['info_auto'] }} - USD. {{ $cotizacion['valor_aproximado'] }}</div>
			</div>
			<div class="info"> NRO. COTIZACION {{ str_pad($cotizacion['numero_prospecto'], 8, '0', STR_PAD_LEFT) }} - DNI {{ $cotizacion['documento'] }}</div>
			<div class="line"></div>
		</div>
	</head>
	
	<body>	
		<div class="container-fluid">
			<table class="tableizer-table">
				<tr class="tableizer-firstrow">
					<th width="{{ $cotizacion['ancho_base'] }}"></th>
					@foreach($cotizacion['companias'] as $compania)
						<th>
							<img src="{{ $compania['ruta'] }}" width="{{ $cotizacion['ancho_columna'] }}"></img>
						</th>
					@endforeach
				</tr>
				<tr>
					<td colspan="{{ $cotizacion['numero_productos'] }}" class="basic0">
							FORMAS DE PAGO
					</td>
				</tr>
				<tr>
					<td width="{{ $cotizacion['ancho_base'] }}" class="basic1"> CUOTAS DE PAGO </td>
					@foreach($cotizacion['companias'] as $compania)
						<td width="{{ $cotizacion['ancho_columna'] }}" class="basic1"> 
							@foreach($compania['prima'] as $prima)
									@if($prima['mes'] == 1)
										<div> {{ $prima['mes'] }} cuota de USD. {{ $prima['prima'] }} </div>
									@else

										<div> {{ $prima['mes'] }} cuotas de USD. {{ $prima['prima'] }} </div>
									@endif
							@endforeach
						</td>
					@endforeach
				</tr>
				@foreach($cotizacion['tiposCoberturas'] as $tipo)
					<tr>
						<td colspan="{{ $cotizacion['numero_productos'] }}" class="basic0">
							 {{ $tipo['tipoCobertura'] }} 
						</td>
					</tr>
					@foreach ($tipo['coberturas'] as $cobertura)
						<tr>
							<td width="{{ $cotizacion['ancho_base'] }}" class="basic1"> {{ $cobertura['descripcion'] }} </td>
							@foreach ($cobertura['productos'] as $deducible)
								<td width="{{ $cotizacion['ancho_columna'] }}" class="basic1"> {{ $deducible['descripcion'] }} </td>
							@endforeach
						</tr>
					@endforeach
				@endforeach
			</table>	  
		</div>
	</body>

	<footer>
		<div class="page">
				<div class="basicfoot">
						La cotización realizada responde solo a una simulación de la contratación de una póliza, por lo que la información brindada es referencial y los montos de las primas y cuotas son solo aproximados. Las coberturas y deducibles aquí descritos están
						sujetas a las Condiciones Generales y Particulares de la póliza. Los términos, condiciones y primas serán especificados en la póliza que se le entregará al asegurado al momento de la contratación. 
				</div>
		</div>
	</footer>
</html>