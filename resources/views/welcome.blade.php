
@extends('layout.master') 

@section('breadcrumbs')
    {!! Breadcrumbs::render('home') !!}
@endsection

@section('content')

<div class="card mb-3" style="max-width: 100%;">
    <div class="row no-gutters">
        <div class="col-md-9">
            <div class="card-body">
                <h5 class="card-title"><strong>COTIZA UN VEHÍCULO</strong></h5>
                <p class="card-text">Puedes cotizar un seguro vehicular en las mejores compañias en linea haciendo clic en la imagen</p>
                {{-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> --}}
            </div>
        </div>
        <div class="col-md-3">
            <a href="{{ URL::asset('/') }}" target="_blank"><img src="{{ URL::asset('images/home/vehicle.jpg') }}" class="card-img" alt="cotiza tu auto"></a>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $('.carousel').carousel()
    </script>
@endsection
