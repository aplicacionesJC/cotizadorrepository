@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('cambiarcontrasenia') !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        {!! Form::open(['route' => 'cambiarcontrasenia', 'class' => 'form']) !!}
            <div class="form-row">
                <div class="form-group col-md-12">
                    {!! Form::label('old_password', 'Contraseña actual', ['control-label']) !!}
                    {!! Form::password('old_password', ['class' => $errors->has('password') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('old_password') }}</div>
                </div>
                <div class="form-group col-md-12">
                    {!! Form::label('password', 'Nueva contraseña', ['class' => 'control-label']) !!}
                    {!! Form::password('password', ['class' => $errors->has('password') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                </div>
                <div class="form-group col-md-12">
                    {!! Form::label('password_confirmation', 'Repetir nueva contraseña', ['control-label']) !!}
                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection