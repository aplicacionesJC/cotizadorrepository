@extends('layout.master') 

@section('styles')
<link rel="stylesheet" href="{{ URL::asset('css/jquery.steps.css') }}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('marca.store') !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Marca</h5>
        {!! Form::open(['route' => 'marca.store', 'class' => 'form']) !!}
            <div class="form-row">
                <div class="form-group col-md-9">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', null, ['class' => $errors->has('nombre') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('nombre') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('es_chino', 'Procedencia', ['class' => 'control-label']) !!}
                    {!! Form::select('es_chino', config('global.PROCEDENCIA'), null, ['class' => $errors->has('es_chino') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('es_chino') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />


{{--  <div id="example-vertical">
    <h3>Marca</h3>
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'marca.store', 'class' => 'form']) !!}
                <div class="form-row">
                    <div class="form-group col-md-9">
                        {!! Form::label('nombre_marca', 'Nombre', ['class' => 'control-label']) !!}
                        {!! Form::text('nombre_marca', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::label('es_chino', 'Procedencia', ['class' => 'control-label']) !!}
                        {!! Form::select('es_chino', config('global.PROCEDENCIA'), null, ['class' => 'form-control']) !!}
                    </div>
                </div>
        </div>
    </div>
    <h3>Modelo</h3>
    <div class="card">
        <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        {!! Form::label('marca', 'Marca', ['class' => 'control-label']) !!}
                        {!! Form::select('marca', $marcas, null, ['disabled' => 'disabled', 'class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('nombre_modelo', 'Nombre', ['class' => 'control-label']) !!}
                        {!! Form::text('nombre_modelo', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('valor_aproximado', 'Valor de mercado', ['class' => 'control-label']) !!}
                        {!! Form::text('valor_aproximado', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>  --}}
@endsection

@section('scripts')
<script src="{{ URL::asset('js/jquery.steps.js') }}"></script>
<script type="text/javascript">
    {{--  $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical",
        onFinished: function (event, currentIndex)
        {
           
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if($('#nombre_marca').val() == "" || $('#es_chino').val() == ""){
                $("#marca").removeAttr("disabled");
            }else{
                $('#marca option[value=""]').prop("selected", true);
                $("#marca").attr("disabled", "disabled");
            }
            return true;
        }
    });  --}}
</script>
@endsection