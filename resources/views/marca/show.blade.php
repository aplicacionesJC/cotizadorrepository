@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('marca.update', $marca) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Marca</h5>
        {!! Form::model($marca, ['method' => 'PATCH', 'route' => ['marca.update', $marca->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-9">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', $marca->nombre, ['class' => $errors->has('nombre') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('nombre') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('es_chino', 'Procedencia', ['class' => 'control-label']) !!}
                    {!! Form::select('es_chino', config('global.PROCEDENCIA'), $marca->es_chino, ['class' => $errors->has('es_chino') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('es_chino') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
{{-- <br />
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Modelo</h5>
        {!! Form::model($modelo, ['method' => 'PATCH', 'route' => ['modelo.update', $modelo->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-4">
                    {!! Form::label('marca_id', 'Marca', ['class' => 'control-label']) !!}
                    {!! Form::select('marca_id', $marcas, null, ['class' => $errors->has('marca_id') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('marca_id') }}</div>
                </div>
                <div class="form-group col-md-5">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', $modelo->nombre, ['class' => $errors->has('nombre') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('nombre') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('valor_referencial', 'Valor de mercado', ['class' => 'control-label']) !!}
                    {!! Form::text('valor_referencial', $modelo->valor_referencial, ['class' => $errors->has('valor_referencial') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('valor_referencial') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div> --}}
@endsection

@section('scripts')
@endsection