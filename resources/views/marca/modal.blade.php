
<div class="card">
    <div class="card-body">
        <div class="table table-striped table-hover">
            <div>
                <div class="form-row">
                    <div class="form-group col-md-9">Marca</div>
                    <div class="form-group col-md-3"></div>
                </div>
            </div>
            <div>
            @if (!$marcas->isEmpty())
                @foreach ($marcas as $item)
                <div class="form-row">
                    <div class="form-group col-md-9">
                        <div class="col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre) }}</div>
                    </div>
                    <div class="form-group col-md-3">
                        <button class="btn btn-danger float-right btn-sm" onclick="change_state({{ $item->id }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                    </div>
                </div>
                @endforeach	
            @else
                <div class="form-group col-md-12">
                    <div>No se encontraron resultados.</div>
                </div>
            @endif						
            </tbody>
        </div>
    </div>
</div>


<script>
    function change_state(tasaId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            url: "{{ url('/marca') }}/" + tasaId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };
</script>