@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('marca.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        <!-- <div class="form-row">
            <div class="col-md-12">
                <button id="btnMarcas" class="btn btn-secondary float-right btn-sm m-1" onclick="fnSetDataModal()"><i class="mdi mdi-car"></i>Marcas</button>
            </div>
        </div> -->
        {!! Form::open(['method' => 'GET', 'route' => 'marca.index']) !!}
            <div class="form-row">
                <div class="form-group col-md-8">
                    {!! Form::label('marca', 'Marca', ['class' => 'control-label']) !!}
                    {!! Form::text('marca', Request::get('marca'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('eschino', 'Procedencia', ['class' => 'control-label']) !!}
                    {!! Form::select('eschino', config('global.PROCEDENCIA'), Request::get('eschino'), ['class' => 'form-control']) !!}
                </div>
            </div>
        
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                <a href="{{ URL::asset('marca/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
            {!! Form::close() !!}
    </div>
</div>
<br />

@if (!$marcas->isEmpty())
    @foreach ($marcas as $index => $item)
    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                {{ strtoupper($item->nombre) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                    <button class="btn btn-danger float-right btn-sm m-1" onclick="change_state({{ $item->id }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                            <a href="{{ URL::asset('marca/'.$item->id.'') }}" class="btn btn-dark float-right btn-sm m-1"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                            <a href="{{ URL::asset('modelo?marca_id='.$item->id) }}" class="btn btn-secondary float-right btn-sm m-1"><i class="mdi mdi-car"></i>Modelos</a>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">MARCA</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->nombre) }}</div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">CHINO</div>
                            <div class="col-sm-8 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">
                                @if($item->es_chino == 0)
                                    NO
                                @else
                                    SI
                                @endif  
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <br />
    @endforeach
    <div class="col-xs-12">
        {{ $marcas->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div>
@else
    <div class="card">
        <div class="card-body">
            <p>No se encontraron resultados.</p>
        </div>
    </div>
@endif

{{-- MODAL DE TASAS --}}
<div class="modal fade" id="modalMarca" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body load_modal">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
<script type="text/javascript">
    function fnSetDataModal(){
        $.get( "{{ url('marcas/lista') }}", function(data) {
            $('#modalMarca').modal();
            $('#modalMarca').on('shown.bs.modal', function(){
                $('#modalMarca .load_modal').html(data);
                $('.modal-header .modal-title').html('Lista de Marcas');
            });
            $('#modalMarca').on('hidden.bs.modal', function(){
                $('#modalMarca .modal-body').data('');
            });
        });
    };

    function change_state(marcaId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/marca') }}/" + marcaId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };
</script>
@endsection