<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="stylesheet" href="{{ URL::asset('layout/iconfonts/mdi/css/materialdesignicons.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/cotizador.css') }}">
        
    </head>
    <body>
        <div class="loader hidden">
            <img src="{{ URL::asset('images/cotizador/loading.gif') }}" alt="Loading..." />
        </div>
        <div class="continer-fluid">
            <div class="row" style="margin:5px;">
                <div class="col-sm-12 col-md-3 col-lg-4">
                    <div class="panel-search">
                        <div class="panel-content">
                            <div class="row">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title blue bold">INFORMACION DE COTIZACION</h5>
                                        <div class="">
                                            <div class="form-row">
                                                <div class="form-group col-sm-6 col-md-12">
                                                    <label class="control-label" name="documento_lbl">Documento</label>
                                                    <input type="text" class="form-control" name="documento" id="documento" disabled/>  
                                                </div>
                                                <div class="form-group col-sm-6 col-md-12">
                                                    <label class="control-label" name="edad_label">Edad</label>
                                                    <input type="text" class="form-control" name="edad" id="edad" disabled/>
                                                </div>
                                                <div class="form-group col-sm-6 col-md-12">
                                                    <label class="control-label" name="vigencia_lbl">VIGENCIA</label>
                                                    <input type="text" class="form-control" name="vigencia" id="vigencia" disabled/>
                                                </div>
                                                <div class="form-group col-sm-6 col-md-12">
                                                    <label class="control-label" name="marca_lbl">MARCA</label>
                                                    <select name="marca" id="marca" class="form-control" onchange="fnChargeModelos(this.options[this.selectedIndex].value)">
                                                        <option value="0">SELECCIONAR MARCA</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6 col-md-12">
                                                    <label class="control-label" name="modelo_lbl">MODELO</label>
                                                    <select name="modelo" id="modelo" class="form-control" onchange="fnChargeAnioModelos(this.options[this.selectedIndex].value)">
                                                        <option value="0">SELECCIONAR MODELO</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6 col-md-12">
                                                    <label class="control-label" name="anio_lbl">AÑO</label>
                                                    <select name="aniomodelo" id="aniomodelo" class="form-control">
                                                        <option value="0">SELECCIONAR AÑO</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6 col-md-12">
                                                    <label class="control-label" name="sumaasegurada_lbl">SUMA ASEGURADA</label>
                                                    <input type="text" class="form-control" name="sumaasegurada" id="sumaasegurada"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <button class="btn btn-orange float-right m-1 sf small bold" onclick="fnSendRequest()"><i class="mdi mdi-square-inc-cash mdi-16px"></i>COTIZAR</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9 col-lg-8">
                    <div class="panel-search" style="background-color:white !important">
                        <div class="panel-content">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sf semibig blue bold">COTIZA TU SEGURO VEHICULAR EN LAS MEJORES COMPAÑIAS</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-10 offset-lg-1">
                                    <img width="100%" height="120px" style="max-width:573px" src="images/compania/seguros.jpg"></img>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 sf small">
                                    Ten en cuenta que no todas las aseguradoras suelen brindar los mismos precios de seguros 
                                    ni brindar los mismos beneficios para un mismo auto, en <strong>Corredores de Seguros 
                                    Arias & Asociados</strong> tenemos la experiencia necesaria para guiarte y brindarte la asistencia que 
                                    necesitas para que puedas elegir la mejor opción para tu vehículo.
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 offset-4">
                                    <img width="180px" height="100px" src="images/compania/pacifico.png"></img>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 sf small">
                                    Para cotizar con <strong>Pacífico Seguros</strong>, comunicate directamente con nosotros al siguiente correo <strong>administracion@arias.com.pe</strong> o al <strong>(01) 711 6393</strong>.
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-2" style="text-align:center">
                                    <img width="100px" height="100px" src="images/cotizador/ahorro.png"></img>
                                </div>
                                <div class="col-sm-6 col-md-8 col-lg-2">
                                    <div class="row">
                                        <div class="sf median bold rose">AHORRA</div>
                                    </div>
                                    <div class="row">
                                        <div class="sf small">El mejor precio garantizado que puedes obtener a tu alcance!</div>
                                    </div>
                                </div>  
                                <div class="col-sm-6 col-md-4 col-lg-2" style="text-align:center">
                                    <img width="100px" height="100px" src="images/cotizador/asistencia.png"></img>
                                </div>
                                <div class="col-sm-6 col-md-8 col-lg-2">
                                    <div class="row">
                                        <div class="sf median bold rose">ASISTENCIA</div>
                                    </div>
                                    <div class="row">
                                        <div class="sf small">Te asistimos antes, durante y después de la contratación de tu seguro vehicular!</div>
                                    </div>
                                </div> 
                                <div class="col-sm-6 col-md-4 col-lg-2" style="text-align:center">
                                    <img width="100px" height="100px" src="images/cotizador/compara.png"></img>
                                </div>
                                <div class="col-sm-6 col-md-8 col-lg-2">
                                    <div class="row">
                                        <div class="sf median bold rose">COMPARA</div>
                                    </div>
                                    <div class="row">
                                        <div class="sf small">Cotiza tu seguro vehicular en segundos y evalua las mejores opciones!</div>
                                    </div>
                                </div>   
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-12">
                                    <div class="sf semibig orange bold" style="text-align:center">¿ CÓMO FUNCIONA ?</div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-6">
                                    <div class="sf small bold"><span class="blue">1.</span> Cotiza las veces que sea necesario</div>
                                    
                                </div>
                                <div class="col-6">
                                    <div class="sf small bold"><span class="blue">2.</span> Un asesor se pondrá en contacto contigo</div>
                                </div>
                                <div class="col-6">
                                    <div class="sf small bold"><span class="blue">3.</span> Elige el producto de tu preferencia</div>
                                </div>
                                <div class="col-6">
                                    <div class="sf small bold"><span class="blue">4.</span> Paga directamente a la aseguradora</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="compare" class="d-none">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="width: 25%;height:140px;">
                                            <div class="col-12">
                                                <img width="140px" height="140px" src="images/cotizador/businesscar.png"></img>
                                            </div>
                                            <div class="col-12">
                                                <div class="sf small bold">COMPARA HASTA CON 3 OPCIONES!</div>
                                            </div>   
                                        </td>
                                        <td style="width: 25%">
                                            <div id="divImg1" class="d-none">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <span class="float-right" onclick="fnDeleteProduct('divImg1')" style="cursor:pointer"><i class="mdi mdi-close-circle-outline mdi-24px orange"></i></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <img id="img1" width="130px" height="87px" src=""></img>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div id="nameImg1" class="sf small bold"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 25%">
                                            <div id="divImg2" class="d-none">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <span class="float-right" onclick="fnDeleteProduct('divImg2')" style="cursor:pointer"><i class="mdi mdi-close-circle-outline mdi-24px orange"></i></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <img id="img2" width="130px" height="87px" src=""></img>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div id="nameImg2" class="sf small bold"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 25%">
                                            <div id="divImg3" class="d-none">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <span class="float-right" onclick="fnDeleteProduct('divImg3')" style="cursor:pointer"><i class="mdi mdi-close-circle-outline mdi-24px orange"></i></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <img id="img3" width="130px" height="87px" src=""></img>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div id="nameImg3" class="sf small bold"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row d-none" id="btnCompare">
                            <div class="col-12" style="text-align:center">
                                <button class="btn btn-orange m-1 sf small bold" onclick="fnGetCoberturas()"><i class="mdi mdi-compare mdi-16px"></i>COMPARAR</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tbCotizaciones" class="col-sm-12">
                    <div class="panel-search">
                        <div class="panel-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-fixed">
                                    <thead>
                                        <tr>
                                            <th style="width: 20%"><div class="sf median bold">ASEGURADORA</div></th>
                                            <th style="width: 30%"><div class="sf median bold">COSTO TOTAL ANUAL</div></th>
                                            <th style="width: 20%"><div class="sf median bold">REQUIERE GPS</div></th>
                                            <th class="d-none d-sm-block" style="width: 30%"><div class="sf median bold">COBERTURAS</div></th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodyPrimas">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tbCoberturas" class="col-sm-12">
                    <div class="row">
                        <div class="col-12" style="text-align:right">
                            <button class="btn btn-orange m-1 sf small bold" onclick="fnBackCotizacion()"><i class="mdi mdi-compare mdi-16px"></i>REGRESAR</button>
                        </div>
                    </div>
                    <div class="panel-search">
                        <div class="panel-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-fixed">
                                    <thead id="headCoberturas">
                                    </thead>
                                    <tbody id="bodyCoberturas">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        
        <script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.range.js') }}"></script>
        <script type="text/javascript">
            const loader = $(".loader");
            var response = null;
            var prospecto;
            var arrayCompare = [];
            var arrayRequest = [];

            $(document).ready(function() 
            {
                const urlParams = new URLSearchParams(window.location.search);
                const prespectoId = urlParams.get('prospecto');
                prospecto = urlParams.get('prospecto');
                $("#tbCoberturas").hide();
                fnLoadPage(prespectoId);
            });

            function fnLoadPage(prespectoId)
            {
                $.ajax({ 
                    type: "GET",
                    url: "{{ url('/prospectoinfo') }}/" + prespectoId,
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result)
                    {
                        response = result;
                        fnSetDatosIngresados(result);
                        fnChargeMarcas();
                        fnChargeModelos(result['marca']['id']);
                        fnChargeAnioModelos(result['modelo']['id']);
                    },
                    error: function(result)
                    {
                    }
                });  
            }

            function fnChargeMarcas()
            {
                $.ajax({
                    type: "GET",
                    url:"{{url('/marcas')}}",
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result){
                        var element = $('#marca');
                        $.each(result, function(key, value){
                            if(value.toUpperCase() === response['marca']['nombre'].toUpperCase())
                                element.append("<option value='" + key + "' selected>" + value + "</option>");
                            else
                                element.append("<option value='" + key + "'>" + value + "</option>");
                        });
                        loader.addClass("hidden"); // class "loader hidden"
                    },
                    error: function(result){
                        // Nada por ahora
                    }
                });
            }

            function fnChargeModelos(id){
                $.ajax({
                    type: "GET",
                    url: "{{url('/modelos')}}/" + id,
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result){
                        var element = $('#modelo');
                        element.empty().append("<option value='0' selected>SELECCIONAR MODELO</option>");
                        $.each(result, function(key, value){
                            if(value.toUpperCase() === response['modelo']['nombre'].toUpperCase())
                                element.append("<option value='" + key + "' selected>" + value + "</option>");
                            else
                                element.append("<option value='" + key + "'>" + value + "</option>");
                        });
                    },
                    error: function(result){
                        // Nada por ahora
                    }
                });
            }

            function fnChargeAnioModelos(id)
            {
                $.ajax({
                    type: "GET",
                    url: "{{url('/aniomodelos')}}/" + id,
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result){
                        var element = $('#aniomodelo');
                        element.empty().append("<option value='0'>SELECCIONAR AÑO</option>");
                        $.each(result, function(key, value){
                            if(value.toUpperCase() === response['anio']['nombre'].toUpperCase())
                                element.append("<option value='" + key + "' selected>" + value + "</option>");
                            else
                                element.append("<option value='" + key + "'>" + value + "</option>");
                        });
                        loader.addClass("hidden");
                    },
                    error: function(result){
                        // Nada por ahora
                    }
                });
            }

            function fnSendRequest()
            {
                loader.removeClass("hidden");
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var quotation = {
                    persona:{
                        documento: response['documento'],
                        fecha_nacimiento: new Date((new Date()).getFullYear() - response['edad'], 1, 1)
                    },
                    prospecto:{
                        correo: response['correo'],
                        celular: response['celular'],
                        departamento_id: response['departamento_id'],
                        valor_aproximado: parseInt($('#sumaasegurada').val()),
                        marca_id: parseInt($('#marca').val()),
                        modelo_id: parseInt($('#modelo').val()),
                        anio_id: parseInt($('#aniomodelo').val()),
                        marca: $("#marca option:selected").text(),
                        modelo: $("#modelo option:selected").text(),
                        anio: $("#aniomodelo option:selected").text()
                    }
                };
                console.log('ola k INICIO');
                $.ajax({
                    type: "POST",
                    url: "{{url('/solicitarcotizacion')}}",
                    data: JSON.stringify(quotation),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result)
                    {
                        window.location.href = "{{url('/resultado?prospecto=')}}"+ result.id_prospecto;
                        loader.addClass("hidden");
                    },
                    error: function(error)
                    {
                        loader.addClass("hidden");
                    }
                });
            }

            function fnSetDatosIngresados(result)
            {
                /* LISTA INICIAL */
                $('#documento').val(result['documento'].toUpperCase());
                $('#edad').val(result['edad']);
                $('#anio').val(result['anio']);
                $('#vigencia').val(result['vigencia']);
                $('#sumaasegurada').val(result['sumaAsegurada']);

                /* TABLA */
                var elementTable = $('#bodyPrimas');
                $.each(result['companias'], function(key, value)
                {
                    var pagos = '';
                    var tempPayment = value['pagos'].sort(function (a, b) {
                                                            if (a.cuota > b.cuota) { return 1; }
                                                            if (a.cuota < b.cuota) { return -1; }
                                                            return 0;
                                                        });
                    $.each(tempPayment, function(key2, value2)
                    {
                        if(value2['cuota'] == 1) pagos = pagos + '<div class="sf semibig bold">USD.  ' + value2['prima'] + '</div><br/>';
                        else pagos = pagos + '<div class="sf small">' + value2['cuota']  + ' Cuotas mensuales de USD.  ' + value2['prima'] + '</div><br/>';
                    });

                    elementTable.append(
                        '<tr id="TABLE_'+ value['abreviatura'].toUpperCase() +'">' +
                            '<td>'+
                                    '<div class="row">' +
                                        '<div class="col-12 text-center sf small bold">' +
                                                value['producto'].toUpperCase() +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="col-12">' +
                                            '<img width="130px" height="87px" src="'+ fnGetLogoCompany(value['abreviatura']) +'"></img>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="col-12 text-center">' +
                                            '<button id="CMP_'+ value['abreviatura'].toUpperCase() +'" class="btn btn-orange btn-sm m-1" onclick="fnShowCompare(this.id)"><div class="sf small bold">COMPARAR</div></button>' +
                                        '</div>' +
                                    '</div>' +
                            '</td>'+
                            '<td id="PRIMA_'+ value['abreviatura'].toUpperCase() +'">' + pagos + '</td>' +
                            '<td><div class="sf small">'+ value['requieregps'] +'</div></td>' +
                            '<td class="d-none d-sm-block"><div class="sf small" style="padding:10px">'+ value['cobertura'] +'</div></td>'+
                        '</tr>');
                });
            }

            function fnGetLogoCompany(company)
            {
                company = company.toUpperCase();
                switch(company){
                    case 'MAPFRE':
                        return "images/compania/mapfre.png";
                    case 'POSITIVA':
                        return "images/compania/positiva.png";
                    case 'RIMAC':
                        return "images/compania/rimac.png";
                    case 'PACIFICO':
                        return "images/compania/pacifico.png";
                    case 'HDI':
                        return "images/compania/hdi.png";
                    case 'QUALITAS':
                        return "images/compania/qualitas.png";
                    default:
                        return '';
                }
            }

            function fnChangeAmountAuto()
            {
                var request = {
                    'prospecto_id': prospecto,
                    'suma_asegurada': $('#sumaAsegurada').val()
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                loader.removeClass("hidden");
                $.ajax({ 
                    type: "POST",
                    url: "{{url('/cambiarsumaasegurada')}}",
                    data: JSON.stringify(request),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result)
                    {
                        location.reload();
                        loader.addClass("hidden");
                    },
                    error: function(result)
                    {
                    }
                });
            }

            function fnShowCompare(id)
            {
                var company = id.substring(4);
                var image = fnGetLogoCompany(company);

                $("#compare").removeClass('d-none');
                var insert = false;
                var elementsCount = 0;
                for(var i = 1; i <= 3; i++)
                {
                    var selector = "img" + i;
                    var imgSrc = $("#" + selector).attr("src");
                    if(!imgSrc)
                    {
                        if(!insert)
                        {
                            $("#" + id).attr("disabled", "disabled")
                            $("#divImg" + i).removeClass('d-none');
                            $("#" + selector).attr("src", image);

                            var nombreProducto = "";
                            var productoID = 0;
                            $.each(response['companias'], function(key, value)
                            {
                                if(value["abreviatura"].toUpperCase() === company)
                                {
                                    nombreProducto = value["producto"].toUpperCase();
                                    productoID = value["productoID"];
                                }
                            });
                            $('#nameImg' + i).text(nombreProducto);

                            insert = true;
                            elementsCount++;

                            arrayCompare.push({ elementoDiv : "divImg" + i, elementoImg : "img" + i, elementoName : "nameImg" + i, posicion : i, compania : company, productoID : productoID })
                        }
                    }
                    else
                    {
                        elementsCount++;
                    }
                }

                if(elementsCount >= 1)
                {
                    $("#btnCompare").removeClass('d-none');
                }
            }

            function fnDeleteProduct(id)
            {
                for(var i = 0; i < arrayCompare.length; i++)
                {
                    if(arrayCompare[i]["elementoDiv"] === id)
                    {
                        console.log(arrayCompare[i]);
                        $('#CMP_' + arrayCompare[i]["compania"]).prop('disabled', false);

                        $('#' + arrayCompare[i]["elementoDiv"]).addClass('d-none');
                        $('#' + arrayCompare[i]["elementoImg"]).attr("src", "");
                        $('#' + arrayCompare[i]["elementoName"]).text("");

                        arrayCompare.splice(i , 1);
                        i--;
                    }
                }
                
                if(arrayCompare.length < 1)
                {
                    $("#btnCompare").addClass('d-none');
                    if(arrayCompare.length == 0)
                    {
                        $("#compare").addClass('row d-none');
                    }
                }
            }

            function fnGetCoberturas()
            {
                arrayRequest = [];
                for(var i = 0; i < arrayCompare.length; i++)
                {
                    arrayRequest[i] = arrayCompare[i].productoID;
                }

                var request = {
                    'prospectoID': prospecto,
                    'productos': arrayRequest
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                loader.removeClass("hidden");

                $.ajax({ 
                    type: "POST",
                    url: "{{ url('/obtenercoberturas') }}",
                    data: JSON.stringify(request),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result)
                    {
                        loader.addClass("hidden");
                        fnShowCoberturas(result);
                    },
                    error: function(result)
                    {
                    }
                });
            }

            function fnShowCoberturas(tiposCoberturas)
            {
                $("#tbCoberturas").show();
                $("#tbCotizaciones").hide();
                $("#compare").hide();

                var headCoberturas = $('#headCoberturas');
                var bodyCoberturas = $('#bodyCoberturas');
                var productNumber = arrayCompare.length;
                var columns = productNumber + 1;
                var percent = 100 / columns;

                var th = '<th style="width:' + percent + '%"><div class="sf median blue bold">COBERTURAS</div></th>';
                for(var i = 0; i < arrayRequest.length; i++)
                {   
                    var nombreProducto = "";
                    $.each(response['companias'], function(key, value)
                    {
                        if(value['productoID'] == arrayRequest[i])
                        {
                            nombreProducto = value["producto"].toUpperCase();
                        }
                    });
                    
                    th += '<th style="width:' + percent + '%"><div class="sf median blue bold">' + nombreProducto + '</div></th>';
                }
                headCoberturas.append('<tr>' + th + '</tr>');

                var td = '';
                $.each(tiposCoberturas, function(tipoKey, tipo)
                {
                    td += '<tr><td style="text-align:left;" colspan="' + columns + '">' + '<div class="sf small orange bold">' + tipo['tipoCobertura'] + '</div>' + '</td></tr>';
                    
                    $.each(tipo['coberturas'], function(coberturaKey, cobertura)
                    {
                        td += '<tr>'
                        td += '<td style="width:' + percent + '%; text-align:left;">' + '<div class="sf verysmall bold">' + cobertura['descripcion'] + '</div>' + '</td>';

                        $.each(cobertura['productos'], function(productoKey, producto)
                        {
                            var formatProduct = producto['descripcion'] ? producto['descripcion'] : "";
                            td += '<td style="width:' + percent + '%">' + '<div class="sf verysmall bold">' + formatProduct + '</div>' + '</td>';
                        });

                        td += '</tr>';
                    });

                });

                bodyCoberturas.append('<tr>' + td + '</tr>');
            }

            function fnBackCotizacion()
            {
                $("#tbCoberturas").hide();
                $('#headCoberturas').empty();
                $('#bodyCoberturas').empty();
                $("#tbCotizaciones").show();
                $("#compare").show();
            }
        </script>
    </body>
</html>