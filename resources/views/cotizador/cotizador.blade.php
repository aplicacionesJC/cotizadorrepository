<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="stylesheet" href="{{ URL::asset('css/cotizador.css') }}">
        <title>Cotizador Vehicular</title>
        <link rel="cotizador" href="{{ URL::asset('images/cotizador/logo.min.png') }}" />
    </head>
    <body>
        <div class="loader hidden">
            <img src="{{ URL::asset('images/cotizador/loading.gif') }}" alt="Loading..." />
        </div>
        <div class="step1 bg-color-white">
            <div class="progress">
                <div class="rcc-progress" data-reactroot="">
                    <div class="row">
                        <div id="firstStep" class="col f1 align-center primary bold" onclick="fnGoToStep('step-1-first')">
                            <div class="big">1</div>
                            <div class="sf median">Ingresa tu DNI</div>
                        </div>
                        <div id="secondStep" class="col f1 align-center" onclick="fnGoToStep('step-2')">
                            <div class="big">2</div>
                            <div class="sf median">Busca tu auto</div>
                        </div>
                        <div id="thirdStep" class="col f1 align-center" onclick="fnGoToStep('step-3')">
                            <div class="big">3</div>
                            <div class="sf median">Confirma tus datos</div>
                        </div>
                    </div>
                    <div style="height:2px" class="rcc-progressbar">
                        <div id="transition" style="width:33%;transition:width 400ms ease-out"> </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="views">
                    <div class="box p-20 align-center" data-reactroot="">
                        <div class="rcc-flow-manager">
                            <div id="step-1-first" class="rcc-flow-manager-container is-shown">
                                <div class="view rut-view">
                                    <div class="row">
                                        <div class="col-12">
                                            <h2 class="title m-20-top m-20-bottom">
                                            <div class="wrap-content">
                                                <div>Bienvenido al&nbsp;</div>
                                                <div>Cotizador de Auto&nbsp;</div>
                                                <div>Full Cobertura</div>
                                            </div>
                                            <div class="wrap-content primary">
                                                <div>¡Busquemos el mejor&nbsp;</div>
                                                <div>precio para ti!</div>
                                            </div>
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
                                            <div class="rcc-control rcc-input  no-error undefined">
                                                <div class="content">
                                                    <div class="control-group">
                                                        <div class="input">
                                                            <span class="icon show">
                                                            </span>
                                                            <input class="input-control small bold" id="dni" placeholder="¿ME INDICAS TU DNI?" required="" maxlength="8"/>
                                                        </div>
                                                    </div>
                                                    <div id="dnierror" class="f1 error-message no-error"> </div>
                                                </div>
                                            </div>
                                            <button class="sf small bold pointer p-10-top p-10-bottom p-10-left p-10-right br-pill rcc-control rcc-button" onclick="fnNexStep('step-1-second')">CONTINUAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="step-1-second" class="rcc-flow-manager-container is-hidden">
                                <div class="view rut-view">
                                    <div class="row">
                                        <div class="col-12">
                                            <h2 class="title m-20-top m-20-bottom">
                                                <div class="wrap-content">
                                                    <div>¿Cuál es tu edad?</div>
                                                </div>
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
                                            <div class="rcc-birthdate">
                                                <div class="row no-gutters">
                                                    {{-- <div class="col-12 col-md-3">
                                                        <div class="rcc-control rcc-input  no-error rcc-select">
                                                            <div class="content">
                                                                <div class="control-group">
                                                                    <select id="dia" class="select-control small bold">
                                                                        <option value="0">DIA</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-5">
                                                        <div class="rcc-control rcc-input  no-error rcc-select">
                                                            <div class="content">
                                                                <div class="control-group">
                                                                    <select id="mes" class="Select-control small bold">
                                                                        <option value="0">MES</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <div class="col-12 col-md-12">
                                                        <div class="rcc-control rcc-input  no-error rcc-select">
                                                            <div class="content">
                                                                <div class="control-group">
                                                                    <select id="anio" class="Select-control small bold">
                                                                        <option value="0">EDAD</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="nacimientoerror" class="f1 error-message no-error"> </div> 
                                            <button class="sf small bold pointer p-10-top p-10-bottom p-10-left p-10-right br-pill rcc-control rcc-button" onclick="fnNexStep('step-2')">CONTINUAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="step-2" class="rcc-flow-manager-container is-hidden">
                                <div class="view rut-view">
                                    <div class="row">
                                        <div class="col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
                                            <h2 class="title m-20-top m-20-bottom">
                                                <div>¡Perfecto!</div>
                                                <div class="primary">Cuéntame sobre tu auto</div>
                                            </h2>
                                            <div id="message" class="rcc-control rcc-input no-error rcc-select" hidden>
                                                <div class="content">
                                                    <div class="control-group">
                                                        <div class="select-group">
                                                            <div class="sf small orange bold">Las cotizaciones para provincias aún no estan disponibles, si deseas más información comúnicate con nosotros al siguiente correo <div class="blue">administracion@arias.com.pe</div>  o al <div class="blue"> (01) 711 6393. </div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rcc-control rcc-input no-error rcc-select">
                                                <div class="content">
                                                    <div class="control-group">
                                                        <div class="select-group">
                                                            <select id="marca" class="select-control small bold" onchange="fnChargeModelos(this.options[this.selectedIndex].value)">
                                                                <option value="0">¿CÚAL ES LA MARCA?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rcc-control rcc-input no-error rcc-select">
                                                <div class="content">
                                                    <div class="control-group">
                                                        <div class="select-group">
                                                            <select id="modelo" class="select-control small bold disabled" onchange="fnChargeAnioModelos(this.options[this.selectedIndex].value)" disabled="disabled">
                                                                <option value="0">¿QUÉ MODELO?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rcc-control rcc-input no-error rcc-select">
                                                <div class="content">
                                                    <div class="control-group">
                                                        <div class="select-group">
                                                            <select id="aniomodelo" class="select-control small bold" disabled="disabled" onchange="fnChangeAnioModelo()">
                                                                <option value="0">¿DE QUE AÑO?</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rcc-control rcc-input  no-error undefined">
                                                <div class="content">
                                                    <div class="control-group">
                                                        <div class="input"><span class="icon hide fa fa-fw fa-lg fa-check aria-hidden success"></span>
                                                            <input type="text" class="input-control small bold" id="valoraproximado" placeholder="¿VALOR APROXIMADO (EN DÓLARES)?" required=""/>
                                                        </div>
                                                    </div>
                                                    <div id="valorerror" class="f1 error-message no-error"> </div>
                                                </div>
                                            </div>
                                            <div class="rcc-control rcc-input no-error rcc-select">
                                                <div class="content">
                                                    <div class="control-group">
                                                        <div class="select-group">
                                                            <select id="departamento" class="select-control small bold" onchange="fnChangeDepartamento(this.options[this.selectedIndex].value)">
                                                                <option value="0">DEPARTAMENTO</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="autoerror" class="f1 error-message no-error"> </div> 
                                            <button id="btnStep2" class="sf small bold pointer p-10-top p-10-bottom p-10-left p-10-right br-pill rcc-control rcc-button" onclick="fnNexStep('step-3')">CONTINUAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="step-3" class="rcc-flow-manager-container is-hidden">
                                <div class="view confirm-view">
                                    <div class="row">
                                        <div class="col-md-6 offset-md-3 col-lg-6 offset-lg-3">
                                            <h2 class="title m-20-top m-20-bottom">
                                                <div class="primary">¡Excelente!</div>
                                                <div class="wrap-content big">
                                                    <div>Verifica tus datos</div>
                                                </div>
                                            </h2>
                                            <div id="message2" class="rcc-control rcc-input no-error rcc-select" hidden>
                                                <div class="content">
                                                    <div class="control-group">
                                                        <div class="select-group">
                                                            <div class="sf small orange bold">No se encontraron cotizaciones para tu búsqueda, si deseas más información comúnicate con nosotros al siguiente correo <div class="blue">administracion@arias.com.pe</div>  o al <div class="blue"> (01) 711 6393. </div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
                                            <div class="vehicle-data sf">
                                                <div>
                                                    <div id="marcatext" class="brand sf small bold"></div>
                                                    <div>
                                                        <div id="modelotext" class="model sf small bold"></div>
                                                        <div id="aniotext" class="align-center sf small bold"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="person-data">
                                                <div class="input">
                                                    <div class="rcc-control rcc-input  no-error undefined">
                                                        <div class="content">
                                                            <div class=" control-group">
                                                                <div class="input"><span class="icon hide fa fa-fw fa-lg fa-check aria-hidden success"></span>
                                                                    <input class="input-control small bold" type="text" id="correo" placeholder="¿CUÁL ES TU CORREO?" required="" value="">
                                                                </div>
                                                            </div>
                                                            <div id="correoerror" class="f1 error-message no-error"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input">
                                                    <div class="phone-box">
                                                        <div class="rcc-control rcc-input  no-error undefined">
                                                            <div class="content">
                                                                <div class=" control-group">
                                                                <div class="input"><span class="icon hide fa fa-fw fa-lg fa-check aria-hidden success"></span>
                                                                    <input class="input-control small bold" type="text" id="celular" placeholder="¿CÚAL ES TU CELULAR?" required="" value="" maxlength="9">
                                                                </div>
                                                                </div>
                                                                <div id="celularerror" class="f1 error-message no-error"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="sf small bold pointer p-10-top p-10-bottom p-10-left p-10-right br-pill rcc-control rcc-button" onclick="fnNexStep('step-4')">CONTINUAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="step-4" class="rcc-flow-manager-container is-hidden"></div>
                        </div>
                        <div class="rcc-flow-manager">
                            <div class="row">
                                <div class="col-xs-12 col-lg-10 offset-lg-1">
                                    <img width="100%" height="120px" src="images/compania/seguros.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
        <script type="text/javascript">
            const loader = $(".loader");
            var actualStep = 'step-1-first';
            var request = {
                persona:{
                    documento: '',
                    fecha_nacimiento: new Date()
                },
                prospecto:{
                    correo: '',
                    celular: '',
                    departamento_id: 0, 
                    valor_aproximado: 0, 
                    marca_id: 0,
                    modelo_id: 0,
                    anio_id: 0,
                    marca: '',
                    modelo: '',
                    anio: ''
                }
            };
            var departamentos = [];
            
            /* funcion principal de la transicion del boton */
            function fnNexStep(nextStep){
                switch(nextStep){
                    case 'step-1-second':
                        var element = $('#dnierror');
                        if(fnValidateStep1First()){
                            element.removeClass('has-error').addClass('no-error');
                            request.persona.documento = $("#dni").val();
                            fnChargeBithdayData();
                            fnTrnsitionSet(33);                        
                            fnSetViewStep('step-1-first', nextStep);
                        }
                        else {
                            element.removeClass('no-error').addClass('has-error');
                            element.empty().append("El DNI no es valido");
                        }
                    break;
                    case 'step-2':
                        actualStep = 'step-2';
                        var element = $('#nacimientoerror');
                        if(fnValidateStep1Second()){
                            element.removeClass('has-error').addClass('no-error');
                            fnSetDate();
                            fnChargeDepartamentos();
                            fnChargeMarcas();
                            fnTrnsitionSet(66);
                            fnSetColorStep('firstStep', 'secondStep');
                            fnSetViewStep('step-1-second', nextStep);
                        }
                        else{
                            element.removeClass('no-error').addClass('has-error');
                            element.empty().append("La fecha no es valida");
                        }  
                    break;
                    case 'step-3':
                        actualStep = 'step-3';
                        var element = $('#autoerror');
                        if(fnValidateStep2()){
                            element.removeClass('has-error').addClass('no-error');
                            fnSetDetalle();
                            fnTrnsitionSet(100);
                            fnSetColorStep('secondStep', 'thirdStep');
                            fnSetViewStep('step-2', nextStep);
                        }
                        else{
                            element.removeClass('no-error').addClass('has-error');
                            element.empty().append("Ingresa todos los datos de tu auto");
                        }
                    break;
                    case 'step-4':
                        var validateCorreo = fnValidateStep3Correo();
                        var validateCelular = fnValidateStep3Celular();

                        var element1 = $('#correoerror');
                        var element2 = $('#celularerror');
                        if(validateCorreo && validateCelular){
                            element1.removeClass('has-error').addClass('no-error');
                            element2.removeClass('has-error').addClass('no-error');
                            fnSetDatos();
                            fnSendRequest();
                        }else {
                            if(!validateCorreo){
                                element1.removeClass('no-error').addClass('has-error');
                                element1.empty().append("Correo no valido");
                            }else element1.removeClass('has-error').addClass('no-error');
                            if(!validateCelular){
                                element2.removeClass('no-error').addClass('has-error');
                                element2.empty().append("Celular no valido")
                            }else element2.removeClass('has-error').addClass('no-error');
                        }
                    break;
                }
            }   

            /* #region Validaciones de pasos */
            function fnValidateStep1First(){
                var fieldValidate = $('#dni').val();
                if(fieldValidate === undefined) return false;
                if(fieldValidate.length !== 8) return false;
                if(isNaN(fieldValidate)) return false;
                return true;
            }

            function fnValidateStep1Second(){
                //var fieldDia = $('#dia').val();
                //var fieldMes = $('#mes').val();
                var fieldAnio = $('#anio').val();
                //if(fieldDia === '0') return false;
                //if(fieldMes === '0') return false;
                if(fieldAnio === '0') return false;
                return true;
            }

            function fnValidateStep2(){
                var fieldMarca = $('#marca').val();
                var fieldModelo = $('#modelo').val();
                var fieldAnioModelo = $('#aniomodelo').val();
                var fieldDepartamento = $('#departamento').val();
                var fieldValorAprox = $('#valoraproximado').val();
                if(fieldMarca === '0') return false;
                if(fieldModelo === '0') return false;
                if(fieldAnioModelo === '0') return false;
                if(fieldDepartamento === '0') return false;
                if(isNaN(fieldValorAprox)) return false;
                return true;
            }

            function fnValidateStep3Correo(){
                var fieldValidate = $('#correo').val();
                expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!expr.test(fieldValidate)) return false;
                else return true;
            }

            function fnValidateStep3Celular(){
                var fieldValidate = $('#celular').val();
                if(fieldValidate.length !== 9) return false;
                if(isNaN(fieldValidate)) return false;
                if(fieldValidate[0] !== '9') return false;
                return true;
            }
            /* #endregion */

            /* funcion para setear la clase para la transicion */
            function fnTrnsitionSet(value){
                document.getElementById("transition").style = "width:"+value+"%;transition:width 400ms ease-out"; 
            }

            /* funcion para setear el paso que debe verse */
            function fnSetViewStep(olderStep, newStep){
                document.getElementById(olderStep).className = "rcc-flow-manager-container is-hidden"; 
                document.getElementById(newStep).className = "rcc-flow-manager-container is-shown"; 
            }

            /* funcion para colorear el step en el que estamos */
            function fnSetColorStep(olderStep, newStep){
                document.getElementById(olderStep).className = "col f1 align-center"; 
                document.getElementById(newStep).className = "col f1 align-center primary bold"; 
            }

            /* funcion para cargar las fechas de nacimiento */
            function fnChargeBithdayData(){
                /*var arrayMes=[
                    { value: 01, name: 'ENERO' },
                    { value: 02, name: 'FEBRERO' },
                    { value: 03, name: 'MARZO' },
                    { value: 04, name: 'ABRIL' },
                    { value: 05, name: 'MAYO' },
                    { value: 06, name: 'JUNIO' },
                    { value: 07, name: 'JULIO' },
                    { value: 08, name: 'AGOSTO' },
                    { value: 09, name: 'SETIEMBRE' },
                    { value: 10, name: 'OCTUBRE' },
                    { value: 11, name: 'NOVIEMBRE' },
                    { value: 12, name: 'DICIEMBRE' }
                ];

                var elementDia = $('#dia');
                for(var iter = 1; iter <= 31; iter++){
                    elementDia.append("<option value='" + iter + "'>" + iter + "</option>");
                }

                var elementMes = $('#mes');
                $.each(arrayMes, function(index, element){
                    elementMes.append("<option value='" + element.value + "'>" + element.name + "</option>");
                })*/

                var elementAnio = $('#anio');
                for(var iter = 18; iter <= 99 ; iter++){
                    elementAnio.append("<option value='" + iter + "'>" + iter + "</option>");
                }
            }
           
            /* funcion para guardar la fecha */
            function fnSetDate(){
                var elementDia = 01; //$('#dia').val();
                var elementMes = 01; //$('#mes').val();
                var elementAnio = (new Date()).getFullYear() - $('#anio').val();
                request.persona.fecha_nacimiento = new Date(elementAnio, elementMes, elementDia);
            }

            /* funcion para cargar los departamentos */
            function fnChargeDepartamentos(){
                $.ajax({ 
                    type: "GET",
                    url:"{{url('/departamentos')}}",
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result){
                        departamentos = result;
                        var elementDepartamento = $('#departamento');
                        $.each(result, function(key, value){
                            elementDepartamento.append("<option value='" + key + "'>" + value + "</option>")
                        });
                    },
                    error: function(result){
                        // Nada por ahora
                    }
                });
            }

            /* funcion para cargar las marcas */
            function fnChargeMarcas(){
                loader.removeClass("hidden");
                $.ajax({ 
                    type: "GET",
                    url:"{{url('/marcas')}}",
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result){
                        var elementMarca = $('#marca');
                        $.each(result, function(key, value){
                            elementMarca.append("<option value='" + key + "'>" + value + "</option>")
                        });
                        loader.addClass("hidden"); // class "loader hidden"
                    },
                    error: function(result){
                        // Nada por ahora
                    }
                });
            }

            /* funcion para cargar los modelos por marca */
            function fnChargeModelos(marcaId){
                $("#message2").hide();
                loader.removeClass("hidden");
                $('#modelo').prop('disabled', false);
                $.ajax({ 
                    type: "GET",
                    url: "{{url('/modelos')}}/" + marcaId,
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result){
                        var elementModelo = $('#modelo');
                        elementModelo.empty().append("<option value='0'>¿QUÉ MODELO?</option>");
                        $.each(result, function(key, value){
                            elementModelo.append("<option value='" + key + "'>" + value + "</option>");
                        });
                        loader.addClass("hidden");
                    },
                    error: function(result){
                        // Nada por ahora
                    }
                });
            }

            /* funcion para cargar los años por modelo */
            function fnChargeAnioModelos(modeloId){
                $("#message2").hide();
                $("#btnStep2").show();
                loader.removeClass("hidden");
                $('#aniomodelo').prop('disabled', false);
                $.ajax({ 
                    type: "GET",
                    url: "{{url('/aniomodelos')}}/" + modeloId,
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result){
                        var elementModelo = $('#aniomodelo');
                        elementModelo.empty().append("<option value='0'>¿DE QUE AÑO?</option>");
                        $.each(result, function(key, value){
                            elementModelo.append("<option value='" + key + "'>" + value + "</option>")
                        });
                        loader.addClass("hidden");
                    },
                    error: function(result){
                        // Nada por ahora
                    }
                });
            }

            /* funcion para guardar el detalle */
            function fnSetDetalle(){
                request.prospecto.departamento_id = parseInt($('#departamento').val());
                request.prospecto.marca_id = parseInt($('#marca').val());
                request.prospecto.modelo_id = parseInt($('#modelo').val());
                request.prospecto.anio_id = parseInt($('#aniomodelo').val());
                request.prospecto.valor_aproximado = parseInt($('#valoraproximado').val());
                request.prospecto.marca = $("#marca option:selected").text();
                request.prospecto.modelo = $("#modelo option:selected").text();
                request.prospecto.anio = $("#aniomodelo option:selected").text();

                $('#marcatext').empty().append($("#marca option:selected").text());
                $('#modelotext').empty().append($("#modelo option:selected").text());
                $('#aniotext').empty().append($("#aniomodelo option:selected").text());
            }

            /* funcion para guardar los datos de contacto */
            function fnSetDatos(){
                request.prospecto.correo = $("#correo").val();
                request.prospecto.celular = $("#celular").val();
            }

            /* funcion encargada de enviar el correo y pasar nueva pagina de cotizados */
            function fnSendRequest(){
                loader.removeClass("hidden");
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax({ 
                    type: "POST",
                    url: "{{url('/solicitarcotizacion')}}",
                    data: JSON.stringify(request),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result){
                        console.log(result);
                        window.location.href = "{{url('/resultado?prospecto=')}}"+ result.id_prospecto;
                        loader.addClass("hidden");
                    },
                    error: function(result){
                        loader.addClass("hidden");
                        $("#message2").show();
                    }
                });
            }

            /* funcion para ir a un paso directamente */
            function fnGoToStep(step){
                switch(step){
                    case 'step-1-first':
                    if(actualStep != 'step-1-first' && actualStep != 'step-1-second'){
                        fnTrnsitionSet(33);
                        fnSetViewStep(actualStep, step);
                        if(actualStep == 'step-2') fnSetColorStep('secondStep', 'firstStep');
                        else fnSetColorStep('thirdStep', 'firstStep');
                        actualStep = step;
                    }
                    break;
                    case 'step-2':
                    if(actualStep != 'step-1-first' && actualStep != 'step-1-second' && actualStep != 'step-2'){
                        fnTrnsitionSet(66);
                        fnSetViewStep(actualStep, step);
                        fnSetColorStep('thirdStep', 'secondStep');
                        actualStep = step;
                    }
                    break;
                }
            }

            function fnChangeDepartamento(departamentoId)
            {
                var selected = $("#departamento option:selected").text();
                if(selected.toUpperCase() === 'PROVINCIA')
                {
                    $("#message").show();
                    $("#btnStep2").hide();
                }
                else
                {
                    $("#message").hide();
                    $("#btnStep2").show();
                }
            }

            function fnChangeAnioModelo(){
                $("#message2").hide();
            }

        </script>
    </body>

</html>