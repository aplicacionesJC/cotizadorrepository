@extends('layout.master') 

@section('styles')
<link rel="stylesheet" href="{{ URL::asset('css/jquery.steps.css') }}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('tipo.store') !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Tipo de Concepto</h5>
        {!! Form::open(['route' => 'tipo.store', 'class' => 'form']) !!}
            <div class="form-row">
                <div class="form-group col-md-9">
                    {!! Form::label('descripcion', 'Descripción', ['class' => 'control-label']) !!}
                    {!! Form::text('descripcion', null, ['class' => $errors->has('descripcion') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('descripcion') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('orden', 'Orden', ['class' => 'control-label']) !!}
                    {!! Form::text('orden', null, ['class' => $errors->has('orden') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('orden') }}</div>
                </div>       
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection