@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('tipo.update', $tipoConcepto) !!}
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Modelo</h5>
        {!! Form::model($tipoConcepto, ['method' => 'PATCH', 'route' => ['tipo.update', $tipoConcepto->id]]) !!}
            <div class="form-row">
                <div class="form-group col-md-9">
                    {!! Form::label('descripcion', 'Descripcion', ['class' => 'control-label']) !!}
                    {!! Form::text('descripcion', $tipoConcepto->descripcion, ['class' => $errors->has('descripcion') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('descripcion') }}</div>
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('orden', 'Orden', ['class' => 'control-label']) !!}
                    {!! Form::text('orden', $tipoConcepto->orden, ['class' => $errors->has('orden') ? 'form-control is-invalid' : 'form-control']) !!}
                    <div class="invalid-feedback">{{ $errors->first('orden') }}</div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-content-save-outline"></i>Guardar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('scripts')
@endsection