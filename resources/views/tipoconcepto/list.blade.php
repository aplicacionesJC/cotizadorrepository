
@extends('layout.master') 

@section('styles')
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('tipo.index') !!}
@endsection

@section('content')
<div class="card border border-primary">
    <div class="card-body">
        <h5 class="card-title">BÚSQUEDA</h5>
        {!! Form::open(['method' => 'GET', 'route' => 'tipo.index']) !!}
            <div class="form-row">
                <div class="form-group col-md-12">
                    {!! Form::label('descripcion', 'Nombre', ['class' => 'control-label']) !!}
                    {!! Form::text('descripcion', Request::get('descripcion'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-dark float-right m-1"><i class="mdi mdi-magnify"></i>Buscar</button>
                <a href="{{ URL::asset('tipo/create') }}" class="btn btn-dark float-right m-1"><i class="mdi mdi-plus"></i>Crear</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<br />

@if (!$tiposconcepto->isEmpty())
    @foreach ($tiposconcepto as $index => $item)
        <div class="card">
            <div class="card-header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                             {{ strtoupper($item->descripcion) }}  
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <button class="btn btn-danger float-right btn-sm m-1" onclick="change_state({{ $item->id }})"><i class="mdi mdi-delete-forever"></i>Eliminar</button>
                            <a href="{{ URL::asset('tipo/'.$item->id.'') }}" class="btn btn-dark float-right btn-sm m-1"><i class="mdi mdi-tooltip-edit"></i>Ver detalle</a>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-5 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">TIPO DE CONCEPTO</div>
                                <div class="col-sm-7 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->descripcion) }}</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-5 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: bold;">ORDEN</div>
                                <div class="col-sm-7 col-xs-12" style="font-family: verdana;font-size: 12px;color: #4c4c4c;font-weight: normal;">{{ strtoupper($item->orden) }}</div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <br />
    @endforeach
    <div class="col-xs-12">
        {{ $tiposconcepto->appends(Request::input())->links("pagination::bootstrap-4") }}
    </div> 
@else
<div class="card">
    <div class="card-body">
        <p>No se encontraron resultados.</p>
    </div>
</div>
@endif

@endsection



@section('scripts')

<script type="text/javascript">

    function change_state(tipoConceptoId){
        if(!confirm('Desea eliminar el registro?')) return;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
             
        $.ajax({
            url: "{{ url('/tipo') }}/" + tipoConceptoId ,
            type: 'POST',
            data: {
                '_method': 'DELETE'
            },
            success: function(result) {
                document.location = document.location;
            }
        });
    };

</script>

@endsection