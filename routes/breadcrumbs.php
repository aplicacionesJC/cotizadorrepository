<?php

/*** INICIO ***/
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('INICIO', URL::route('home'));
});

/*** USUARIOS ***/
Breadcrumbs::register('usuario.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('USUARIOS', URL::route('usuario.index'));
});

Breadcrumbs::register('usuario.store', function($breadcrumbs)
{
    $breadcrumbs->parent('usuario.index');
    $breadcrumbs->push('CREAR', URL::route('usuario.store'));
});

Breadcrumbs::register('usuario.update', function($breadcrumbs, $usuario)
{
    $breadcrumbs->parent('usuario.index');
    $breadcrumbs->push($usuario->nombre.' / EDITAR', URL::route('usuario.update', $usuario->id));
});

/*** COMPAÑIAS ***/
Breadcrumbs::register('compania.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('COMPAÑIAS', URL::route('compania.index'));
});

Breadcrumbs::register('compania.store', function($breadcrumbs)
{
    $breadcrumbs->parent('compania.index');
    $breadcrumbs->push('CREAR', URL::route('compania.store'));
});

Breadcrumbs::register('compania.update', function($breadcrumbs, $compania)
{
    $breadcrumbs->parent('compania.index');
    $breadcrumbs->push($compania->nombre.' / EDITAR', URL::route('compania.update', $compania->id));
});

/*** PRODUCTOS ***/
Breadcrumbs::register('producto.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('PRODUCTOS', URL::route('producto.index'));
});

Breadcrumbs::register('producto.store', function($breadcrumbs)
{
    $breadcrumbs->parent('producto.index');
    $breadcrumbs->push('CREAR', URL::route('producto.store'));
});

Breadcrumbs::register('producto.update', function($breadcrumbs, $producto)
{
    $breadcrumbs->parent('producto.index');
    $breadcrumbs->push($producto->nombre.' / EDITAR', URL::route('producto.update', $producto->id));
});

/*** TIPOS DE CONCEPTOS ***/
Breadcrumbs::register('tipo.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('TIPOS DE CONCEPTO', URL::route('tipo.index'));
});

Breadcrumbs::register('tipo.store', function($breadcrumbs)
{
    $breadcrumbs->parent('tipo.index');
    $breadcrumbs->push('CREAR', URL::route('tipo.store'));
});

Breadcrumbs::register('tipo.update', function($breadcrumbs, $tipo)
{
    $breadcrumbs->parent('tipo.index');
    $breadcrumbs->push($tipo->descripcion.' / EDITAR', URL::route('tipo.update', $tipo->id));
});

/*** CONCEPTOS ***/
Breadcrumbs::register('concepto.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('CONCEPTOS', URL::route('concepto.index'));
});

Breadcrumbs::register('concepto.store', function($breadcrumbs)
{
    $breadcrumbs->parent('concepto.index');
    $breadcrumbs->push('CREAR', URL::route('concepto.store'));
});

Breadcrumbs::register('concepto.update', function($breadcrumbs, $concepto)
{
    $breadcrumbs->parent('concepto.index');
    $breadcrumbs->push($concepto->descripcion.' / EDITAR', URL::route('concepto.update', $concepto->id));
});

/*** DEDUCIBLES ***/
Breadcrumbs::register('deducible.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('DETALLES DE CONCEPTOS', URL::route('deducible.index'));
});

Breadcrumbs::register('deducible.store', function($breadcrumbs)
{
    $breadcrumbs->parent('deducible.index');
    $breadcrumbs->push('CREAR', URL::route('deducible.store'));
});

Breadcrumbs::register('deducible.update', function($breadcrumbs, $deducible)
{
    $breadcrumbs->parent('deducible.index');
    $breadcrumbs->push(strtoupper($deducible->descripcion).' / EDITAR', URL::route('deducible.update', $deducible->id));
});

// Breadcrumbs::register('conceptos.index', function($breadcrumbs)
// {
//     $breadcrumbs->parent('home');
//     $breadcrumbs->push('CONCEPTOS', URL::route('conceptos.index'));
// });

// Breadcrumbs::register('deducibles.store', function($breadcrumbs, $producto)
// {
//     $breadcrumbs->parent('conceptos.index', $producto);
//     $breadcrumbs->push('CREAR DEDUCIBLE', URL::route('deducibles.store', $producto));
// });

/*** MARCAS ***/
Breadcrumbs::register('marca.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('MARCAS', URL::route('marca.index'));
});

Breadcrumbs::register('marca.store', function($breadcrumbs)
{
    $breadcrumbs->parent('marca.index');
    $breadcrumbs->push('CREAR', URL::route('marca.store'));
});

Breadcrumbs::register('marca.update', function($breadcrumbs, $marca)
{
    $breadcrumbs->parent('marca.index');
    $breadcrumbs->push($marca->nombre.' / EDITAR', URL::route('marca.update', $marca->id));
});

/*** MODELOS ***/
Breadcrumbs::register('modelo.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('MODELOS', URL::route('modelo.index'));
});

Breadcrumbs::register('modelo.store', function($breadcrumbs)
{
    $breadcrumbs->parent('modelo.index');
    $breadcrumbs->push('CREAR', URL::route('modelo.store'));
});

Breadcrumbs::register('modelo.update', function($breadcrumbs, $modelo)
{
    $breadcrumbs->parent('modelo.index');
    $breadcrumbs->push($modelo->nombre.' / EDITAR', URL::route('modelo.update', $modelo->id));
});


/*** PRODUCTOS POR MODELO ***/
Breadcrumbs::register('modprod.index', function($breadcrumbs, $modelo)
{
    $breadcrumbs->parent('modelo.update', $modelo);
    $breadcrumbs->push('PRODUCTOS', URL::route('modelo.update', $modelo));
});

Breadcrumbs::register('modprod.store', function($breadcrumbs, $modelo)
{
    $breadcrumbs->parent('modelo.update', $modelo);
    $breadcrumbs->push('CREAR', URL::route('modelo.store', $modelo));
});

/*** TASAS ***/
Breadcrumbs::register('tasa.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('TASAS', URL::route('tasa.index'));
});

Breadcrumbs::register('tasa.store', function($breadcrumbs)
{
    $breadcrumbs->parent('tasa.index');
    $breadcrumbs->push('CREAR', URL::route('tasa.store'));
});

Breadcrumbs::register('tasa.update', function($breadcrumbs, $tasa)
{
    $breadcrumbs->parent('tasa.index');
    $breadcrumbs->push($tasa->id.' - '.$tasa->antiguedad.' / EDITAR', URL::route('tasa.update', $tasa->id));
});

/*** CUOTAS ***/
Breadcrumbs::register('cuota.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('CUOTAS', URL::route('cuota.index'));
});

Breadcrumbs::register('cuota.store', function($breadcrumbs)
{
    $breadcrumbs->parent('cuota.index');
    $breadcrumbs->push('CREAR', URL::route('cuota.store'));
});

Breadcrumbs::register('cuota.update', function($breadcrumbs, $cuota)
{
    $breadcrumbs->parent('cuota.index');
    $breadcrumbs->push($cuota->meses.' / EDITAR', URL::route('cuota.update', $cuota->id));
});

/*** CONCEPTOS ***/
// Breadcrumbs::register('tipos.index', function($breadcrumbs, $modelo)
// {
//     $breadcrumbs->parent('tipo.index');
//     $breadcrumbs->push($modelo->nombre.' / PRODUCTOS', URL::route('productos.index', $modelo));
// });

/*** PROSPECTOS ***/
Breadcrumbs::register('prospecto.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('COTIZACIONES', URL::route('prospecto.index'));
});

Breadcrumbs::register('prospecto.show', function($breadcrumbs, $prospecto)
{
    $breadcrumbs->parent('prospecto.index');
    $breadcrumbs->push(str_pad($prospecto['codigo_prospecto'], 8, '0', STR_PAD_LEFT).' / DETALLE', URL::route('prospecto.show', $prospecto['codigo_prospecto']));
});

/*** CONFIGURACIONES ***/
Breadcrumbs::register('configuracion.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('CONFIGURACIONES', URL::route('configuracion.index'));
});

Breadcrumbs::register('configuracion.store', function($breadcrumbs)
{
    $breadcrumbs->parent('configuracion.index');
    $breadcrumbs->push('CREAR', URL::route('configuracion.store'));
});

Breadcrumbs::register('configuracion.update', function($breadcrumbs, $configuracion)
{
    $breadcrumbs->parent('configuracion.index');
    $breadcrumbs->push($configuracion->codigo_grupo.' / EDITAR', URL::route('configuracion.update', $configuracion->id));
});

/*** CONTRASEÑAS ***/
Breadcrumbs::register('cambiarcontrasenia', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('CAMBIAR CONTRASEÑA', URL::route('cambiarcontrasenia'));
});

