<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/* Rutas de Autenticacion */
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');
//$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//$this->post('register', 'Auth\RegisterController@register');

/* Rutas de Cotizacion */
/* GET */
Route::get('/', function () { return View::make('cotizador.cotizador'); });
Route::get('/departamentos', 'Cotizador\CotizadorController@getDepartamentos');
Route::get('/marcas', 'Cotizador\CotizadorController@getMarcas');
Route::get('/modelos/{marcaId}', 'Cotizador\CotizadorController@getModelosByMarcas');
Route::get('/aniomodelos/{modeloId}', 'Cotizador\CotizadorController@getAniosByModelo');
Route::get('/resultado', function () { return View::make('cotizador.resultadovip'); });
Route::get('/prospectoinfo/{prospectoId}', 'Cotizador\CotizadorController@GetProspectoById');
Route::get('/companias','Cotizador\CotizadorController@getCompanias');

/* POST */
Route::post('/solicitarcotizacion', 'Cotizador\CotizadorController@postEnviarCotizacion');
Route::post('/cambiarsumaasegurada', 'Cotizador\CotizadorController@postCambiarCotizacion');
Route::post('/obtenercoberturas', 'Cotizador\CotizadorController@postObtenerCoberturas');

/* Rutas de Administracion */
Route::get('home', 'HomeController@index')->name('home');
Route::resource('compania', 'Administrador\CompaniaController');
Route::resource('prospecto', 'Administrador\ProspectoController', ['only' => ['index', 'show']]);
Route::resource('marca', 'Administrador\MarcaController');
Route::resource('modelo', 'Administrador\ModeloController');
Route::resource('producto', 'Administrador\ProductoController');
Route::resource('tipo', 'Administrador\TipoConceptoController');
Route::resource('concepto', 'Administrador\ConceptoController');
Route::resource('deducible', 'Administrador\DeducibleController');
Route::resource('configuracion', 'Administrador\ConfiguracionController');
Route::resource('usuario', 'Administrador\UsuarioController');
Route::resource('tasa', 'Administrador\TasaController');
Route::resource('cuota', 'Administrador\CuotaController');
Route::resource('modelo/{modeloId}/productos', 'Administrador\ProductoModeloController');
//Route::resource('producto/{productoId}/beneficios', 'Administrador\DeducibleProductoController');
//Route::resource('producto/{productoId}/deducibles', 'Administrador\DeducibleController');
//Route::resource('deducibleproducto', 'Administrador\DeducibleProductoController');
Route::get('/conceptosportipo', 'Administrador\ConceptoController@getConceptosByTipo');
Route::get('/producto/tasa/{productoId}', 'Administrador\TasaController@getTasasByProducto');
Route::get('/prospecto/pdf/{prospectoId}', 'Administrador\ProspectoController@getDownloadPdf');
Route::get('/prospecto/correo/{prospectoId}', 'Administrador\ProspectoController@getFormatoCorreo');
Route::post('/prospecto/correo/{prospectoId}', 'Administrador\ProspectoController@postEnviarCorreo');
Route::get('/cambiarcontrasenia', function () { return View::make('contrasenia.cambiar'); })->name('cambiarcontrasenia');
Route::post('/cambiarcontrasenia', 'Administrador\UsuarioController@changePassword')->name('cambiarcontrasenia');
Route::post('/prospecto/compra', 'Administrador\ProspectoController@postSolicitarCompra');
Route::get('/marcas/lista', 'Administrador\MarcaController@getMarcaModal');
Route::get('/pruebapdf', function () { return View::make('pdf.cotizacionPDF2'); });
