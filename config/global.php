<?php

return [

    'ESTADO_COTIZACION' => ['' => 'SELECCIONAR', 1 => 'PENDIENTE', 2 => 'EN SOLICITUD', 3 => 'COMPRADA'],
    'PROCEDENCIA' => ['' => 'SELECCIONAR', 0 => 'OTRO', 1 => 'CHINO']
];

?>