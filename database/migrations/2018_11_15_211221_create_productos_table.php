<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('compania_id')->unsigned();
            $table->foreign('compania_id')->references('id')->on('compania');
            $table->string('nombre', 100);
            $table->string('abreviatura', 50);
            $table->decimal('prima_minima', 9, 2);
            $table->decimal('tope_gps', 9, 2)->nullable();
            $table->decimal('comision', 9, 2)->nullable();
            $table->decimal('descuento', 9, 2)->nullable();
            $table->integer('estado')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
