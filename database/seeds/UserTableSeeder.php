<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Rol;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Rol::where('nombre', 'Admin')->first();
        $role_admin = Rol::where('nombre', 'Venta')->first();

        $user = new User();
        $user->nombre = 'Usuario1';
        $user->email = 'usuario1@ejemplo.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->nombre = 'Usuario2';
        $user->email = 'usuario2@ejemplo.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_admin);
    }
}
