<?php

use Illuminate\Database\Seeder;
use App\Rol;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Rol();
        $role->nombre = 'Admin';
        $role->descripcion = 'Administrador';
        $role->save();


        $role = new Rol();
        $role->nombre = 'Venta';
        $role->descripcion = 'Venta';
        $role->save();
    }
}
