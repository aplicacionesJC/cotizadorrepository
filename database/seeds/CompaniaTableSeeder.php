<?php

use Illuminate\Database\Seeder;
use App\Compania;

class CompaniaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $compania = new Compania();
        $compania->nombre = 'Mapfre';
        $compania->abreviatura = 'Mapfre';
        $compania->logo = '';
        $compania->estado = 1;
        $compania->save();


        $compania = new Compania();
        $compania->nombre = 'La Positiva';
        $compania->abreviatura = 'Positiva';
        $compania->logo = '';
        $compania->estado = 1;
        $compania->save();

        $compania = new Compania();
        $compania->nombre = 'Rimac';
        $compania->abreviatura = 'Rimac';
        $compania->logo = '';
        $compania->estado = 1;
        $compania->save();

        $compania = new Compania();
        $compania->nombre = 'Pacifico';
        $compania->abreviatura = 'Pacifico';
        $compania->logo = '';
        $compania->estado = 1;
        $compania->save();

        $compania = new Compania();
        $compania->nombre = 'HDI';
        $compania->abreviatura = 'HDI';
        $compania->logo = '';
        $compania->estado = 1;
        $compania->save();
    }
}
