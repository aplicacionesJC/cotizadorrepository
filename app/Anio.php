<?php

namespace App;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Model;

class Anio extends BaseModel
{
    // use Notifiable;
    // use SoftDeletes;

    /*
    *   Nombre de la tabla en BD
    */
    protected $table = 'anios';

    /**
     * Fields dates
     *
     * @var array
     */
    protected $date = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['nombre', 'estado'];

    public function anioModelos()
    {
        return $this->hasMany(AnioModelo::class);
    }

    public function modelos()
    {
        return $this->belongsToMany(Modelo::class, 'anio_modelos', 'anio_id', 'modelo_id');
    }
}
