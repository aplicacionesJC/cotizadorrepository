<?php

namespace App;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Model;

class Modelo extends BaseModel
{
    // use Notifiable;
    // use SoftDeletes;

    /*
    *   Nombre de la tabla en BD
    */
    protected $table = 'modelos';

    /**
     * Fields dates
     *
     * @var array
     */
    protected $date = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['marca_id', 'nombre','valor_referencial', 'estado'];

    public function marca()
    {
        return $this->belongsTo(Marca::class);
    }

    public function anioModelos()
    {
        return $this->hasMany(AnioModelo::class);
    }

    public function anios()
    {
        return $this->belongsToMany(Anio::class, 'anio_modelos', 'modelo_id', 'anio_id');
    }
}
