<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Parametro extends BaseModel
{
    
    /*
    *   Nombre de la tabla en BD
    */
    protected $table = 'parametros';

    /**
     * Fields dates
     *
     * @var array
     */
    protected $date = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['codigo_grupo', 'codigo','descripcion', 'valor_texto', 'valor_numerico', 'estado'];
}
