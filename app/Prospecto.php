<?php

namespace App;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Model;

class Prospecto extends BaseModel
{
    // use Notifiable;
    // use SoftDeletes;

    /*
    *   Nombre de la tabla en BD
    */
    protected $table = 'prospectos';

    /**
     * Fields dates
     *
     * @var array
     */
    protected $date = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['departamento_id', 'persona_id', 'anio_id', 'marca_id', 'modelo_id', 'valor_aproximado','fecha_cotizacion', 'correo', 'celular', 'estado', 'cotizaciones_id'];
}
