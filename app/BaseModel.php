<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use Notifiable;
    use SoftDeletes;

    public static function create(array $attributes = [])
    {
        if(count($attributes) > 0) $attributes['estado'] = 1;
        $model = static::query()->create($attributes);
        return $model;
    }

}
