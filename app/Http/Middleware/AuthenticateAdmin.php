<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateAdmin
{
    public function handle($request, Closure $next, $guard = null){
        if(Auth::check() && Auth::user()->hasRole('Admin')){
            if($request->ajax() || $request->wantsJson()){
                return response('Unauthorized.', 401);
            }else{
                return response(view('errors.403'), 403);
            }
        }

        return $next($request);
    }
}
