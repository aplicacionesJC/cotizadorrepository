<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarcaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'nombre' => 'required|between:1,70',
                    'es_chino' => 'required|integer'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'nombre' => 'required|between:1,70',
                    'es_chino' => 'required|integer'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'nombre' => 'nombre',
            'es_chino' => 'procedencia'
        ];
    }
}
