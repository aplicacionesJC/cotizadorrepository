<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeducibleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'tipo_adicional_id' => 'required|integer',
                    'adicional_id' => 'required|integer',
                    'producto_id' => 'required|integer',
                    'descripcion' => 'required|between:1,400',
                    'flag_resumen' => 'required|integer'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'tipo_adicional_id' => 'required|integer',
                    'adicional_id' => 'required|integer',
                    'producto_id' => 'required|integer',
                    'descripcion' => 'required|between:1,400',
                    'flag_resumen' => 'required|integer'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'tipo_adicional_id' => 'tipo de concepto',
            'adicional_id' => 'concepto',
            'descripcion' => 'descripción del deducible',
            'producto_id' => 'producto'
        ];
    }
}
