<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParametroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'codigo_grupo' => 'required|between:4,50|regex:/^\S*$/u',
                    'codigo' => 'required|between:4,50',
                    'descripcion' => 'required|between:4,50',
                    'valor_texto' => 'required'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'codigo_grupo' => 'required|between:4,50',
                    'codigo' => 'required|between:4,50',
                    'descripcion' => 'required|between:4,50',
                    'valor_texto' => 'required'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'codigo_grupo' => 'còdigo de grupo',
            'codigo' => 'codigo',
            'descripcion' => 'descripcion',
            'valor_texto' => 'valor'
        ];
    }
}
