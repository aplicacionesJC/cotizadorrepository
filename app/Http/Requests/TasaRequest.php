<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TasaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'producto_id' => 'required|integer',
                    'antiguedad' => 'required|integer|between:0,'.date('Y'),
                    'porcentaje' => 'required|numeric|between:0,100'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'producto_id' => 'required|integer',
                    'antiguedad' => 'required|integer|between:0,'.date('Y'),
                    'porcentaje' => 'required|numeric|between:0,100'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'producto_id' => 'producto'
        ];
    }
}
