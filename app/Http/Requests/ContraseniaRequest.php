<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContraseniaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|current_password',
            'password' => 'required|confirmed|between:8,20'
        ];
    }

    public function attributes()
    {
        return [
            'old_password' => 'Contraseña actual',
            'password' => 'Contraseña nueva',
            'password_confirmation' => 'repetir contraseña nueva'
        ];
    }
}
