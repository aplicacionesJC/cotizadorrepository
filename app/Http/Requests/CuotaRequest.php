<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CuotaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'compania_id' => 'required|integer',
                    'meses' => 'required|integer|between:1,12',
                    'factor' => 'required|numeric'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'compania_id' => 'required|integer',
                    'meses' => 'required|integer|between:1,12',
                    'factor' => 'required|numeric'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'compania_id' => 'compañia'
        ];
    }
}
