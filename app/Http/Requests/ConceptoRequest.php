<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConceptoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'tipo_adicional_id' => 'required',
                    'descripcion' => 'required|between:2,50',
                    'orden' => 'required|integer'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'tipo_adicional_id' => 'required',
                    'descripcion' => 'required|between:2,50',
                    'orden' => 'required|integer'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'tipo_adicional_id' => 'tipo de concepto',
            'descripcion' => 'descripción del concepto',
            'orden' => 'orden'
        ];
    }
}
