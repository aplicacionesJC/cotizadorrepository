<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModeloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'nombre' => 'required|between:1,70',
                    'marca_id' => 'required|integer',
                    'valor_referencial' => 'required|numeric',
                    'anio' => 'required|array'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'nombre' => 'required|between:1,70',
                    'marca_id' => 'required|integer',
                    'valor_referencial' => 'required|numeric',
                    'anio' => 'required|array'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'nombre' => 'nombre',
            'marca_id' => 'marca',
            'valor_referencial' => 'valor aproximado',
            'anio' => 'año'
        ];
    }
}
