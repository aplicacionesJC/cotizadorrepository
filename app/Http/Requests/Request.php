<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    
	/**
	 * Get the response for a forbidden operation
	 * 
	 * @var \Illuminate\Http\Response
	 */
    public function forbiddenResponse()
    {
    	return Response(view('error.403'), 403);
    }

    
}