<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TipoConceptoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'descripcion' => 'required|between:1,70',
                    'orden' =>  ['required', 'integer', Rule::unique('tipo_adicionales')->whereNull('deleted_at') ],
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'descripcion' => 'required|between:1,70',
                    'orden' =>  ['required', 'integer', Rule::unique('tipo_adicionales')->whereNull('deleted_at') ],
                ];
            default:break;
        }
    }
}
