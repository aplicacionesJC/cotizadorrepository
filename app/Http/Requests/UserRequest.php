<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'nombre' => 'required|between:8,50',
                    'email' => 'required|email|unique:users|between:8,50',
                    'password' => 'required|confirmed|between:8,20',
                    'rol' => 'required|array'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'nombre' => 'required|between:8,50',
                    'email' => 'required|between:8,50',
                    'password' => 'confirmed|between:0,20',
                    'rol' => 'required|array'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'email' => 'correo',
            'password' => 'Contraseña',
            'password_confirmation' => 'repetir contraseña'
        ];
    }
}
