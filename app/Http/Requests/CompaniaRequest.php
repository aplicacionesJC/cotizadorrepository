<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompaniaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Override method of validation
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if ($this->has('logo')) 
        {
            if(is_string($this->all()['logo']))
            {
                $this->merge(['logo'=> null ]);
            }
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ab = $this->all()['abreviatura'];
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'nombre' => 'required|between:1,100',
                    'abreviatura' => ['required', 'between:1,50', Rule::unique('companias')->whereNull('deleted_at') ],
                    'logo' => 'required|image'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'nombre' => 'required|between:1,100',
                    'abreviatura' => 'required|between:1,50',
                    'logo' => 'nullable|image'
                ];
            default:break;
        }
    }
}
