<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'compania_id' => 'required|integer',
                    'nombre' => 'required|between:1,70',
                    'abreviatura' => 'required|between:1,50',
                    'prima_minima' => 'required|numeric',
                    'tope_gps' => 'required|numeric',
                    'comision' => 'required|numeric|between:0,100',
                    'descuento' => 'required|numeric|between:0,100'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'compania_id' => 'required|integer',
                    'nombre' => 'required|between:1,70',
                    'abreviatura' => 'required|between:1,50',
                    'prima_minima' => 'required|numeric',
                    'tope_gps' => 'required|numeric',
                    'comision' => 'required|numeric|between:0,100',
                    'descuento' => 'required|numeric|between:0,100'
                ];
            default:break;
        }
    }

    public function attributes()
    {
        return [
            'compania_id' => 'compañia',
            'tope_gps' => 'tope de gps'
        ];
    }
}
