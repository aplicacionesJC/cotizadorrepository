<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Administrador\GenericController;
use DB;
use Session;
use App\Compania;
use App\Producto;
use App\Http\Requests\CompaniaRequest;

class CompaniaController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $companias = Compania::where(function($query) use($request)
                                    {
                                        if(!empty($request['abreviatura']))
                                            $query->where('abreviatura', 'like', '%'.$request['abreviatura'].'%');
                                        
                                        if(!empty($request['nombre']))
                                            $query->where('nombre', 'like', '%'.$request['nombre'].'%');
                                    })
                                    ->orderBy('abreviatura', 'ASC')
                                    ->paginate(10);
                                        
        return view('compania.list', ['companias' => $companias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('compania.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CompaniaRequest $request)
    {   
        $extension = $request->file('logo')->getClientOriginalExtension(); 
        $imageName = $request->get('abreviatura').'.'.$extension;

        $compania = new Compania(array(
            'abreviatura' => $request->get('abreviatura'),
            'nombre' => $request->get('nombre'),
            'estado' => 1,
            'logo' => 'images/compania/'. $imageName
        ));
    
        $compania->save();
    
        $request->file('logo')->move(
            public_path('') . '/images/compania/', $imageName
        );
    
        Session::flash('flash_message', 'Compañia agregada satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $compania = Compania::findOrFail($id);
        $compania['logo'] = str_replace('images/compania/', '', $compania['logo']);
        return view('compania.show', ['compania' => $compania]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, CompaniaRequest $request)
    {
        $data = $request->all();

        if($request->hasFile('logo')) 
        {
            $extension = $request->file('logo')->getClientOriginalExtension(); 
            $imageName = $data['abreviatura'].'.'.$extension;
            $data['logo'] = 'images/compania/'. $imageName;

            $compania = Compania::findOrFail($id);
            $compania->fill($data)->save();

            $request->file('logo')->move(
                public_path('') . '/images/compania/', $imageName
            );
            
        } else {
            $compania = Compania::findOrFail($id);
            $data['logo'] = $compania['logo'];
            $compania->fill($data)->save();
        }

        Session::flash('flash_message', 'Compañia modificada satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $productos = Producto::where('compania_id', '=', $id)->first();
        if($productos == null)
        {
            $compania = Compania::findOrFail($id);
    
            $compania->delete();
            Session::flash('flash_message', 'Aseguradora eliminada satisfactoriamente!');
        } else
        {
            Session::flash('error_message', 'La aseguradora, tiene productos asociados, no puede eliminarse!');
        }
        return 'Ok';
    }
}
