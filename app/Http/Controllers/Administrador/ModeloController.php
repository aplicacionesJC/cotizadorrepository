<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Marca;
use App\Modelo;
use App\Anio;
use App\AnioModelo;
use App\ModeloProducto;
use App\Http\Requests\ModeloRequest;

class ModeloController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        $modelos = Marca::where(function($query) use($request){
            if(!empty($request['marca_id']))
                $query->where('marcas.id', '=', $request['marca_id']);
            
            if(!empty($request['eschino']))
                $query->where('es_chino', '=', $request['eschino']);
            
        })
        ->join('modelos', function($join) use($request){
            $join->on('modelos.marca_id', '=', 'marcas.id');

            if(!empty($request['modelo']))
                $join->where('modelos.nombre', 'like', '%'.$request['modelo'].'%');
            
            $join->whereNull('modelos.deleted_at');
        })
        ->select(DB::raw(
            "
            marcas.id as codigo_marca,
            marcas.nombre as nombre_marca,
            marcas.es_chino as es_chino, 
            marcas.estado as estado_marca, 
            modelos.nombre as nombre_modelo, 
            modelos.estado as estado_modelo, 
            modelos.valor_referencial as valor_referencial,
            modelos.id as codigo_modelo
            "
        ))
        ->paginate(10);

        $marcaArray = ['' => 'SELECCIONAR'];
        $marcas = Marca::all();
        foreach ($marcas as $key => $value) {
            $marcaArray[$value->id] = strtoupper($value->nombre);
        }
        
        return view('modelo.list', ['modelos' => $modelos, 'marcas' => $marcaArray]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $anios = Anio::all();
        foreach ($anios as $key => $value) {
            $aniosArray[$value->id] = strtoupper($value->nombre);
        }

        $marcaArray = ['' => 'SELECCIONAR'];
        $marcas = Marca::all();
        foreach ($marcas as $key => $value) {
            $marcaArray[$value->id] = strtoupper($value->nombre);
        }
        
        return view('modelo.create', ['marcas' => $marcaArray, 'anios' => $aniosArray]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $modelo = Modelo::findOrFail($id);

        $marcas = Marca::all();
        foreach ($marcas as $key => $value)
            $marcasArray[$value->id] = strtoupper($value->nombre);

        $anios = Anio::all();
        foreach ($anios as $key => $value) {
            $aniosArray[$value->id] = strtoupper($value->nombre);
        }

        $modelo['anios'] = AnioModelo::where(function($query) use($modelo){
            $query->where('anio_modelos.modelo_id', '=', $modelo->id);
        })
        ->join('anios', function($join) use($modelo){
            $join->on('anios.id', '=', 'anio_modelos.anio_id');
        })
        ->get();

        return view('modelo.show', ['modelo' => $modelo, 'anios' => $aniosArray, 'marcas' => $marcasArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ModeloRequest $request)
    {        
        $modelo = $request->all();
        $modelo = Modelo::create($modelo);

        foreach ($request->anio as $key => $anio) {
            AnioModelo::create(['anio_id' => $anio, 'modelo_id' => $modelo->id, 'estado' => 1]);
        }

        Session::flash('flash_message', 'Modelo agregado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, ModeloRequest $request)
    {
        $data = $request->all();

        $modelo = Modelo::findOrFail($id);
        $modelo->fill($data)->save();

        $anios = AnioModelo::where(function($query) use($modelo){
            $query->where('anio_modelos.modelo_id', '=', $modelo->id);
        })
        ->join('anios', function($join) use($modelo){
            $join->on('anios.id', '=', 'anio_modelos.anio_id');
        })
        ->get();

        foreach ($request->anio as $key => $anio) {
            $existe = false;
            foreach ($anios as $key => $anioOld) {
                if($anio == $anioOld->id){
                    $existe = true;
                }
            }
            if(!$existe){
                AnioModelo::create(['anio_id' => $anio, 'modelo_id' => $modelo->id, 'estado' => 1]);
            }
        }

        foreach ($anios as $key => $anioOld) {
            $existe = false;
            foreach ($request->anio as $key => $anio) {
                if($anio == $anioOld->id){
                    $existe = true;
                }
            }
            if(!$existe){
                $anioDelete = AnioModelo::where(function($query) use($anioOld){
                    $query->where('anio_modelos.modelo_id', '=', $anioOld->modelo_id);
                    $query->where('anio_modelos.anio_id', '=', $anioOld->anio_id);
                });
                $anioDelete->delete();
            }
        }

        Session::flash('flash_message', 'Modelo modificado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $modeloProducto = ModeloProducto::where('modelo_id', '=', $id)->first();
        if($modeloProducto == null) {
            $modelo = Modelo::findOrFail($id);

            $anios = AnioModelo::where('modelo_id', '=', $id)->get();
            foreach ($anios as $anio) {
                $anio->delete();
            }

            $modelo->delete();
            Session::flash('flash_message', 'Modelo eliminado satisfactoriamente!');
        } else {
            Session::flash('info_message', 'El modelo, tiene productos asociados, no puede eliminarse!');
        }
        return 'Ok';
    }
}
