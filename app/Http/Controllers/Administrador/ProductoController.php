<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use DB;
use Session;
use App\Producto;
use App\Compania;
use App\Http\Requests\ProductoRequest;

class ProductoController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $productos = Producto::where(function($query) use($request){
            if(!empty($request['producto']))
                $query->where('productos.nombre', 'like', '%'.$request['producto'].'%');
            
            if(!empty($request['abreviatura']))
                $query->where('productos.abreviatura', 'like', '%'.$request['abreviatura'].'%');
        })
        ->join('companias', function($join) use($request){
            $join->on('companias.id', '=', 'productos.compania_id');

            if(!empty($request['compania']))
                $join->where('companias.id', '=', $request['compania']);
            
            $join->whereNull('companias.deleted_at');
        })
        ->select(DB::raw(
            "
            productos.id as codigo_producto,
            productos.compania_id as codigo_compania,
            productos.nombre as nombre_producto, 
            productos.abreviatura as abreviatura_producto, 
            productos.prima_minima as prima_minima, 
            productos.tope_gps as tope_gps, 
            productos.comision as comision,
            productos.descuento as descuento,
            productos.estado as estado,
            companias.nombre as nombre_compania,
            companias.abreviatura as abreviatura_compania
            "
        ))
        ->paginate(10);

        $companiasArray = ['' => 'SELECCIONAR'];
        $companias = Compania::all();
        foreach ($companias as $key => $value) {
            $companiasArray[$value->id] = strtoupper($value->nombre);
        }
        return view('producto.list', ['productos' => $productos, 'companias' => $companiasArray]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $producto = Producto::findOrFail($id);

        $companias = Compania::all();
        foreach ($companias as $key => $value)
            $companiasArray[$value->id] = strtoupper($value->nombre);

        return view('producto.show', ['producto' => $producto, 'companias' => $companiasArray]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $companiaArray = ['' => 'SELECCIONAR'];
        $companias = Compania::all();
        foreach ($companias as $key => $value) {
            $companiaArray[$value->id] = strtoupper($value->nombre);
        }
        return view('producto.create', ['companias' => $companiaArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProductoRequest $request)
    {        
        $producto = $request->all();
        $producto = Producto::create($producto);

        Session::flash('flash_message', 'Producto agregado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, ProductoRequest $request)
    {
        $data = $request->all();

        $producto = Producto::findOrFail($id);
        $producto->fill($data)->save();

        Session::flash('flash_message', 'Producto modificado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $producto = Producto::findOrFail($id);

        // $nuevoEstado = $producto->estado == 1 ? 0 : 1;
        // $producto->update(['estado' => $nuevoEstado]);
        // Session::flash('flash_message', 'Se cambio de estado satisfactoriamente!');

        $producto->delete();
        Session::flash('flash_message', 'Producto eliminado satisfactoriamente!');

        return 'Ok';
    }
}
