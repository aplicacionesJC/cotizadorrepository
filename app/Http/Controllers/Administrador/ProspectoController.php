<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Session;
use App\Prospecto;
use App\Persona;
use App\Cotizado;
use App\Marca;
use App\Modelo;
use App\Anio;
use App\Producto;
use App\Compania;
use App\TipoAdicional;
use App\Adicional;
use App\Deducible;
use App\ModeloProducto;
use App\Tasa;
use App\Cuota;
use App\Parametro;
use Mail;
use PDF;
use Storage;

class ProspectoController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $prospectos = Prospecto::where(function($query) use($request){
            if(!empty($request['cotizacion']))
                $query->where('prospectos.id', '=', (int)$request['cotizacion']);
                
            if(!empty($request['estado']))
                $query->where('prospectos.estado', '=', $request['estado']); 
        })
        ->join('personas', function($join) use($request){
            $join->on('personas.id', '=', 'prospectos.persona_id');

            if(!empty($request['documento']))
                $join->where('documento', '=', $request['documento']);
        })
        ->join('marcas', function($join) use($request){
            $join->on('marcas.id', '=', 'prospectos.marca_id');
        })
        ->join('modelos', function($join) use($request){
            $join->on('modelos.id', '=', 'prospectos.modelo_id');
        })
        ->join('anios', function($join) use($request){
            $join->on('anios.id', '=', 'prospectos.anio_id');
        })
        ->select(DB::raw(
            "
            prospectos.id as codigo_cotizacion,
            prospectos.valor_aproximado as valor_aproximado,
            prospectos.fecha_cotizacion as fecha_cotizacion, 
            prospectos.correo as correo, 
            prospectos.celular as celular, 
            prospectos.estado as estado_cotizacion, 
            personas.documento as documento,
            personas.fecha_nacimiento as fecha_nacimiento,
            personas.nombres as nombres,
            personas.ape_paterno as ape_paterno,
            personas.ape_materno as ape_materno,
            marcas.nombre as nombre_marca,
            marcas.es_chino as procedencia,
            modelos.nombre as nombre_modelo,
            modelos.valor_referencial as valor_referencial,
            anios.nombre as anio
            "
        ))
        ->paginate(10);
        
        return view('prospecto.list', ['prospectos' => $prospectos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $prospecto = Prospecto::where(function($query) use($id){
            $query->where('prospectos.id', '=', $id);
        })
        ->join('personas', function($join){
            $join->on('personas.id', '=', 'prospectos.persona_id');
        })
        ->join('marcas', function($join){
            $join->on('marcas.id', '=', 'prospectos.marca_id');
        })
        ->join('modelos', function($join){
            $join->on('modelos.id', '=', 'prospectos.modelo_id');
        })
        ->join('anios', function($join){
            $join->on('anios.id', '=', 'prospectos.anio_id');
        })
        ->select(DB::raw(
            "
            prospectos.id as codigo_prospecto,
            prospectos.valor_aproximado as valor_aproximado_prospecto,
            prospectos.fecha_cotizacion as fecha_cotizacion_prospecto, 
            prospectos.estado as estado_prospecto, 
            prospectos.correo as correo_persona, 
            prospectos.celular as celular_persona, 
            personas.documento as documento_persona,
            personas.fecha_nacimiento as fecha_nacimiento_persona,
            personas.nombres as nombre_persona,
            personas.ape_paterno as ape_paterno_persona,
            personas.ape_materno as ape_materno_persona,
            marcas.nombre as nombre_marca,
            marcas.es_chino as es_chino_marca,
            modelos.nombre as nombre_modelo,
            modelos.valor_referencial as valor_referencial_modelo,
            anios.nombre as anio_fabricacion,
            prospectos.cotizaciones_id as codigo_cotizado
            "
        ))
        ->first()
        ->toArray();

        $cotizaciones = Cotizado::where(function($query) use($id){
            $query->where('prospecto_id', '=', $id);
        })
        ->join('tasas', function($join){
            $join->on('tasas.id', '=', 'cotizados.tasa_id');
        })
        ->join('productos', function($join){
            $join->on('productos.id', '=', 'cotizados.producto_id');
        })
        ->join('companias', function($join){
            $join->on('companias.id', '=', 'productos.compania_id');
        })
        ->select(DB::raw(
            "
            productos.id as codigo_producto,
            productos.nombre as nombre_producto,
            productos.abreviatura as abreviatura_producto,
            productos.tope_gps as tope_gps,
            productos.prima_minima as prima_minima,
            productos.comision as valor_comision,
            productos.descuento as valor_descuento,

            companias.nombre as nombre_compania,
            companias.abreviatura as abreviatura_compania,

            cotizados.valor_tasa as valor_tasa, 
            cotizados.cuota as meses,
            cotizados.valor_prima as valor_prima,
            cotizados.id as codigo_cotizado,
            cotizados.estado as estado_cotizado
            "
        ))
        ->get()
        ->toArray();

        $cotizadosMem = [];
        $iterador = 0;
        $existeProducto = false;
        foreach ($cotizaciones as $cotizacionDb) 
        {
            foreach ($cotizadosMem as $cotizadoMem) 
            {
                if ($cotizadoMem['codigo_producto'] == $cotizacionDb['codigo_producto']) 
                {
                    $existeProducto = true;
                    // agarro y genero un nuevo elemento en el campo cuotas
                    $iteradorCuota = count($cotizadoMem['cuotas']);
                    

                    $cotizadosMem[$iterador - 1]['cuotas'][$iteradorCuota] = [
                                                                'meses' => $cotizacionDb['meses'],
                                                                'prima' => $cotizacionDb['valor_prima'],
                                                                'estado' => $cotizacionDb['estado_cotizado'],
                                                                'codigo_cotizado' => $cotizacionDb['codigo_cotizado'],
                                                            ];
                } else {
                    $existeProducto = false;
                }
            }

            if (!$existeProducto) {
                // genero un nuevo elemento en el campo cotizados con la info repetida
                $cotizadosMem[$iterador] = [
                                                'codigo_producto' => $cotizacionDb['codigo_producto'],
                                                'nombre_producto' => $cotizacionDb['nombre_producto'],
                                                'nombre_compania' => $cotizacionDb['nombre_compania'],
                                                'abreviatura_producto' => $cotizacionDb['abreviatura_producto'],
                                                'abreviatura_compania' => $cotizacionDb['abreviatura_compania'],
                                                'tope_gps' => $cotizacionDb['tope_gps'],
                                                'valor_tasa' => $cotizacionDb['valor_tasa'],
                                                'prima_minima' => $cotizacionDb['prima_minima'],
                                                'valor_comision' => $cotizacionDb['valor_comision'],
                                                'valor_descuento' => $cotizacionDb['valor_descuento'],
                                                'cuotas' => []
                                            ];

                // genero el primer elemento del campo cuotas
                $cotizadosMem[$iterador]['cuotas'][0] = [
                                                            'meses' => $cotizacionDb['meses'],
                                                            'prima' => $cotizacionDb['valor_prima'],
                                                            'estado' => $cotizacionDb['estado_cotizado'],
                                                            'codigo_cotizado' => $cotizacionDb['codigo_cotizado']
                                                        ];
                $iterador++;
            }
        }

        $prospecto['cotizados'] = $cotizadosMem;

        return view('prospecto.show', ['prospecto' => $prospecto ]);
    }

    public function getFormatoCorreo($prospectoId)
    {
        $prospecto = Prospecto::findOrFail($prospectoId);
        $dataPDF = $this->getDataPdf($prospectoId);

        return view('correo.solicitudCotizacion', ['info' => $dataPDF]);
    }

    public function postEnviarCorreo($prospectoId)
    {
        $prospecto = Prospecto::findOrFail($prospectoId);
        $dataPDF = $this->getDataPdf($prospectoId);
        $pdf = file_get_contents(public_path('') .'/pdf/cotizacion_'.$prospectoId.'.pdf');
        //$pdf = PDF::loadView('pdf.cotizacionPDF', array('cotizacion' => $dataPDF));

        $mailInfo = [
            'pdf' => $pdf,
            'correo' => $prospecto->correo,
            'dataCorreo' => $dataPDF 
        ];

        Mail::send('correo.solicitudCotizacion',['info' => $dataPDF], function($email) use($mailInfo){
            $email->subject('Solicitud de Cotizacion Autos - Arias & Asociados');
            $email->to($mailInfo['correo']);
            //$email->attachData($mailInfo['pdf']->output(), 'cotizacion.pdf');
            $email->attachData($mailInfo['pdf'], 'cotizacion_'.$prospectoId.'.pdf');
        });

        return response()->json(['success'=>'Se enviaron los datos de forma correcta']);
    }
    
    public function getDownloadPdf($prospectoId)
    {
        return redirect('/pdf/cotizacion_'.$prospectoId.'.pdf');
        // $dataPDF = $this->getDataPdf($prospectoId);
        // $pdf = PDF::loadView('pdf.cotizacionPDF', array('cotizacion' => $dataPDF));
        // return $pdf->download();
    }

    public function getDataPdf($prospectoId)
    {
        $now = Carbon::now();       
        $prospecto = Prospecto::findOrFail($prospectoId);
        $persona = Persona::findOrFail($prospecto->persona_id);
        $marca = Marca::findOrFail($prospecto->marca_id);
        $modelo = Modelo::findOrFail($prospecto->modelo_id);
        $anioAutoMovil = Anio::findOrFail($prospecto->anio_id);
        $cotizaciones = Cotizado::where('prospecto_id', '=', $prospectoId)->get();

        /* TRAER LOS PRODUCTOS QUE COTIZAN EL PROSPECTO */
        $productoModelo = 
        ModeloProducto::where('modelo_id', $modelo->id)
                        ->join('productos', 'modelo_productos.producto_id', '=', 'productos.id')
                        ->get();

        
        $companiaPDF = [];
        $productos = [];
        $productosCount = 0;
        if(!empty($productoModelo))
        {
            foreach($productoModelo as $producto)
            {

                /* OBJETO AUXILIAR DE CONSULTA */
                $anioProducto = [
                    'antiguedad' => ($now->year - (int)$anioAutoMovil['nombre']),
                    'producto_id' => $producto['producto_id'],
                    'prima_minima' => $producto['prima_minima']
                ];

                /* TRAER LA TASA PARA EL MODELO POR AÑO Y ANTIGUEDAD */
                $tasa = Tasa::where(function($query) use($anioProducto){
                    if(!empty($anioProducto['antiguedad']))  $query->where('antiguedad', '=', $anioProducto['antiguedad']);
                    if(!empty($anioProducto['producto_id']))  $query->where('producto_id', '=', $anioProducto['producto_id']);
                })
                ->join('productos', 'productos.id', '=', 'tasas.producto_id')
                ->join('companias', 'companias.id', '=', 'productos.compania_id')
                ->get();

                $tasa = $tasa->first();
                
                if(!empty($tasa))
                {
                    $productos[$productosCount] = $producto['id'];
                    $productosCount++;

                    /* TRAER LOS FACTORES POR COMPAÑIA */
                    $cuotas = Cuota::where('compania_id', '=', $producto['compania_id'])->orderBy('meses','DESC')->get();
                    $factor = Parametro::where(function($query){ $query->where('codigo_grupo', 'FACTOR'); $query->where('codigo', 'PORCENTAJE');})->get();
                    $factor = $factor->first();

                    /* VALOR DE LA TASA */
                    $valor_tasa = ($prospecto->valor_aproximado * $tasa['porcentaje']) / 100;
                    $valor_prima = $valor_tasa * $factor['valor_numerico'];
                    $valor_prima = (float)($valor_prima < $producto['prima_minima'] ? $producto['prima_minima'] : $valor_prima);

                    $prima = [];
                    foreach ($cuotas as $value) 
                    {
                        $valor_prima_calculado = (float)($valor_prima / $value['meses']);
                        $prima[$value['meses']] = ['mes' => $value['meses'], 'prima' => round($valor_prima_calculado, 2) ];   
                    }

                    $companiaPDF[$tasa['abreviatura']] =   [
                                                                'abreviatura' => $tasa['abreviatura'],
                                                                'ruta' => $tasa['logo'],
                                                                'nombre' => $tasa['nombre'],
                                                                'prima' => $prima
                                                            ];
                }
            }
        }

        $response = [];

        $tiposCount = 0;
        $tiposCobertura = TipoAdicional::all();

        foreach ($tiposCobertura as $tipo) 
        {
            $response[$tiposCount] = [
                'tipoCobertura' => $tipo['descripcion'],
                'codigoTipo' => 'TIP_COB_' . $tiposCount,
                'coberturas' => []
            ];

            $coberturasCount = 0;
            $coberturas = Adicional::where('tipo_adicional_id', $tipo->id)->get();

            foreach ($coberturas as $cobertura) 
            {
                $response[$tiposCount]['coberturas'][$coberturasCount] = [
                    'descripcion' => $cobertura['descripcion'],
                    'productos' => []  
                ];

                $productosCount = 0;
                foreach ($productos as $productoID) 
                {
                    $useQuery = ['coberturaID' => $cobertura->id, 'productoID' => $productoID];

                    $deducible = Deducible::where(function($query) use($useQuery){
                        $query->where('adicional_id', '=', $useQuery['coberturaID']);
                        $query->where('producto_id', '=', $useQuery['productoID']);                            
                    })->first();
                    
                    $response[$tiposCount]['coberturas'][$coberturasCount]['productos'][$productosCount] = [
                        'productoID' => $productoID,
                        'descripcion' => $deducible['descripcion']
                    ];

                    $productosCount++;
                }

                $coberturasCount++;
            }

            $tiposCount++;
        }

        $ancho = (540/$productosCount) > 270 ? 270 : (540/$productosCount);
        $fijo = (540/$productosCount) > 270 ? 460 : 180;

        $dataPDF = [
            'info_auto' => $marca->nombre." ".$modelo->nombre." ".$anioAutoMovil->nombre,
            'valor_aproximado' => $prospecto->valor_aproximado,
            'numero_prospecto' => $prospectoId,
            'documento' => $persona->documento,
            'fecha' => Carbon::now()->toTimeString(),
            'ancho_columna' => ($ancho)."px",
            'ancho_base' => ($fijo)."px",
            'numero_productos' => $productosCount + 1,
            'companias' => $companiaPDF,
            'tiposCoberturas' => $response
        ];

        return $dataPDF;
    }

    public function postSolicitarCompra(Request $request){
        $data = $request->json()->all();

        $prospecto = Prospecto::findOrFail((int)$data['prospectoId']);
        $cotizado = Cotizado::findOrFail((int)$data['cotizacionId']);

        $prospecto->update(['cotizaciones_id' => (int)$data['cotizacionId'], 'estado' => 2]);
        //$cotizado->update(['estado' => 2]);

        Session::flash('flash_message', 'Solicitud de compra realizada satisfactoriamente!');

        return response(['success' => 'ok', 'prospecto' => $prospecto])->header('Content-Type', 'json');
    }
}
