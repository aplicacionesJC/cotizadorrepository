<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use DB;
use Session;
use App\Producto;
use App\Modelo;
use App\ModeloProducto;
use App\Http\Requests\ProductoRequest;
use Illuminate\Routing\Route;

class ProductoModeloController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $modeloId = (int)$request->route()->parameters['modeloId'];

        $productos = Producto::where(function($query) use($modeloId){})
        ->join('modelo_productos', function($join) use($modeloId){
            $join->on('modelo_productos.producto_id', '=', 'productos.id');

            if(!empty($modeloId))
                $join->where('modelo_productos.modelo_id', '=', $modeloId);

            $join->whereNull('modelo_productos.deleted_at');
        })
        ->select(DB::raw(
            "
            productos.id as id,
            productos.compania_id as codigo_compania,
            productos.nombre as nombre_producto, 
            productos.abreviatura as abreviatura_producto, 
            productos.prima_minima as prima_minima, 
            productos.tope_gps as tope_gps, 
            productos.comision as comision,
            productos.descuento as descuento,
            productos.estado as estado,
            modelo_productos.id as codigo,
            modelo_productos.modelo_id as modelo_id
            "
        ))
        ->paginate(10);

        $modelo = Modelo::findOrFail($modeloId);

        return view('productomodelo.list', ['productos' => $productos, 'modelo' => $modelo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $modeloId = (int)$request->route()->parameters['modeloId'];
        $modelo = Modelo::findOrFail($modeloId);

        $modeloProductos = ModeloProducto::where('modelo_id', '=', $modeloId)->get();
        $productos = Producto::all();
        foreach ($productos as $key => $producto) {
            $existe = false;
            foreach ($modeloProductos as $key => $value) {
                if($value->producto_id == $producto->id){
                    $existe = true;
                }
            }
            if(!$existe){
                $productosArray[$producto->id] = strtoupper($producto->nombre);
            }
        }

        return view('productomodelo.create', ['modelo' => $modelo, 'productos' => $productosArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {        
        $modeloId = (int)$request->route()->parameters['modeloId'];

        $productoModelo = $request->all();
        $productoModelo = ModeloProducto::create(['modelo_id' => $modeloId, 'producto_id' => $request['producto_id'], 'estado' => 1]);

        Session::flash('flash_message', 'Producto asociado al modelo satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $productoId = (int)$request->route()->parameters['producto'];
        $modeloProducto = ModeloProducto::findOrFail($productoId);

        // $nuevoEstado = $compania->estado == 1 ? 0 : 1;
        // $compania->update(['estado' => $nuevoEstado]);
        // Session::flash('flash_message', 'Se cambio de estado satisfactoriamente!');

        $modeloProducto->delete();
        Session::flash('flash_message', 'Producto asociado al modelo eliminado satisfactoriamente!');

        return 'Ok';
    }

}
