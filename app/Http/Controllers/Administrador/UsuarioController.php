<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use App\Rol;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ContraseniaRequest;
use Illuminate\Support\Facades\Auth;

class UsuarioController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        
        $usuarios = User::where(function($query) use($request){
            if(!empty($request['nombre']))
                $query->where('users.nombre', 'like', '%'.$request['nombre'].'%');
            
            if(!empty($request['correo']))
                $query->where('users.email', 'like', '%'.$request['correo'].'%');
        });

        if(!empty($request['rol'])){
            $usuarios = $usuarios->join('rol_user', function($join) use($request){
                $join->on('rol_user.user_id', '=', 'users.id');

                $join->where('rol_user.rol_id', '=', $request['rol']);
            });
        }
            
        $usuarios = $usuarios->paginate(10);

        $rolesArray = ['' => 'SELECCIONAR'];
        $roles = Rol::all();
        foreach ($roles as $key => $value) {
            $rolesArray[$value->id] = strtoupper($value->nombre);
        }
        return view('usuario.list', ['usuarios' => $usuarios, 'roles' => $rolesArray]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $usuario = User::findOrFail($id);

        $roles = Rol::all();
        foreach ($roles as $key => $value)
            $rolesArray[$value->id] = strtoupper($value->nombre);

        return view('usuario.show', ['usuario' => $usuario, 'roles' => $rolesArray]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Rol::all();
        foreach ($roles as $key => $value)
            $rolesArray[$value->id] = strtoupper($value->nombre);

        return view('usuario.create', ['roles' => $rolesArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(UserRequest $request)
    {        
        
        if($request->offsetExists('password'))
            $request->offsetSet('password', bcrypt($request->offsetGet('password')));

        $usuario = $request->all();
        $usuario = User::create($usuario);

        if($request->offsetExists('rol'))
            foreach ($request->rol as $value) {
                $usuario->roles()->attach(Rol::findOrFail($value));
            }

        Session::flash('flash_message', 'Usuario agregado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, UserRequest $request)
    {
        if($request->offsetExists('password'))
            $request->offsetSet('password', bcrypt($request->offsetGet('password')));
        
        $data = $request->all();

        $user = User::findOrFail($id);
        $user->fill($data)->save();

        Session::flash('flash_message', 'Usuario modificado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $usuario = User::findOrFail($id);

        // $nuevoEstado = $usuario->estado == 1 ?  0 : 1;
        // $usuario->update(['estado' => $nuevoEstado]);
        // Session::flash('flash_message', 'Se cambio de estado satisfactoriamente!');

        $usuario->delete();
        Session::flash('flash_message', 'Usuario eliminado satisfactoriamente!');

        return 'Ok';
    }

    public function changePassword(ContraseniaRequest $request){
        if($request->offsetExists('password'))
            $request->offsetSet('password', bcrypt($request->offsetGet('password')));
        
        $data = $request->all();
        $id = Auth::id();
        $user = User::findOrFail($id);
        $user->fill($data)->save();

        Session::flash('flash_message', 'Contraseña modificada satisfactoriamente!');

        return redirect()->back();
    }
}
