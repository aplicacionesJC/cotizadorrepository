<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Administrador\GenericController;
use DB;
use Session;
use App\Adicional;
use App\TipoAdicional;
use App\Deducible;
use App\Http\Requests\ConceptoRequest;

class ConceptoController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        
        $conceptos = Adicional::where(function($query) use($request){
            if(!empty($request['descripcion']))
                $query->where('adicionales.descripcion', 'like', '%'.$request['descripcion'].'%');
        })
        ->join('tipo_adicionales', function($join) use($request){
            $join->on('tipo_adicionales.id', '=', 'adicionales.tipo_adicional_id');
            
            if(!empty($request['tipo_concepto_id']))
                $join->where('tipo_adicionales.id', '=', $request['tipo_concepto_id']);

            $join->whereNull('tipo_adicionales.deleted_at');
        })
        ->select(DB::raw(
            "
            adicionales.id as id,
            adicionales.descripcion as descripcion,
            adicionales.orden as orden, 
            adicionales.estado as estado,
            adicionales.tipo_adicional_id as tipo_concepto_id,
            tipo_adicionales.descripcion as tipo_concepto
            "
        ))
        ->paginate(10);

        $tipoConceptoArray = ['' => 'SELECCIONAR'];
        $tiposConcepto = TipoAdicional::all();
        foreach ($tiposConcepto as $key => $value) {
            $tipoConceptoArray[$value->id] = strtoupper($value->descripcion);
        }
        return view('concepto.list', ['conceptos' => $conceptos, 'tiposConcepto' => $tipoConceptoArray]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $tipoConceptoArray = ['' => 'SELECCIONAR'];
        $tiposConcepto = TipoAdicional::all();
        foreach ($tiposConcepto as $key => $value) {
            $tipoConceptoArray[$value->id] = strtoupper($value->descripcion);
        }

        return view('concepto.create', ['tiposConcepto' => $tipoConceptoArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ConceptoRequest $request)
    {        
        $concepto = $request->all();
        $concepto = Adicional::create($concepto);

        Session::flash('flash_message', 'Concepto agregado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        
        $tipoConceptoArray = ['' => 'SELECCIONAR'];
        $tiposConcepto = TipoAdicional::all();
        foreach ($tiposConcepto as $key => $value) {
            $tipoConceptoArray[$value->id] = strtoupper($value->descripcion);
        }

        $concepto = Adicional::findOrFail($id);
        return view('concepto.show', ['concepto' => $concepto, 'tiposConcepto' => $tipoConceptoArray]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, ConceptoRequest $request)
    {
        $data = $request->all();

        $concepto = Adicional::findOrFail($id);
        $concepto->fill($data)->save();

        Session::flash('flash_message', 'Concepto modificado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $deducible = Deducible::where('adicional_id', '=', $id)->first();
        if($deducible == null){
            $concepto = Adicional::findOrFail($id);
    
            $concepto->delete();
            Session::flash('flash_message', 'Concepto eliminado satisfactoriamente!');
        }else{
            Session::flash('info_message', 'El concepto, tiene detalles asociados, no puede eliminarse!');
        }
        return 'Ok';
    }

    /**
     * Get the concepts by type.
     *
     * @param  int  $id
     * @return Response
     */
    public function getConceptosByTipo(Request $request){
        $tipo = $request['tipo'];
        $conceptos = Adicional::where('tipo_adicional_id', '=', $tipo)->get(['id', 'descripcion']);

        return response($conceptos->toJson())->header('Content-Type', 'json');
    }
}