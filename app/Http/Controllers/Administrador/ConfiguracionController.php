<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Parametro;
use DB;
use Session;
use App\Http\Requests\ParametroRequest;

class ConfiguracionController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        
        $parametros = Parametro::where(function($uqery) use($request){})->paginate(10);

        $agrupadores = Parametro::distinct()->select(DB::raw("codigo_grupo as nombre"))->get();
        return view('configuracion.list', ['parametros' => $parametros, 'agrupadores' => $agrupadores]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $parametro = Parametro::findOrFail($id);
        $agrupadores = Parametro::distinct()->select(DB::raw("codigo_grupo as nombre"))->get();
        return view('configuracion.show', ['parametro' => $parametro, 'agrupadores' => $agrupadores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $agrupadores = Parametro::distinct()->select(DB::raw("codigo_grupo as nombre"))->get();
        return view('configuracion.create', ['agrupadores' => $agrupadores]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ParametroRequest $request)
    {        
        $parametro = $request->all();
        $parametro = Parametro::create($parametro);

        Session::flash('flash_message', 'Parametro agregado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, ParametroRequest $request)
    {
        $data = $request->all();

        $parametro = Parametro::findOrFail($id);
        $parametro->fill($data)->save();

        Session::flash('flash_message', 'parametro modificado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $parametro = Parametro::findOrFail($id);

        // $nuevoEstado = $parametro->estado == 1 ? 0 : 1;
        // $parametro->update(['estado' => $nuevoEstado]);
        // Session::flash('flash_message', 'Se cambio de estado satisfactoriamente!');

        $parametro->delete();
        Session::flash('flash_message', 'Parámetro eliminado satisfactoriamente!');

        return 'Ok';
    }
}
