<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\Tasa;
use App\Producto;
use App\Http\Requests\TasaRequest;

class TasaController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $anioActual = date('Y');
        
        if(!empty($request['anio']))
            $request['antiguedad'] = $anioActual - $request['anio'];

        $tasas = Producto::where(function($query) use($request){
            if(!empty($request['producto_id']))
                $query->where('productos.id', '=', $request['producto_id']);
        })
        ->join('tasas', function($join) use($request){
            $join->on('tasas.producto_id', '=', 'productos.id');

            if(!empty($request['anio']))
                $join->where('tasas.antiguedad', '=', $request['antiguedad']);
            
            $join->whereNull('tasas.deleted_at');
        })
        ->select(DB::raw(
            "
            productos.id as codigo_producto,
            productos.compania_id as codigo_compania,
            productos.nombre as nombre_producto, 
            productos.abreviatura as abreviatura_producto, 
            tasas.antiguedad as antiguedad,
            tasas.id as codigo_tasa,
            tasas.porcentaje as porcentaje,
            tasas.estado as estado
            "
        ))
        ->orderBy('codigo_producto', 'ASC')
        ->orderBy('antiguedad', 'ASC')
        ->paginate(10);

        $productosArray = ['' => 'SELECCIONAR'];
        $productos = Producto::all();
        foreach ($productos as $key => $value) {
            $productosArray[$value->id] = strtoupper($value->nombre);
        }

        $anios = ['' => 'SELECCIONAR'];
        for ($i = 1990 ; $i <= $anioActual ; $i++) { 
            $anios[$i] = $i;
        }

        return view('tasa.list', ['productos' => $productosArray, 'tasas' => $tasas, 'anios' => $anios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $productosArray = ['' => 'SELECCIONAR'];
        $productos = Producto::all();
        foreach ($productos as $key => $value) {
            $productosArray[$value->id] = strtoupper($value->nombre);
        }

        return view('tasa.create', ['productos' => $productosArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(TasaRequest $request)
    {        
        $tasa = $request->all();
        $tasa['antiguedad'] = date('Y') - $tasa['antiguedad'];
        $tasa = Tasa::create($tasa);

        Session::flash('flash_message', 'Tasa agregada satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $productosArray = ['' => 'SELECCIONAR'];
        $productos = Producto::all();
        foreach ($productos as $key => $value) {
            $productosArray[$value->id] = strtoupper($value->nombre);
        }

        $tasa = Tasa::findOrFail($id);
        $tasa['antiguedad'] = date('Y') - $tasa['antiguedad'];
        return view('tasa.show', ['productos' => $productosArray, 'tasa' => $tasa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, TasaRequest $request)
    {
        $data = $request->all();
        $data['antiguedad'] = date('Y') - $data['antiguedad'];
        $tasa = Tasa::findOrFail($id);
        $tasa->fill($data)->save();

        Session::flash('flash_message', 'Tasa modificada satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $tasa = Tasa::findOrFail($id);

        $tasa->delete();
        Session::flash('flash_message', 'Tasa eliminada satisfactoriamente!');

        return 'Ok';
    }

    public function getTasasByProducto($productoId)
    {
        $tasas = Producto::where(function($query) use($productoId){
            if(!empty($productoId))
                $query->where('productos.id', '=', $productoId);
        })
        ->join('tasas', function($join) use($productoId){
            $join->on('tasas.producto_id', '=', 'productos.id');
            $join->whereNull('tasas.deleted_at');
        })
        ->select(DB::raw(
            "
            tasas.producto_id as codigo_producto,
            productos.nombre as nombre_producto,
            productos.abreviatura as abreviatura_producto, 
            tasas.antiguedad as antiguedad_tasa, 
            tasas.estado as estado_tasa, 
            tasas.porcentaje as porcentaje_tasa,
            tasas.id as codigo_tasa
            "
        ))
        ->orderBy('antiguedad_tasa', 'ASC')
        ->paginate(25);

        return view('tasa.modal', ['tasas' => $tasas, 'producto_id' => $productoId]);
    }
}
