<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Administrador\GenericController;
use DB;
use Session;
use App\Cuota;
use App\Compania;
use App\Http\Requests\CuotaRequest;

class CuotaController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        
        $cuotas = Cuota::where(function($query) use($request) {
            
            if(!empty($request['meses']))
                $query->where('meses', '=', $request['meses']);
        })
        ->join('companias', function($join) use($request){
            $join->on('companias.id', '=', 'cuotas.compania_id');

            if(!empty($request['compania_id']))
                $join->where('companias.id', '=', $request['compania_id']);

            $join->whereNull('companias.deleted_at');
        })
        ->select(DB::raw(
            "
            cuotas.id as id,
            cuotas.meses as meses,
            cuotas.factor as factor, 
            cuotas.compania_id as codigo_compania, 
            cuotas.estado as estado,
            companias.nombre as nombre_compania, 
            companias.abreviatura as abreviatura_compania
            "
        ))
        ->orderBy('nombre_compania', 'ASC')
        ->orderBy('meses', 'ASC')
        ->paginate(10);

        $companiasArray = ['' => 'SELECCIONAR'];
        $companias = Compania::all();
        foreach ($companias as $key => $value) {
            $companiasArray[$value->id] = strtoupper($value->nombre);
        }
        
        return view('cuota.list', ['cuotas' => $cuotas, 'companias' => $companiasArray]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $companiasArray = ['' => 'SELECCIONAR'];
        $companias = Compania::all();
        foreach ($companias as $key => $value) {
            $companiasArray[$value->id] = strtoupper($value->nombre);
        }

        return view('cuota.create', ['companias' => $companiasArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CuotaRequest $request)
    {   
        $cuota = $request->all();
        $cuota = Cuota::create($cuota);
      
        Session::flash('flash_message', 'Cuota agregada satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $cuota = Cuota::findOrFail($id);

        $companiasArray = ['' => 'SELECCIONAR'];
        $companias = Compania::all();
        foreach ($companias as $key => $value) {
            $companiasArray[$value->id] = strtoupper($value->nombre);
        }

        return view('cuota.show', ['cuota' => $cuota, 'companias' => $companiasArray]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, CuotaRequest $request)
    {
        $data = $request->all();

        $cuota = Cuota::findOrFail($id);
        $cuota->fill($data)->save();

        Session::flash('flash_message', 'Cuota modificada satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $cuota = Cuota::findOrFail($id);
    
        $cuota->delete();
        Session::flash('flash_message', 'Cuota eliminada satisfactoriamente!');

        return 'Ok';
    }
}