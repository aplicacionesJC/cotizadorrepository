<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GenericController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
}
