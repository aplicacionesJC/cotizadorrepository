<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Administrador\GenericController;
use DB;
use Session;
use App\TipoAdicional;
use App\Adicional;
use App\Http\Requests\TipoConceptoRequest;

class TipoConceptoController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        
        $companias = TipoAdicional::where(function($query) use($request){
            if(!empty($request['descripcion']))
                $query->where('descripcion', 'like', '%'.$request['descripcion'].'%');
        })->paginate(10);
        
        return view('tipoconcepto.list', ['tiposconcepto' => $companias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipoconcepto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(TipoConceptoRequest $request)
    {   
        $tipoConcepto = new TipoAdicional(array(
            'descripcion' => $request->get('descripcion'),
            'orden' => $request->get('orden'),
            'estado' => 1
        ));
      
        $tipoConcepto->save();
      
        Session::flash('flash_message', 'Tipo de concepto agregado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $tipoConcepto = TipoAdicional::findOrFail($id);
        return view('tipoconcepto.show', ['tipoConcepto' => $tipoConcepto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, TipoConceptoRequest $request)
    {
        $data = $request->all();

        $tipoConcepto = TipoAdicional::findOrFail($id);
        $tipoConcepto->fill($data)->save();

        Session::flash('flash_message', 'Tipo de concepto modificado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $concepto = Adicional::where('tipo_adicional_id', '=', $id)->first();
        if($concepto == null){
            $tipoConcepto = TipoAdicional::findOrFail($id);
    
            $tipoConcepto->delete();
            Session::flash('flash_message', 'Tipo de concepto eliminado satisfactoriamente!');
        }else{
            Session::flash('info_message', 'El tipo de concepto, tiene conceptos asociados, no puede eliminarse!');
        }
        return 'Ok';
    }
}