<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Marca;
use App\Modelo;
use App\Http\Requests\MarcaRequest;

class MarcaController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        $marcas = Marca::where(function($query) use($request){
            if(!empty($request['marca']))
                $query->where('marcas.nombre', 'like', '%'.$request['marca'].'%');
            
            if(!empty($request['eschino']))
                $query->where('es_chino', '=', $request['eschino']);
            
        })
        // ->join('modelos', function($join) use($request){
        //     $join->on('modelos.marca_id', '=', 'marcas.id');

        //     if(!empty($request['modelo']))
        //         $join->where('modelos.nombre', 'like', '%'.$request['modelo'].'%');
            
        //     $join->whereNull('modelos.deleted_at');
        // })
        // ->select(DB::raw(
        //     "
        //     marcas.id as codigo_marca,
        //     marcas.nombre as nombre_marca,
        //     marcas.es_chino as es_chino, 
        //     marcas.estado as estado_marca, 
        //     modelos.nombre as nombre_modelo, 
        //     modelos.estado as estado_modelo, 
        //     modelos.valor_referencial as valor_referencial,
        //     modelos.id as codigo_modelo
        //     "
        // ))
        ->paginate(10);
        
        return view('marca.list', ['marcas' => $marcas]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $marca = Marca::findOrFail($id);
        return view('marca.show', ['marca' => $marca]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $marcaArray = ['' => 'SELECCIONAR'];
        $marcas = Marca::all();
        foreach ($marcas as $key => $value) {
            $marcaArray[$value->id] = strtoupper($value->nombre);
        }
        return view('marca.create', ['marcas' => $marcaArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MarcaRequest $request)
    {        
        $marca = $request->all();
        $marca = Marca::create($marca);

        Session::flash('flash_message', 'Marca agregada satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, MarcaRequest $request)
    {
        $data = $request->all();

        $marca = Marca::findOrFail($id);
        $marca->fill($data)->save();

        Session::flash('flash_message', 'Marca modificada satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $modelos = Modelo::where('marca_id', '=', $id)->first();
        if($modelos == null){
            $marca = Marca::findOrFail($id);

            // $nuevoEstado = $compania->estado == 1 ? 0 : 1;
            // $compania->update(['estado' => $nuevoEstado]);
            // Session::flash('flash_message', 'Se cambio de estado satisfactoriamente!');
    
            $marca->delete();
            Session::flash('flash_message', 'Marca eliminada satisfactoriamente!');
        }else{
            Session::flash('info_message', 'La marca, tiene modelos asociados, no puede eliminarse!');
        }
        return 'Ok';
    }

    public function getMarcaModal(){
        $marcas = Marca::all();
        return view('marca.modal', ['marcas' => $marcas]);
    }
}
