<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\TipoAdicional;
use App\Adicional;
use App\Deducible;
use App\Producto;
use App\Http\Requests\DeducibleRequest;

class DeducibleController extends GenericController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){

        $deducibles = TipoAdicional::where(function($query) use($request){
            if(!empty($request['tipo_concepto_id']))
                $query->where('tipo_adicionales.id', '=', $request['tipo_concepto_id']);
        })
        ->join('adicionales', function($join) use($request){
            $join->on('adicionales.tipo_adicional_id', '=', 'tipo_adicionales.id');

            if(!empty($request['concepto_id']))
                $join->where('adicionales.id', '=', $request['concepto_id']);
            
            $join->whereNull('adicionales.deleted_at');
        })
        ->join('deducibles', function($join) use($request){
            $join->on('deducibles.adicional_id', '=', 'adicionales.id');

            if(!empty($request['descripcion']))
                $join->where('deducibles.descripcion', 'like', '%'.$request['descripcion'].'%');

            if(!empty($request['flag_resumen']))
                $join->where('deducibles.flag_resumen', 'like', '%'.$request['flag_resumen'].'%');

            $join->whereNull('deducibles.deleted_at');
        })
        ->join('productos', function($join) use($request){
            $join->on('productos.id', '=', 'deducibles.producto_id');

            if(!empty($request['producto_id']))
                $join->where('productos.id', '=', $request['producto_id']);

            $join->whereNull('productos.deleted_at');
        })
        ->select(DB::raw(
            "
            tipo_adicionales.id as codigo_tipo,
            tipo_adicionales.descripcion as descripcion_tipo,
            tipo_adicionales.orden as orden_tipo,
            adicionales.id as codigo_concepto,
            adicionales.descripcion as descripcion_concepto,
            adicionales.orden as orden_concepto,
            deducibles.producto_id as codigo_producto,
            deducibles.id as codigo_deducible,
            deducibles.descripcion as descripcion_deducible,
            deducibles.flag_resumen as flag_resumen,
            productos.nombre as nombre_producto,
            productos.abreviatura as abreviatura_producto
            "
        ))
        ->paginate(10);

        $tipoConceptoArray = ['' => 'SELECCIONAR'];
        $tiposConcepto = TipoAdicional::all();
        foreach ($tiposConcepto as $key => $value) {
            $tipoConceptoArray[$value->id] = strtoupper($value->descripcion);
        }

        $conceptosArray = ['' => 'SELECCIONAR'];
        $conceptos = Adicional::all();
        foreach ($conceptos as $key => $value) {
            $conceptosArray[$value->id] = strtoupper($value->descripcion);
        }

        $productosArray = ['' => 'SELECCIONAR'];
        $productos = Producto::all();
        foreach ($productos as $key => $value) {
            $productosArray[$value->id] = strtoupper($value->abreviatura);
        }

        $resumenArray = ['' => 'SELECCIONAR', 1 => 'SI', 0 => 'NO'];

        return view('deducible.list', 
                        ['deducibles' => $deducibles, 'tiposConcepto' => $tipoConceptoArray, 
                        'conceptos' => $conceptosArray, 'productos' => $productosArray,
                        'resumen' => $resumenArray]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        // $productoId = (int)$request->route()->parameters['productoId'];
        // $producto = Producto::findOrFail($productoId);

        $tiposArray = ['' => 'SELECCIONAR'];
        $tipos = TipoAdicional::all();
        foreach ($tipos as $key => $value) {
            $tiposArray[$value->id] = strtoupper($value->descripcion);
        }

        $productosArray = ['' => 'SELECCIONAR'];
        $productos = Producto::all();
        foreach ($productos as $key => $value) {
            $productosArray[$value->id] = strtoupper($value->abreviatura);
        }

        $conceptosArray = ['' => 'SELECCIONAR'];
        $resumenArray = ['' => 'SELECCIONAR', 1 => 'SI', 0 => 'NO'];

        return view('deducible.create', 
                    ['productos' => $productosArray, 'tipoConceptos' => $tiposArray, 
                     'conceptos' => $conceptosArray, 'resumen' => $resumenArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(DeducibleRequest $request)
    {        
        $deducible = $request->all();
        $deducible = Deducible::create($deducible);

        Session::flash('flash_message', 'Deducible agregado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $deducible = Deducible::findOrFail($id);
        $conceptoDeducible = Adicional::findOrFail($deducible->adicional_id);

        $productosArray = ['' => 'SELECCIONAR'];
        $productos = Producto::all();
        foreach ($productos as $key => $value) {
            $productosArray[$value->id] = strtoupper($value->abreviatura);
        }

        $tipoConceptoArray = ['' => 'SELECCIONAR'];
        $tiposConcepto = TipoAdicional::all();
        foreach ($tiposConcepto as $key => $value) {
            $tipoConceptoArray[$value->id] = strtoupper($value->descripcion);
        }

        $conceptosArray = ['' => 'SELECCIONAR'];
        $conceptos = Adicional::where(function($query) use($conceptoDeducible){
                $query->where('adicionales.tipo_adicional_id', '=', $conceptoDeducible->tipo_adicional_id);
        })->get();

        //dd($conceptos);

        foreach ($conceptos as $key => $value) {
            $conceptosArray[$value->id] = strtoupper($value->descripcion);
        }

        $resumenArray = ['' => 'SELECCIONAR', 1 => 'SI', 0 => 'NO'];

        $deducible['tipo_adicional_id'] = $conceptoDeducible->tipo_adicional_id;

        return view('deducible.show', 
                        ['productos'=> $productosArray, 'tiposConcepto' => $tipoConceptoArray,
                         'conceptos' => $conceptosArray, 'resumen' => $resumenArray,
                         'deducible' => $deducible]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, DeducibleRequest $request)
    {
        $data = $request->all();

        $deducible = Deducible::findOrFail($id);
        $deducible->fill($data)->save();

        Session::flash('flash_message', 'Deducible modificado satisfactoriamente!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $deducible = Deducible::findOrFail($id);
    
        $deducible->delete();
        Session::flash('flash_message', 'Concepto eliminado satisfactoriamente!');
        return 'Ok';
    }

}
