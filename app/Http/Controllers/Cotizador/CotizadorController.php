<?php

namespace App\Http\Controllers\Cotizador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Departamento;
use App\Marca;
use App\Modelo;
use App\Anio;
use App\AnioModelo;
use App\Persona;
use App\Prospecto;
use App\Cotizado;
use App\ModeloProducto;
use App\Tasa;
use App\Compania;
use App\Cuota;
use App\Deducible;
use App\TipoAdicional;
use App\Adicional;
use App\Producto;
use App\Parametro;
use Carbon\Carbon;
use Mail;
use PDF;
use Datetime;

class CotizadorController extends Controller
{
    public function getDepartamentos(){
        $departamentos = Departamento::all()->pluck('nombre', 'id');
        return response($departamentos)->header('Content-Type', 'json');
    }

    public function getMarcas(){
        $marcas = Marca::all()->pluck('nombre', 'id');
        return response($marcas)->header('Content-Type', 'json');
    }

    public function getModelosByMarcas($marcaId){
        $marca = Marca::findOrFail($marcaId);
        $modelos = $marca->modelos->pluck('nombre', 'id');
        return response($modelos)->header('Content-Type', 'json');
    }

    public function getAniosByModelo($modeloId){
        $modelo = Modelo::findOrFail($modeloId);
        $anios = $modelo->anios->pluck('nombre', 'id');
        return response($anios)->header('Content-Type', 'json');
    }

    public function getCompanias(){
        $companias = Compania::all()->pluck('nombre', 'id');
        return response($companias)->header('Content-Type', 'json');
    }

    public function postEnviarCotizacion(Request $request)
    {
        try
        {

            $data = $request->json()->all();
            $personaReq = $data['persona'];
            $prospectoReq = $data['prospecto'];

            $now = Carbon::now();           
    
            /* VALIDAR LA CREACION DE LA PERSONA */
            $persona = Persona::where('documento', $personaReq['documento'])->first();
            if(empty($persona))
            {
                $persona = $personaReq;
                $persona['fecha_nacimiento'] = date("Y-m-d", strtotime($personaReq['fecha_nacimiento']));
                $persona = Persona::create($persona);
            }
    
            /* CREACION DEL PROSPECTO */
            $prospecto = $prospectoReq;
            $prospecto['persona_id'] = $persona['id'];
            $prospecto['fecha_cotizacion'] = $now->format('Y-m-d H:i:s');
            $prospecto = Prospecto::create($prospecto);
    
            /* TRAER LOS PRODUCTOS QUE COTIZAN EL PROSPECTO */
            $productoModelo = 
                    ModeloProducto::where('modelo_id', $prospectoReq['modelo_id'])
                    ->join('productos', 'modelo_productos.producto_id', '=', 'productos.id')
                    ->get();
    
            $companiaPDF = [];
            $anioAutoMovil = Anio::find($prospectoReq['anio_id']);
    
            $productos = [];
            $productosCount = 0;
            if(!empty($productoModelo))
            {
                foreach($productoModelo as $producto)
                {
    
                    /* OBJETO AUXILIAR DE CONSULTA */
                    $anioProducto = [
                        'antiguedad' => ($now->year - (int)$anioAutoMovil['nombre']),
                        'producto_id' => $producto['producto_id'],
                        'prima_minima' => $producto['prima_minima']
                    ];
    
                    /* TRAER LA TASA PARA EL MODELO POR AÑO Y ANTIGUEDAD */
                    $tasa = Tasa::where(function($query) use($anioProducto){
                        if(!empty($anioProducto['antiguedad']))  $query->where('antiguedad', '=', $anioProducto['antiguedad']);
                        if(!empty($anioProducto['producto_id']))  $query->where('producto_id', '=', $anioProducto['producto_id']);
                    })
                    ->join('productos', 'productos.id', '=', 'tasas.producto_id')
                    ->join('companias', 'companias.id', '=', 'productos.compania_id')
                    ->get();
    
                    $tasa = $tasa->first();
                    
                    if(!empty($tasa))
                    {
                        $productos[$productosCount] = $producto['id'];
                        $productosCount++;

                        /* TRAER LOS FACTORES POR COMPAÑIA */
                        $cuotas = Cuota::where('compania_id', '=', $producto['compania_id'])->orderBy('meses','DESC')->get();
                        $factor = Parametro::where(function($query){ $query->where('codigo_grupo', 'FACTOR'); $query->where('codigo', 'PORCENTAJE');})->get();
                        $factor = $factor->first();

                        /* VALOR DE LA TASA */
                        $valor_tasa = ($prospectoReq['valor_aproximado'] * $tasa['porcentaje']) / 100;
                        $valor_prima = $valor_tasa * $factor['valor_numerico'];
                        $valor_prima = (float)($valor_prima < $producto['prima_minima'] ? $producto['prima_minima'] : $valor_prima);
    
                        $prima = [];
                        foreach ($cuotas as $value) 
                        {
                            $valor_prima_calculado = (float)($valor_prima / $value['meses']);
                            $prima[$value['meses']] = ['mes' => $value['meses'], 'prima' => round($valor_prima_calculado, 2) ];   

                            $cotizado = [
                                'tasa_id' => $tasa['id'],
                                'prospecto_id' => $prospecto->id,
                                'modelo_id' => $prospectoReq['modelo_id'],
                                'producto_id' => $anioProducto['producto_id'],
                                'valor_prima' => round($valor_prima_calculado, 2),
                                'cuota' => $value['meses'],
                                'valor_descuento' => $producto['descuento'],
                                'valor_tasa' => $tasa['porcentaje'],
                                'valor_factor' => $factor['valor_numerico']
                            ]; 
                            $cotizado = Cotizado::create($cotizado);
                        }
    
                        $companiaPDF[$tasa['abreviatura']] =   [
                                                                    'abreviatura' => $tasa['abreviatura'],
                                                                    'ruta' => $tasa['logo'],
                                                                    'nombre' => $tasa['nombre'],
                                                                    'prima' => $prima
                                                                ];
                    }
                }
            }

            $response = [];

            $tiposCount = 0;
            $tiposCobertura = TipoAdicional::all();

            foreach ($tiposCobertura as $tipo) 
            {
                $response[$tiposCount] = [
                    'tipoCobertura' => $tipo['descripcion'],
                    'codigoTipo' => 'TIP_COB_' . $tiposCount,
                    'coberturas' => []
                ];

                $coberturasCount = 0;
                $coberturas = Adicional::where('tipo_adicional_id', $tipo->id)->get();

                foreach ($coberturas as $cobertura) 
                {
                    $response[$tiposCount]['coberturas'][$coberturasCount] = [
                        'descripcion' => $cobertura['descripcion'],
                        'productos' => []  
                    ];

                    $productosCount = 0;
                    foreach ($productos as $productoID) 
                    {
                        $useQuery = ['coberturaID' => $cobertura->id, 'productoID' => $productoID];

                        $deducible = Deducible::where(function($query) use($useQuery){
                            $query->where('adicional_id', '=', $useQuery['coberturaID']);
                            $query->where('producto_id', '=', $useQuery['productoID']);                            
                        })->first();
                        
                        $response[$tiposCount]['coberturas'][$coberturasCount]['productos'][$productosCount] = [
                            'productoID' => $productoID,
                            'descripcion' => $deducible['descripcion']
                        ];

                        $productosCount++;
                    }

                    $coberturasCount++;
                }

                $tiposCount++;
            }
            
    
            /* ARMAR EL PDF */
            $ancho = (540/$productosCount) > 270 ? 270 : (540/$productosCount);
            $fijo = (540/$productosCount) > 270 ? 460 : 180;
            $dataPDF = [
                'info_auto' => $prospectoReq['marca']." ".$prospectoReq['modelo']." ".$prospectoReq['anio'],
                'valor_aproximado' => $prospectoReq['valor_aproximado'],
                'numero_prospecto' => $prospecto['id'],
                'documento' => $personaReq['documento'],
                'fecha' => $now->toTimeString(),
                'ancho_columna' => ($ancho)."px",
                'ancho_base' => ($fijo)."px",
                'numero_productos' => $productosCount + 1,
                'companias' => $companiaPDF,
                'tiposCoberturas' => $response
            ];

            $pdf = PDF::loadView('pdf.cotizacionPDF', array('cotizacion' => $dataPDF));

            // GUARDANDO EL ARCHIVO
            file_put_contents(public_path('') .'/pdf/cotizacion_'.$prospecto['id'].'.pdf', $pdf->output());

            /* ENVIO DE CORREO */
            $emails = Parametro::where(function($query){ $query->where('codigo_grupo', 'EMAIL_COPIA'); })->pluck('valor_texto');
            $email = [$prospectoReq['correo']];
            foreach($emails as $iterator){
                array_push($email, $iterator);
            }
            
            $mailInfo = [
                'pdf' => $pdf,
                'correo' => $email,
                'dataCorreo' => $dataPDF 
            ];

            Mail::send('correo.solicitudCotizacion',['info' => $mailInfo['dataCorreo']], function($email) use($mailInfo){
                $email->subject('Solicitud de Cotizacion Autos - Arias & Asociados');
                $email->to($mailInfo['correo']);
                $email->attachData($mailInfo['pdf']->output(), 'cotizacion.pdf');
            });

            return response()->json(['success'=>'Se enviaron los datos de forma correcta', 'id_prospecto' => $prospecto['id']]); //view('cotizador.resultado', $dataPDF);
            
        }
        catch(Exception $ex){

        }
        
    }

    /**
     * @param $prospectoId
     * @return array
     */
    public function GetProspectoById($prospectoId)
    {
        try
        {
            $now = Carbon::now();
            $prospecto = Prospecto::findOrFail($prospectoId);
            $persona = Persona::findOrFail($prospecto['persona_id']);
            $marca = Marca::findOrFail($prospecto['marca_id']);
            $modelo = Modelo::findOrFail($prospecto['modelo_id']);
            $anio = Anio::findOrFail($prospecto['anio_id']);
            $cotizaciones = Cotizado::where('prospecto_id', '=', $prospectoId)->get();
            
            $companias = [];
            $count = 0;
            foreach ($cotizaciones as $key => $cotizacion)
            {
                /* SELECCION DE COBERTURAS A MOSTRAR EN EL RESUMEN */
                $producto = Producto::find($cotizacion['producto_id']);
                $coberturas = Adicional::where(function($query) use($producto){})
                                        ->join('deducibles', function($join) use($producto){
                                            $join->on('deducibles.adicional_id', '=', 'adicionales.id')
                                                 ->where('deducibles.producto_id', '=', $producto['id'])
                                                 ->where('deducibles.flag_resumen', '=', 1);
                                        })
                                        ->select('adicionales.descripcion as principal', 'deducibles.descripcion as descripcion')
                                        ->get();

                $coberturaTexto = 'Ninguna';
                foreach ($coberturas as $key => $cobertura)
                {
                    $coberturaTexto = $cobertura['principal'] . ' <br/> ' . $cobertura['descripcion'] . ' <br/><br/> ';
                }

                /* CUOTAS DE PAGO POR COMPANIA */
                $exist = false;
                $compania = Compania::find($producto['compania_id']);
                foreach ($companias as $key => $comp)
                {
                    if($comp['abreviatura'] == $compania['abreviatura']){
                        $exist = true;
                        array_push($companias[$key]['pagos'], [ 'cuota' => $cotizacion['cuota'], 'prima' => $cotizacion['valor_prima'] ]);
                    }
  
                }

                if(!$exist)
                {
                    $pagos = [];
                    $gps = $producto['requiere_gps'] == 0 ? 'No' : 'Si';
                    $pagos[] = [ 'cuota' => $cotizacion['cuota'], 'prima' => $cotizacion['valor_prima'] ];

                    $companias[$count] = [
                        'abreviatura' => $compania['abreviatura'],
                        'producto' => $producto['nombre'],
                        'productoID' => $producto['id'],
                        'pagos' => $pagos,
                        'cobertura' => $coberturaTexto,
                        'requieregps' => $gps
                    ];

                    $count++;
                }

            }

            $response = [
                'sumaAsegurada' => $prospecto['valor_aproximado'],
                'documento' => $persona['documento'],
                'marca' => [ 'id' => $marca['id'], 'nombre' =>  $marca['nombre'] ],
                'modelo' => [ 'id' => $modelo['id'], 'nombre' =>  $modelo['nombre'] ],
                'anio' => [ 'id' => $anio['id'], 'nombre' =>  $anio['nombre'] ],
                'edad' => $now->diffInYears(Carbon::parse($persona['fecha_nacimiento'])),
                'vigencia' => $now->format('d/m/Y'),
                'correo' => $prospecto['correo'],
                'celular' => $prospecto['celular'],
                'departamento_id' => $prospecto['departamento_id'],
                'companias' => $companias
            ];

            return $response;
        }
        catch(Exception $ex)
        {

        }
    }


    public function postCambiarCotizacion(Request $request)
    {
        try 
        {
            $data = $request->json()->all();
            
            $prospecto = Prospecto::findOrFail($data['prospecto_id']);
            $cotizados = Cotizado::where('prospecto_id', '=', $prospecto['id'])->get();

            $minPrima = 1000000000;
            $maxPrima = 0;
            $companias = [];
            foreach ($cotizados as $key => $cotizado) 
            {
                $tasa = Tasa::findOrFail($cotizado['tasa_id']);
                $producto = Producto::findorFail($cotizado['producto_id']);
                $compania = Compania::findOrFail($producto['compania_id']);
                $cuotas = Cuota::where('compania_id', '=', $producto['compania_id'])->orderBy('meses','DESC')->get();
                $factor = Parametro::where(function($query){ $query->where('codigo_grupo', 'FACTOR'); $query->where('codigo', 'PORCENTAJE');})->get();
                $factor = $factor->first();

                /* VALOR DE LA TASA */
                $valor_tasa = ($request['suma_asegurada'] * $tasa['porcentaje']) / 100;
                $valor_prima = $valor_tasa * $factor['valor_numerico'];
                $valor_prima = (float)($valor_prima < $producto['prima_minima'] ? $producto['prima_minima'] : $valor_prima);


                $prima = [];
                foreach ($cuotas as $value) {
                    $valor_prima_calculado = (float)($valor_prima / $value['meses']);
                    $prima[$value['meses']] = ['mes' => $value['meses'], 'prima' => $valor_prima_calculado ];  
                }

                $cotizado['valor_prima'] = $valor_prima;
                $cotizado['valor_tasa'] = $tasa['porcentaje'];
                $cotizado->save();

                $companias[$key] = [
                    'abreviatura' => $compania['abreviatura'],
                    'prima' => $valor_prima
                ];
            }

            $prospecto['valor_aproximado'] = $data['suma_asegurada'];
            $prospecto->save();

            $response = [
                'sumaAsegurada' => $prospecto['valor_aproximado'],
                'minSumaAsegurada' => (0.8) * $prospecto['valor_aproximado'],
                'maxSumaAsegurada' => (1.2) * $prospecto['valor_aproximado'],
                'minPrima' => $minPrima,
                'maxPrima' => $maxPrima,
                'companias' => $companias
            ];

            return response($response)->header('Content-Type', 'json');
        } 
        catch (exception $ex)
        {
        
        }
    }

    public function postObtenerCoberturas(Request $req){
        try 
        {
            $request = $req->json()->all();
            $response = [];
            // debes de darme la informacion de los productos -- producto id

            $tiposCount = 0;
            $tiposCobertura = TipoAdicional::all();

            foreach ($tiposCobertura as $tipo) 
            {
                $response[$tiposCount] = [
                    'tipoCobertura' => $tipo['descripcion'],
                    'codigoTipo' => 'TIP_COB_' . $tiposCount,
                    'coberturas' => []
                ];

                $coberturasCount = 0;
                $coberturas = Adicional::where('tipo_adicional_id', $tipo->id)->get();

                foreach ($coberturas as $cobertura) 
                {
                    $response[$tiposCount]['coberturas'][$coberturasCount] = [
                        'descripcion' => $cobertura['descripcion'],
                        'productos' => []  
                    ];

                    $productosCount = 0;
                    foreach ($request['productos'] as $productoID) 
                    {
                        $useQuery = ['coberturaID' => $cobertura->id, 'productoID' => $productoID];

                        $deducible = Deducible::where(function($query) use($useQuery){
                            $query->where('adicional_id', '=', $useQuery['coberturaID']);
                            $query->where('producto_id', '=', $useQuery['productoID']);
                        })->first();
                        
                        $response[$tiposCount]['coberturas'][$coberturasCount]['productos'][$productosCount] = [
                            'productoID' => $productoID,
                            'descripcion' => $deducible['descripcion']
                        ];

                        $productosCount++;
                    }

                    $coberturasCount++;
                }

                $tiposCount++;
            }
            
            return response($response)->header('Content-Type', 'json');
        } 
        catch (exception $ex) 
        {
        }
    }

}
