<?php

namespace App;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Model;

class Tasa extends BaseModel
{
    // use Notifiable;
    // use SoftDeletes;

    /*
    *   Nombre de la tabla en BD
    */
    protected $table = 'tasas';

    /**
     * Fields dates
     *
     * @var array
     */
    protected $date = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['producto_id', 'porcentaje', 'antiguedad', 'estado'];

}
