<?php

namespace App;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Model;

class ModeloProducto extends BaseModel
{
    // use Notifiable;
    // use SoftDeletes;

    /*
    *   Nombre de la tabla en BD
    */
    protected $table = 'modelo_productos';

    /**
     * Fields dates
     *
     * @var array
     */
    protected $date = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['modelo_id', 'producto_id', 'estado'];
}
